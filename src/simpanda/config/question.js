export const question = {
    kapal_tunda: [
        {
            title: "DOKUMEN KAPAL TUNDA",
            content: [
                {
                    subtitle: false,
                    radio: ['Valid', 'Tidak Valid', 'Tidak Ada'],
                    question: [
                        {id: 1, text: "SIUPAL / SIOPSUS", value: 1, disabled: true},
                        {id: 2, text: "Surat Ukur"},
                        {id: 3, text: "Gross Akte"},
                        {id: 4, text: "Bukti Sewa/Kerjasama"},
                        {id: 5, text: "Surat tanda Kebangsaan Kapal Indonesia : (Surat Laut untuk GT 175 atau lebih, Pas Tahunan GT: 7 s/d 175) Izin olah gerak dari Syahbandar"},
                        {id: 6, text: "Sertifikat Keselamatan Kapal"},
                        {id: 7, text: "Sertifikat Keselamatan Radio"},
                        {id: 8, text: "Sertifikat Garis Muat"},
                        {id: 9, text: "Surat Izin Komunikasi Radio Kapal"},
                        {id: 10, text: "Sertifikat Bollard Pull"},
                        {id: 11, text: "Sertifikat Sistem Pemadam Kebakaran"},
                        {id: 12, text: "Sertifikat Susunan Perwira (Safe Manning)"},
                        {id: 13, text: "Ship's Particular yang ditanda tangani oleh Nahkoda Kapal"},
                    ],
                },
            ],
        },
        {
            title: "KONDISI UMUM PENUNDAAN",
            content: [
                {
                    subtitle: false,
                    question: [
                        {id: 14, text: "Skema penggunaan kapal tunda"},
                        {id: 15, text: "Jarak antar kapal (sandar sandar/lepas sandar)"},
                        {id: 16, text: "Lokasi perairan (sungai/pelabuhan/off shore)"},
                        {id: 17, text: "Jenis tambatan"},
                        {id: 18, text: "Penambahan fungsi kapal tunda (sebagai kapal pandu dan/atau kapal kepil)"},
                    ]
                }
            ]
        },
        {
            title: "PEMERIKSAAN PERFORMA KAPAL TUNDA",
            content: [
                {
                    subtitle: false,
                    question: [
                        {id: 19, text: "Kesesuaian jenis kapal tunda terhadap kapal yang dilayani,kondisi perairan dan skema penggunaan kapal tunda"},
                        {id: 20, text: "Kemampuan olah gerak kapal tunda ditinjau dari ruang gerak yang tersedia,kondisi perairan dan skema penggunaan kapal tunda"},
                        {id: 21, text: "Kesesuaian kekuatan tarik (bollard pull) dan daya kuda (horse power) terhadap kapal yang dilayani, kondisi perairan dan skema penggunaan kapal tunda"},
                    ],
                }
            ],
        },
        {
            title: "PEMERIKSAAN FISIK KAPAL TUNDA",
            content: [
                {
                    subtitle: "KETERSEDIAAN DAN KESESUAIAN PERALATAN NAVIGASI DAN RADIO TELEKOMUNIKASI",
                    question: [
                        {id: 22, text: "Radar/ARPA"},
                        {id: 23, text: "GPS"},
                        {id: 24, text: "Marine VHF"},
                        {id: 25, text: "Echo Sounder"},
                        {id: 26, text: "Kompas"},
                    ],
                },
                {
                    subtitle: "KETERSEDIAAN DAN KONDISI PERALATAN PENUNDAAN",
                    question: [
                        {id: 27, text: "Tali tunda"},
                        {id: 28, text: "Fairlead"},
                        {id: 29, text: "Emergency/quick release"},
                        {id: 30, text: "Bolder"},
                        {id: 31, text: "Drum"},
                        {id: 32, text: "Towing whinch"},
                        {id: 33, text: "Dapra/fender"},
                        {id: 34, text: "Towing hooks"},
                    ],
                },
                {
                    subtitle: "KETERSEDIAAN DAN KONDISI ALAT PEMADAM KEBAKARAN (FIX WATER INSTALATION)",
                    question: [
                        {id: 35, text: "Pompa pemadam (fire pump)"},
                        {id: 36, text: "Selang pemadam (fire hose)"},
                        {id: 37, text: "Fire Hydrant"},
                        {id: 38, text: "Water Gun/Torret"},
                        {id: 39, text: "International Shore Connection"},
                        {id: 40, text: "Fire Alarm"},
                    ],
                },
                {
                    subtitle: "KETERSEDIAAN DAN KESESUAIAN PERALATAN PENANGGULANGAN PENCEMARAN",
                    question: [
                        {id: 41, text: "Tangki penampung kotor"},
                        {id: 42, text: "Alat pemisah minyak"},
                        {id: 43, text: "Sprayer"},
                        {id: 44, text: "Dispersant"},
                        {id: 45, text: "Serbuk kimia"},
                        {id: 46, text: "Oil Skimer"},
                        {id: 47, text: "Absorbent"},
                        {id: 48, text: "Sawdust"},
                    ],
                },
                {
                    subtitle: "KETERSEDIAAN DAN VALIDASI BUKU CATATAN MINYAK (OIL RECORD BOOK)",
                    question: [
                        {id: 49, text: "Ketersediaan buku catatan minyak (oil record book)"},
                        {id: 50, text: "Validasi buku catatan minyak (oil record book)"},
                    ],
                },
                {
                    subtitle: "KETERSEDIAAN DAN KESESUAIAN PERALATAN KESELAMATAN PENUNJANG LAINNYA",
                    question: [
                        {id: 51, text: "Life Jacket sesuai jumlah awak kapal"},
                        {id: 52, text: "Lampu sorot"},
                        {id: 53, text: "Akses naik/turun ke/dari kapal"},
                    ],
                }
            ],
        },
    ],
    kapal_pandu: [
        {
            title: "DOKUMEN KAPAL PANDU",
            content: [
                {
                    subtitle: false,
                    radio: ['Valid', 'Tidak Valid', 'Tidak Ada'],
                    question: [
                        {id: 1, text: "Bukti kepemilikan atau bukti kerjasama/bukti sewa"},
                        {id: 2, text: "Ship's Particular yang ditanda tangani oleh Nahkoda Kapal atau pemilik"},
                        {id: 3, text: "Surat tanda Kebangsaan Kapal Indonesia : (Surat Laut untuk GT 7 s/d 175 , Pas kecil untuk GT kurang dari 7)"},
                        {id: 4, text: "Sertifikat Keselamatan Kapal"},
                        {id: 5, text: "Sertifikat Keselamatan Radio"},
                        {id: 6, text: "Surat Izin Komunikasi Radio Kapal"},
                        {id: 7, text: "Sertifikat Susunan Perwira (Safe Manning)"},
                    ],
                },
            ],
        },
        {
            title: "KONDISI UMUM PENGGUNAAN KAPAL PANDU",
            content: [
                {
                    subtitle: false,
                    question: [
                        {id: 8, text: "Jarak pelayanan kapal pandu"},
                        {id: 9, text: "Lokasi perairan (sungai/pelabuhan/off shore)"},
                        {id: 10, text: "Jumlah rata-rata kapal yang dilayani pemanduan"},
                        {id: 11, text: "Kondisi perairan (tinggi ombak,kecepatan arus & angin)"},
                    ]
                }
            ]
        },
        {
            title: "PEMERIKSAAN PERFORMA KAPAL PANDU",
            content: [
                {
                    subtitle: false,
                    question: [
                        {id: 12, text: "Kesesuaian kecepatan kapal pandu terhadap area / jarak pelayanan"},
                        {id: 13, text: "Kesesuaian material / bahan utama kapal terhadap kondisi perairan"},
                        {id: 14, text: "Kesesuaian tingkat kenyamanan kapal pandu"},
                        {id: 15, text: "Kemampuan kapal pandu untuk menaik/turunkan pandu ke/dari atas kapal saat kapal yang dilayani masih memiliki kecepatan (underway)"},
                    ],
                }
            ],
        },
        {
            title: "PEMERIKSAAN FISIK KAPAL PANDU",
            content: [
                {
                    subtitle: "KETERSEDIAAN DAN KESESUAIAN PERALATAN NAVIGASI DAN RADIO TELEKOMUNIKASI",
                    question: [
                        {id: 16, text: "RADAR/ARPA"},
                        {id: 17, text: "GPS"},
                        {id: 18, text: "Marine VHF"},
                        {id: 19, text: "Kompas"},
                        {id: 20, text: "Lampu navigasi"},
                    ],
                },
                {
                    subtitle: "KETERSEDIAAN DAN KONDISI KELENGKAPAN KAPAL PANDU",
                    question: [
                        {id: 21, text: "Ruang tempat duduk paling sedikit memiliki 4 (empat) kursi"},
                        {id: 22, text: "Ruang kamar mandi / water closet (WC)"},
                        {id: 23, text: "Pagar relling tunggal"},
                        {id: 24, text: "Deck depan untuk transfer pandu"},
                        {id: 25, text: "Pelampung"},
                        {id: 26, text: "Lampu sorot"},
                        {id: 27, text: "Pelampung penolong (life buoy)"},
                        {id: 28, text: "Baju pelampung (life jacket)"},
                    ],
                },
            ],
        },
    ],
    kapal_kepil: [
        {
            title: "DOKUMEN KAPAL KEPIL",
            content: [
                {
                    subtitle: false,
                    radio: ['Valid', 'Tidak Valid', 'Tidak Ada'],
                    question: [
                        {id: 1, text: "Bukti kepemilikan atau bukti kerjasama/bukti sewa"},
                        {id: 2, text: "Ship's Particular yang ditanda tangani oleh Nahkoda Kapal atau pemilik"},
                        {id: 3, text: "Surat tanda Kebangsaan Kapal Indonesia : (Surat Laut untuk GT 7 s/d 175 , Pas kecil untuk GT kurang dari 7)"},
                        {id: 4, text: "Sertifikat Keselamatan Kapal"},
                        {id: 5, text: "Sertifikat Keselamatan Radio"},
                        {id: 6, text: "Surat Izin Komunikasi Radio Kapal"},
                        {id: 7, text: "Sertifikat Susunan Perwira (Safe Manning)"},
                    ],
                },
            ],
        },
        {
            title: "KONDISI UMUM PENGGUNAAN KAPAL KEPIL",
            content: [
                {
                    subtitle: false,
                    question: [
                        {id: 8, text: "Jarak pelayanan kapal kepil"},
                        {id: 9, text: "Lokasi perairan (sungai/pelabuhan/off shore)"},
                        {id: 10, text: "Jumlah rata-rata kapal yang dilayani pemanduan"},
                        {id: 11, text: "Kondisi perairan (tinggi ombak,kecepatan arus & angin)"},
                    ]
                }
            ]
        },
        {
            title: "PEMERIKSAAN PERFORMA KAPAL KEPIL",
            content: [
                {
                    subtitle: false,
                    question: [
                        {id: 12, text: "Kesesuaian kecepatan kapal kepil terhadap area / jarak pelayanan. Ketersediaan dan kesesuaian jenis, konstruksi dan bahan kapal kepil terhadap jumlah kapal yang akan dilayani, kondisi perairan"},
                        {id: 13, text: "Kesesuaian material / bahan utama kapal terhadap kondisi perairan, jenis tambatan dan skema/prosedur pengepilan"},
                    ],
                }
            ],
        },
        {
            title: "PEMERIKSAAN FISIK KAPAL KEPIL",
            content: [
                {
                    subtitle: "KETERSEDIAAN DAN KESESUAIAN PERALATAN NAVIGASI DAN RADIO TELEKOMUNIKASI",
                    question: [
                        {id: 14, text: "RADAR/ARPA"},
                        {id: 15, text: "GPS"},
                        {id: 16, text: "Marine VHF"},
                        {id: 17, text: "Echo Sounder"},
                        {id: 18, text: "Kompas"},
                        {id: 19, text: "Lampu navigasi"},
                    ],
                },
                {
                    subtitle: "KETERSEDIAAN DAN KONDISI KELENGKAPAN KAPAL KEPIL",
                    question: [
                        {id: 20, text: "Ruang tempat duduk paling sedikit memiliki 4 (empat) kursi"},
                        {id: 21, text: "Ruang kamar mandi / water closet (WC)"},
                        {id: 22, text: "Pagar relling tunggal"},
                        {id: 23, text: "Deck depan untuk transfer kepil"},
                        {id: 24, text: "Pelampung"},
                        {id: 25, text: "Lampu sorot"},
                        {id: 26, text: "Pelampung penolong (life buoy)"},
                        {id: 27, text: "Baju pelampung (life jacket)"},
                    ],
                },
            ],
        },
    ],
}