export const FieldConfig = {
    resource: {
        cabang:{
            personil:"tempat_tugas",
            asset_kapal: "lokasi",
            stasiun_pandu: "ga ada",
            rumah_dinas: "ga ada",
            investigasi_insiden: "ga ada",
            evaluasi_pelimpahan: "cabang_id",
            pemeriksaan_kapal: "cabang_id",
            sarana_bantu_pemandu: "cabang_id",
        },
    },
};