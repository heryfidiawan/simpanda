// Tidak jadi di pakai
export const Module = [
	{
		name: 'dashboard',
		text: 'Dashboard',
		menus: [
			{	
				id: 1,
				name: 'dashboard',
				text: 'Dashboard',
				actions: ['view'],
			},
		],
	},
	{
		name: 'resource',
		text: 'Resource',
		menus: [
			{	
				id: 2,
				name: 'pandu',
				text: 'Pandu',
				actions: ['create','view','edit'],
			},
			{	
				id: 3,
				name: 'pendukung_pandu',
				text: 'Pendukung Pandu',
				actions: ['create','view','edit'],
			},
			{	
				id: 4,
				name: 'pandu_schedule',
				text: 'Pandu Schedule',
				actions: ['create','view','edit'],
			}
		]
	},
	{
		module: 'asset',
		text: 'Asset',
		menus: [
			{	
				id: 5,
				name: 'kapal',
				text: 'Kapal',
				actions: ['create','view','edit'],
			},
			{	
				id: 6,
				name: 'stasiun_pandu',
				text: 'Stasiun Pandu',
				actions: ['create','view','edit'],
			},
			{	
				id: 7,
				name: 'rumah_dinas',
				text: 'Rumah Dinas',
				actions: ['create','view','edit'],
			},
			{	
				id: 8,
				name: 'armada_schedule',
				text: 'Armada Schedule',
				actions: ['create','view','edit'],
			}
		]
	},
	{
		module: 'inspection',
		text: 'Inspection',
		menus: [
			{	
				id: 9,
				name: 'sarana_bantu_pemandu',
				text: 'Sarana Bantu Pemanduan',
				actions: ['create','view','edit'],
			},
			{	
				id: 10,
				name: 'pemeriksaan_kapal',
				text: 'Pemeriksaan Kapal',
				actions: ['create','view','edit'],
			},
			{	
				id: 11,
				name: 'investigasi_insiden',
				text: 'Investigasi Insiden',
				actions: ['create','view','edit'],
			},
			{	
				id: 12,
				name: 'evaluasi_pelimpahan',
				text: 'Evaluasi Pelimpahan',
				actions: ['create','view','edit'],
			}
		]
	},
]