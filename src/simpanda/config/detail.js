import moment from 'moment'

const sertifikat = [
  [
    {
      label: "Jenis",
      type: "select",
      name: "jenis_cert_id",
      view: 'jenis_cert',
      className: "table",
      table: true,
      url: "jeniscert",
      parent: true,
      param: ['jenis_cert_id'],
      child_url: "tipecert",
      op: 'nama',
    },
    {
      label: "Kategori",
      type: "select",
      name: "tipe_cert_id",
      view: "tipe_cert",
      className: "tipe_cert table",
      table: true,
      url: "tipecert",
      op: 'nama',
      child: true,
    },
    {
      label: "Nomor",
      type: "text",
      name: "no_sertifikat",
      className: "table",
      table: true,
    },
    {
      label: "Lembaga yang Mengeluarkan",
      type: "text",
      name: "issuer",
      className: "table",
      table: true,
    },
    {
      label: false,
      type: "button",
      text: "Tambahkan",
      className: "btn btn-primary",
      table: true,
      length: 7,
    }
  ],
  [
    {
      label: "Tanggal Terbit",
      type: "date",
      name: "tanggal_keluar_sertifikat",
      className: "table",
      table: true,
      maxDate: moment(new Date()).format('YYYY-MM-DD'),
    },
    {
      label: "Tanggal Kadaluarsa",
      type: "date",
      name: "tanggal_expire",
      className: "table",
      table: true,
      minDate: moment(new Date()).add(1, 'days').format('YYYY-MM-DD'),
    },
    {
      label: "File",
      type: "file",
      name: "sertifikat",
      className: "table",
      table: true,
    }
  ]
]

const tableSertifikat = {
  thead: ["Jenis", "Kategori", "No Sertifikat", "Lembaga", "Terbit", "Kadaluarsa", "File"],
  tbody: ["jenis_cert", "tipe_cert", "no_sertifikat", "issuer", "tanggal_keluar_sertifikat", "tanggal_expire", "sertifikat"],
}

const penempatanTugas = [
  [
    {
      label: "Tempat Tugas",
      type: "select",
      name: "cabang_id",
      required: true,
      url: "cabang",
      op: 'nama',
    },
    {
      label: "Tanggal Mulai",
      type: "date",
      name: "tanggal_mulai",
      required: true,
    },
    {
      label: "Nomor SK",
      type: "text",
      name: "nomor_sk",
      required: true,
    },
    // {
    //     label: "Tanggal Selesai",
    //     type: "date",
    //     name: "tanggal_selesai",
    //     required: true,
    // },
  ],
  [
    {
      label: "Unggah SK",
      type: "file",
      name: "sk",
      required: true,
    }
  ]
]

const skpp = [

  [
    {
      label: "Tanggal Mulai",
      type: "date",
      name: "skpp_tanggal_mulai",
      required: true,
    },
    {
      label: "Unggah SKPPP",
      type: "file",
      name: "skpp",
      required: true,
    },
  ],
  [
    {
      label: "Tanggal Selesai",
      type: "date",
      name: "skpp_tanggal_selesai",
      required: true,
    },
    // {
    //     label: "Unggah Surat Kesehatan",
    //     type: "file",
    //     name: "surat_kesehatan",
    //     required: true,
    // },
  ]
]

const skesehatan = [

  [
    {
      label: "Tanggal Mulai",
      type: "date",
      name: "skes_tanggal_mulai",
      required: false,
    },
    {
      label: "Unggah Sertifikat Kesehatan",
      type: "file",
      name: "surat_kesehatan",
      required: false,
    },
  ],
  [
    {
      label: "Tanggal Selesai",
      type: "date",
      name: "skes_tanggal_selesai",
      required: false,
    },

  ]
]

const sequipment = [

  [
    {
      label: "No Sertifikat",
      type: "text",
      name: "nomor_sertifikat",
      required: false,
    },
    {
      label: "Tempat Keluar Sertifikat",
      type: "text",
      name: "tempat_keluar",
      required: false,
    },
    {
      label: "Upload Sertifikat",
      type: "file",
      name: "sertifikat_equipment",
      required: false,
    },
  ],
  [
    {
      label: "Tanggal Terbit Sertifikat",
      type: "date",
      name: "tanggal_terbit",
      required: false,
    },
    {
      label: "Tanggal Kadaluarsa Sertifikat",
      type: "date",
      name: "tanggal_kadaluarsa",
      required: false,
    },

  ]
]

export const jabatan_pendukung_pandu = [

  {
    id: 1,
    name: "Master",
  },
  {
    id: 2,
    name: "Chief Officer",
  },
  {
    id: 3,
    name: "2nd Officer",
  },
  {
    id: 4,
    name: "Chief Engineer",
  },
  {
    id: 5,
    name: "2nd Engineer",
  },
  {
    id: 6,
    name: "3rd Engineer",
  },
  {
    id: 7,
    name: "A/B",
  },
  {
    id: 8,
    name: "Oiler",
  },
]

export const jabatan = [

  {
    id: 1,
    name: "DGM Pelayanan Kapal",
  },
  {
    id: 2,
    name: "ADGM Pelayanan Kapal",
  },
  {
    id: 3,
    name: "ADGM Perencanaan Pemanduan",
  },
  {
    id: 4,
    name: "ADGM Administrasi Kepanduan, Komunikasi dan Prasarana Pemanduan",
  },
  {
    id: 5,
    name: "Pandu Bandar Utama",
  },
  {
    id: 6,
    name: "Pandu Bandar",
  },
  {
    id: 7,
    name: "Pandu Laut",
  },
  {
    id: 8,
    name: "SPV Pelayanan Kapal",
  },
  {
    id: 9,
    name: "VTS Operator",
  },
]

const country = [
  { "name": "Afghanistan" },
  { "name": "Åland Islands" },
  { "name": "Albania" },
  { "name": "Algeria" },
  { "name": "American Samoa" },
  { "name": "Andorra" },
  { "name": "Angola" },
  { "name": "Anguilla" },
  { "name": "Antarctica" },
  { "name": "Antigua and Barbuda" },
  { "name": "Argentina" },
  { "name": "Armenia" },
  { "name": "Aruba" },
  { "name": "Australia" },
  { "name": "Austria" },
  { "name": "Azerbaijan" },
  { "name": "Bahamas" },
  { "name": "Bahrain" },
  { "name": "Bangladesh" },
  { "name": "Barbados" },
  { "name": "Belarus" },
  { "name": "Belgium" },
  { "name": "Belize" },
  { "name": "Benin" },
  { "name": "Bermuda" },
  { "name": "Bhutan" },
  { "name": "Bolivia (Plurinational State of)" },
  { "name": "Bonaire, Sint Eustatius and Saba" },
  { "name": "Bosnia and Herzegovina" },
  { "name": "Botswana" },
  { "name": "Bouvet Island" },
  { "name": "Brazil" },
  { "name": "British Indian Ocean Territory" },
  { "name": "United States Minor Outlying Islands" },
  { "name": "Virgin Islands (British)" },
  { "name": "Virgin Islands (U.S.)" },
  { "name": "Brunei Darussalam" },
  { "name": "Bulgaria" },
  { "name": "Burkina Faso" },
  { "name": "Burundi" },
  { "name": "Cambodia" },
  { "name": "Cameroon" },
  { "name": "Canada" },
  { "name": "Cabo Verde" },
  { "name": "Cayman Islands" },
  { "name": "Central African Republic" },
  { "name": "Chad" },
  { "name": "Chile" },
  { "name": "China" },
  { "name": "Christmas Island" },
  { "name": "Cocos (Keeling) Islands" },
  { "name": "Colombia" },
  { "name": "Comoros" },
  { "name": "Congo" },
  { "name": "Congo (Democratic Republic of the)" },
  { "name": "Cook Islands" },
  { "name": "Costa Rica" },
  { "name": "Croatia" },
  { "name": "Cuba" },
  { "name": "Curaçao" },
  { "name": "Cyprus" },
  { "name": "Czech Republic" },
  { "name": "Denmark" },
  { "name": "Djibouti" },
  { "name": "Dominica" },
  { "name": "Dominican Republic" },
  { "name": "Ecuador" },
  { "name": "Egypt" },
  { "name": "El Salvador" },
  { "name": "Equatorial Guinea" },
  { "name": "Eritrea" },
  { "name": "Estonia" },
  { "name": "Ethiopia" },
  { "name": "Falkland Islands (Malvinas)" },
  { "name": "Faroe Islands" },
  { "name": "Fiji" },
  { "name": "Finland" },
  { "name": "France" },
  { "name": "French Guiana" },
  { "name": "French Polynesia" },
  { "name": "French Southern Territories" },
  { "name": "Gabon" },
  { "name": "Gambia" },
  { "name": "Georgia" },
  { "name": "Germany" },
  { "name": "Ghana" },
  { "name": "Gibraltar" },
  { "name": "Greece" },
  { "name": "Greenland" },
  { "name": "Grenada" },
  { "name": "Guadeloupe" },
  { "name": "Guam" },
  { "name": "Guatemala" },
  { "name": "Guernsey" },
  { "name": "Guinea" },
  { "name": "Guinea-Bissau" },
  { "name": "Guyana" },
  { "name": "Haiti" },
  { "name": "Heard Island and McDonald Islands" },
  { "name": "Holy See" },
  { "name": "Honduras" },
  { "name": "Hong Kong" },
  { "name": "Hungary" },
  { "name": "Iceland" },
  { "name": "India" },
  { "name": "Indonesia" },
  { "name": "Côte d'Ivoire" },
  { "name": "Iran (Islamic Republic of)" },
  { "name": "Iraq" },
  { "name": "Ireland" },
  { "name": "Isle of Man" },
  { "name": "Israel" },
  { "name": "Italy" },
  { "name": "Jamaica" },
  { "name": "Japan" },
  { "name": "Jersey" },
  { "name": "Jordan" },
  { "name": "Kazakhstan" },
  { "name": "Kenya" },
  { "name": "Kiribati" },
  { "name": "Kuwait" },
  { "name": "Kyrgyzstan" },
  { "name": "Lao People's Democratic Republic" },
  { "name": "Latvia" },
  { "name": "Lebanon" },
  { "name": "Lesotho" },
  { "name": "Liberia" },
  { "name": "Libya" },
  { "name": "Liechtenstein" },
  { "name": "Lithuania" },
  { "name": "Luxembourg" },
  { "name": "Macao" },
  { "name": "Macedonia (the former Yugoslav Republic of)" },
  { "name": "Madagascar" },
  { "name": "Malawi" },
  { "name": "Malaysia" },
  { "name": "Maldives" },
  { "name": "Mali" },
  { "name": "Malta" },
  { "name": "Marshall Islands" },
  { "name": "Martinique" },
  { "name": "Mauritania" },
  { "name": "Mauritius" },
  { "name": "Mayotte" },
  { "name": "Mexico" },
  { "name": "Micronesia (Federated States of)" },
  { "name": "Moldova (Republic of)" },
  { "name": "Monaco" },
  { "name": "Mongolia" },
  { "name": "Montenegro" },
  { "name": "Montserrat" },
  { "name": "Morocco" },
  { "name": "Mozambique" },
  { "name": "Myanmar" },
  { "name": "Namibia" },
  { "name": "Nauru" },
  { "name": "Nepal" },
  { "name": "Netherlands" },
  { "name": "New Caledonia" },
  { "name": "New Zealand" },
  { "name": "Nicaragua" },
  { "name": "Niger" },
  { "name": "Nigeria" },
  { "name": "Niue" },
  { "name": "Norfolk Island" },
  { "name": "Korea (Democratic People's Republic of)" },
  { "name": "Northern Mariana Islands" },
  { "name": "Norway" },
  { "name": "Oman" },
  { "name": "Pakistan" },
  { "name": "Palau" },
  { "name": "Palestine, State of" },
  { "name": "Panama" },
  { "name": "Papua New Guinea" },
  { "name": "Paraguay" },
  { "name": "Peru" },
  { "name": "Philippines" },
  { "name": "Pitcairn" },
  { "name": "Poland" },
  { "name": "Portugal" },
  { "name": "Puerto Rico" },
  { "name": "Qatar" },
  { "name": "Republic of Kosovo" },
  { "name": "Réunion" },
  { "name": "Romania" },
  { "name": "Russian Federation" },
  { "name": "Rwanda" },
  { "name": "Saint Barthélemy" },
  { "name": "Saint Helena, Ascension and Tristan da Cunha" },
  { "name": "Saint Kitts and Nevis" },
  { "name": "Saint Lucia" },
  { "name": "Saint Martin (French part)" },
  { "name": "Saint Pierre and Miquelon" },
  { "name": "Saint Vincent and the Grenadines" },
  { "name": "Samoa" },
  { "name": "San Marino" },
  { "name": "Sao Tome and Principe" },
  { "name": "Saudi Arabia" },
  { "name": "Senegal" },
  { "name": "Serbia" },
  { "name": "Seychelles" },
  { "name": "Sierra Leone" },
  { "name": "Singapore" },
  { "name": "Sint Maarten (Dutch part)" },
  { "name": "Slovakia" },
  { "name": "Slovenia" },
  { "name": "Solomon Islands" },
  { "name": "Somalia" },
  { "name": "South Africa" },
  { "name": "South Georgia and the South Sandwich Islands" },
  { "name": "Korea (Republic of)" },
  { "name": "South Sudan" },
  { "name": "Spain" },
  { "name": "Sri Lanka" },
  { "name": "Sudan" },
  { "name": "Suriname" },
  { "name": "Svalbard and Jan Mayen" },
  { "name": "Swaziland" },
  { "name": "Sweden" },
  { "name": "Switzerland" },
  { "name": "Syrian Arab Republic" },
  { "name": "Taiwan" },
  { "name": "Tajikistan" },
  { "name": "Tanzania, United Republic of" },
  { "name": "Thailand" },
  { "name": "Timor-Leste" },
  { "name": "Togo" },
  { "name": "Tokelau" },
  { "name": "Tonga" },
  { "name": "Trinidad and Tobago" },
  { "name": "Tunisia" },
  { "name": "Turkey" },
  { "name": "Turkmenistan" },
  { "name": "Turks and Caicos Islands" },
  { "name": "Tuvalu" },
  { "name": "Uganda" },
  { "name": "Ukraine" },
  { "name": "United Arab Emirates" },
  { "name": "United Kingdom of Great Britain and Northern Ireland" },
  { "name": "United States of America" },
  { "name": "Uruguay" },
  { "name": "Uzbekistan" },
  { "name": "Vanuatu" },
  { "name": "Venezuela (Bolivarian Republic of)" },
  { "name": "Viet Nam" },
  { "name": "Wallis and Futuna" },
  { "name": "Western Sahara" },
  { "name": "Yemen" },
  { "name": "Zambia" },
  { "name": "Zimbabwe" },
]

export const DetailConfig = {
  resource: {
    pandu: {
      url: "personil",
      other: "sertifikat",
      more: "Riwayat Aktivitas",
      modal: true,
      defaultValue: {
        tipe_personil_id: 1,
        approval_status_id: 0,
      },
      card: [
        {
          title: "Info Umum",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Nama",
                type: "text",
                name: "nama",
                required: true,
              },
              {
                label: "NIPP",
                type: "number",
                className: "number",
                name: "nipp",
                max: "9",
                required: true,
              },
              {
                label: "Status",
                type: "select",
                name: "enable",
                required: true,
                val: 'id',
                option: [
                  {
                    id: 1,
                    name: "Aktif",
                  },
                  {
                    id: 0,
                    name: "Tidak Aktif",
                  },
                ],
              },
              {
                label: "Unggah CV",
                type: "file",
                name: "cv",
                required: true,
              },
            ],
            [
              {
                label: "Tempat Lahir",
                type: "text",
                name: "tempat_lahir",
                required: true,
              },
              {
                label: "Tanggal Lahir",
                type: "date",
                name: "tanggal_lahir",
                required: true,
              },
              {
                label: "Status Kepegawaian",
                type: "select",
                name: "status_kepegawaian_id",
                required: true,
                val: 'id',
                option: [
                  {
                    id: 1,
                    name: "Organik",
                  },
                  {
                    id: 2,
                    name: "Non Organik",
                  },
                ],
              },
              {
                label: "Jabatan",
                type: "select",
                name: "jabatan",
                required: true,
                val: 'name',
                option: jabatan,
              },
              {
                label: "Kategori",
                type: "select",
                name: "pandu_bandar_laut_id",
                required: true,
                url: false,
                op: false,
                val: 'id',
                option: [
                  {
                    id: 1,
                    name: "Bandar",
                  },
                  {
                    id: 2,
                    name: "Laut",
                  },
                  {
                    id: 3,
                    name: "Bandar Utama",
                  },
                  {
                    id: 4,
                    name: "Pandu Laut Dalam",
                  },
                ],
              },
            ],
          ],
        },
        {
          title: "Penempatan Tugas",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: penempatanTugas,
        },
        {
          title: "SKPPP",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: skpp,
        },
        {
          title: "Sertifikat Kesehatan",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: skesehatan,
        },
        {
          title: "Sertifikat",
          type: "form_input",
          action: ["create", "edit"],
          content: sertifikat,
        },
        {
          title: false,
          type: "table",
          action: ["create", "edit", "detail"],
          content: tableSertifikat,
        },
        {
          title: false,
          type: "submit",
          action: ["create", "edit"],
          content: {
            text: "Simpan Pandu",
            className: "btn btn-primary float-right",
          },
        },
      ],
    },
  },
  asset: {
    kapal: {
      url: "assetkapal",
      other: "sertifikat",
      more: "Riwayat Aktivitas",
      modal: true,
      defaultValue: {
        approval_status_id: 0,
      },
      card: [
        {
          title: false,
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Kepemilikan Kapal",
                type: "select",
                name: "kepemilikan_kapal_id",
                required: true,
                url: "pemilikkapal",
                op: 'nama',
              },
              {
                label: "Nama Asset",
                type: "text",
                name: "nama_asset",
                required: true,
              },
              {
                label: "Status",
                type: "select",
                name: "enable",
                required: true,
                val: 'id',
                option: [
                  {
                    id: 1,
                    name: "Aktif",
                  },
                  {
                    id: 0,
                    name: "Tidak Aktif",
                  },
                ],
              },
              {
                label: "Tahun Perolehan",
                type: "number",
                className: "number",
                name: "tahun_perolehan",
                required: true,
              },
              {
                label: "Lokasi",
                type: "select",
                name: "cabang_id",
                required: true,
                url: "cabang",
                op: 'nama',
              },
            ],
            [
              {
                label: "Jenis Kapal",
                type: "select",
                name: "tipe_asset_id",
                required: true,
                url: "tipeasset/?flag=kapal",
                op: 'nama',
              },
              {
                label: "Daya",
                grouptext: "hp",
                type: "group-append",
                className: "decimal",
                name: "horse_power",
                divider: "daya",
                pembagi: "2",
                required: true,
              },
              {
                label: "Nilai Perolehan",
                type: "number",
                className: "number",
                name: "nilai_perolehan",
                required: true,
              },
              {
                label: "Kode Fasilitas",
                type: "text",
                name: "kd_fas",
                required: true,
                readonly: true,
              },
            ],
          ],
        },
        {
          title: "Dimensi Utama & Pembuatan",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "LOA",
                type: "group-append",
                className: "decimal",
                grouptext: "m",
                name: "loa",
                required: true,
              },
              {
                label: "Breadth",
                type: "group-append",
                className: "decimal",
                grouptext: "m",
                name: "breadth",
                required: true,
              },
              {
                label: "Depth",
                type: "group-append",
                className: "decimal",
                grouptext: "m",
                name: "depth",
                required: true,
              },
              {
                label: "Draft Max",
                type: "group-append",
                className: "decimal",
                grouptext: "m",
                name: "draft_max",
                required: true,
              },
            ],
            [
              {
                label: "Tahun Pembuatan",
                type: "number",
                className: "number",
                name: "tahun_pembuatan",
                required: true,
              },
              {
                label: "Kontruksi",
                type: "select",
                name: "kontruksi",
                required: true,
                url: false,
                op: false,
                val: 'name',
                option: [
                  {
                    id: 1,
                    name: "Baja",
                  },
                  {
                    id: 2,
                    name: "Fiber",
                  },
                  {
                    id: 3,
                    name: "Fiber Glass",
                  },
                  {
                    id: 4,
                    name: "Aluminium",
                  },
                ],
              },
              {
                label: "Negara Pembuat",
                type: "select",
                name: "negara_pembuat",
                required: true,
                url: false,
                op: false,
                val: 'name',
                option: country,
              },
            ],
          ],
        },
        {
          title: "Spesifikasi Teknik Mesin Induk",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Daya 1 Mesin",
                type: "group-append",
                className: "decimal",
                grouptext: "hp",
                name: "daya",
                required: true,
                readonly: true,
              },
              {
                label: "Merk",
                type: "text",
                name: "merk",
                required: true,
              },
            ],
            [
              {
                label: "Putaran",
                type: "group-append",
                className: "decimal",
                grouptext: "rpm",
                name: "putaran",
                required: true,
              },
              {
                label: "Tipe",
                type: "text",
                name: "tipe",
                required: true,
              },
            ],
          ],
        },
        {
          title: "Spesifikasi Teknik Mesin Bantu",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Daya Motor",
                grouptext: "hp",
                type: "group-append",
                className: "decimal",
                name: "daya_motor",
                required: true,
              },
              {
                label: "Putaran",
                type: "group-append",
                className: "decimal",
                grouptext: "rpm",
                name: "putaran_spesifikasi",
                required: true,
              },
              {
                label: "Tipe",
                type: "text",
                name: "tipe_spesifikasi",
                required: true,
              },
            ],
            [
              {
                label: "Daya Generator",
                type: "group-append",
                className: "decimal",
                grouptext: "kva",
                name: "daya_generator",
                required: true,
              },
              {
                label: "Merk",
                type: "text",
                name: "merk_spesifikasi",
                required: true,
              },
            ],
          ],
        },
        {
          title: "Klasifikasi",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Klas",
                type: "select",
                name: "klas",
                required: true,
                val: 'name',
                option: [
                  {
                    id: 1,
                    name: 'BKI',
                  },
                  {
                    id: 2,
                    name: 'Non-Class',
                  }
                ],
              },
              {
                label: "No Registrasi",
                type: "number",
                className: "number",
                name: "no_registrasi",
                required: true,
              },
              {
                label: "Port Of Registration",
                type: "text",
                name: "port_of_registration",
                required: true,
              },
              {
                label: "Notasi Lambung",
                type: "text",
                name: "notasi_lambung",
                required: true,
              },
            ],
            [
              {
                label: "Notasi Permesinan",
                type: "text",
                name: "notasi_permesinan",
                required: true,
              },
              {
                label: "Notasi Perlengkapan",
                type: "text",
                name: "notasi_perlengkapan",
                required: false,
              },
              {
                label: "Notasi Perairan",
                type: "text",
                name: "notasi_perairan",
                required: false,
              },
            ],
          ],
        },
        {
          title: false,
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Gross Tonnage",
                type: "group-append",
                className: "decimal",
                grouptext: "ton",
                name: "gross_tonnage",
                required: true,
              },
              {
                label: "Kecepatan",
                type: "group-append",
                className: "decimal",
                grouptext: "knot",
                name: "kecepatan",
                required: true,
              },
            ],
            [
              {
                label: "Bolard Pull",
                type: "group-append",
                className: "decimal",
                grouptext: "ton",
                name: "bolard_pull",
                required: true,
              },
              {
                label: "Ship Particular",
                type: "file",
                name: "ship_particular",
                required: true,
              },
            ],
          ],
        },
        {
          title: "Sertifikat Kapal",
          type: "form_input",
          action: ["create", "edit"],
          content: sertifikat,
        },
        {
          title: false,
          type: "table",
          action: ["create", "edit", "detail"],
          content: tableSertifikat,
        },
        {
          title: false,
          type: "submit",
          action: ["create", "edit"],
          content: {
            text: "Simpan Kapal",
            className: "btn btn-primary float-right",
          },
        },
      ],
    },
    stasiun_pandu_and_equipment: {
      url: "assetstasiunequipment",
      more: "Riwayat Aktivitas",
      modal: true,
      defaultValue: {
        approval_status_id: 0,
      },
      card: [
        {
          title: "Informasi Umum",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Cabang",
                type: "select",
                name: "cabang_id",
                required: true,
                url: "cabang",
                op: 'nama',
              },
              {
                label: "Kategori",
                type: "select",
                name: "kategori_equipment",
                required: true,
                url: false,
                op: false,
                val: 'id',
                option: [
                  {
                    id: 1,
                    name: "MARINE VHF",
                  },
                  {
                    id: 2,
                    name: "MARINE HT",
                  },
                  {
                    id: 3,
                    name: "LIFE JACKET",
                  },
                  {
                    id: 4,
                    name: "KENDARAAN OPERASIONAL",
                  },
                  {
                    id: 5,
                    name: "AIS",
                  },
                  {
                    id: 6,
                    name: "PETA",
                  },
                  {
                    id: 7,
                    name: "BUKU BUKU STANDAR (PASANG SURUT, DLL)",
                  },
                  {
                    id: 8,
                    name: "MARINE RADIO",
                  },
                  {
                    id: 9,
                    name: "KOMPUTER",
                  },
                  {
                    id: 10,
                    name: "PRINTER",
                  },
                  {
                    id: 11,
                    name: "ANEMOMETER",
                  },
                  {
                    id: 12,
                    name: "FM TRANSCEIVER",
                  },
                  {
                    id: 13,
                    name: "JAS HUJAN",
                  },
                  {
                    id: 14,
                    name: "SAFETY SHOES",
                  },
                  {
                    id: 15,
                    name: "SARUNG TANGAN",
                  },
                  {
                    id: 16,
                    name: "HELM",
                  },
                  
                ],
              },

              {
                label: "Nomor Asset",
                type: "number",
                className: "number",
                name: "nomor_asset",
                required: true,
              },
              {
                label: "Umur Aktif (Tahun)",
                type: "number",
                className: "number",
                name: "total_aktif",
                required: true,
              },

              {
                label: "Lokasi Pemegang",
                type: "text",
                name: "lokasi_pemegang",
                required: true,
              },
              {
                label: "Alamat",
                type: "textarea",
                name: "alamat",
                required: true,
              },
            ],
            [
              {
                label: "Jenis Asset",
                type: "select",
                name: "tipe_asset_id",
                required: true,
                url: "tipeasset/?flag=stasiun",
                op: 'nama',
              },
              {
                label: "Nama Asset",
                type: "text",
                name: "nama",
                required: true,
              },
              {
                label: "Tahun Perolehan",
                type: "number",
                className: "number",
                name: "tahun_perolehan",
                required: true,
              },
              {
                label: "Nilai Perolehan",
                type: "number",
                className: "number",
                name: "nilai_perolehan",
                required: true,
              },
              {
                label: "Kondisi",
                type: "select",
                name: "kondisi",
                required: true,
                val: 'name',
                option: [
                  {
                    id: 1,
                    name: 'Baik',
                  },
                  {
                    id: 2,
                    name: 'Kurang Baik',
                  }
                ],
              },
              {
                label: "Status",
                type: "select",
                name: "enable",
                required: true,
                val: 'id',
                option: [
                  {
                    id: 1,
                    name: "Aktif",
                  },
                  {
                    id: 0,
                    name: "Tidak Aktif",
                  },
                ],
              },
              {
                label: "Keterangan",
                type: "textarea",
                name: "details",
                required: true,
              },
            ],
          ],
        },
        {
          title: "Sertifikat Equipment",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: sequipment,
        },
        {
          title: false,
          type: "submit",
          action: ["create", "edit"],
          content: {
            text: "Simpan Stasiun Pandu & Equipment",
            className: "btn btn-primary float-right",
          },
        },
      ],
    },
    rumah_dinas: {
      url: "assetrumahdinas",
      more: "Riwayat Aktivitas",
      modal: true,
      defaultValue: {
        approval_status_id: 0,
        satuan: 1,
      },
      card: [
        {
          title: "Asset Rumah Dinas",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Nama Asset",
                type: "text",
                name: "nama_asset",
                required: true,
              },
              {
                label: "No Asset",
                type: "text",
                name: "no_asset",
                required: true,
              },
              {
                label: "Tahun Perolehan",
                type: "number",
                className: "number",
                name: "tahun_perolehan",
                required: true,
              },
              {
                label: "Alamat",
                type: "text",
                name: "alamat",
                required: true,
              },
              {
                label: "Wilayah",
                type: "select",
                name: "cabang_id",
                required: true,
                url: "cabang",
                op: 'nama',
              },
              {
                label: "Status",
                type: "select",
                name: "enable",
                required: true,
                val: 'id',
                option: [
                  {
                    id: 1,
                    name: "Aktif",
                  },
                  {
                    id: 0,
                    name: "Tidak Aktif",
                  },
                ],
              },
            ],
            [
              // {
              //   label: "Satuan",
              //   type: "number",
              //   className: "number",
              //   name: "satuan",
              //   required: true,
              //   readonly: true,
              //   value: '1',
              // },
              {
                label: "Status Kepemilikan",
                type: "select",
                name: "status_kepemilikan",
                required: true,
                val: 'name',
                option: [
                  {
                    id: "1",
                    name: "Milik",
                  },
                  {
                    id: "2",
                    name: "Sewa",
                  },
                ],
              },
              {
                label: "Nilai Perolehan",
                type: "number",
                className: "decimal",
                name: "nilai_perolehan",
                required: true,
              },
              {
                label: "Nilai Buku",
                type: "number",
                className: "decimal",
                name: "nilai_buku",
                required: true,
              },
              {
                label: "Keterangan",
                type: "textarea",
                name: "keterangan_rumah_dinas",
                required: true,
              },
            ],
          ],
        },
        {
          title: "Perawatan Asset",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Tanggal",
                type: "date",
                name: "tanggal",
                required: false,
              },
              {
                label: "Catatan",
                type: "textarea",
                name: "catatan",
                required: false,
              },
            ],
            [
              {
                label: "Nilai",
                type: "number",
                className: "decimal",
                name: "nilai",
                required: false,
              },
            ],
          ],
        },
        {
          title: false,
          type: "submit",
          action: ["create", "edit"],
          content: {
            text: "Simpan Rumah Dinas",
            className: "btn btn-primary float-right",
          },
        },
      ],
    },
  },
  administrator: {
    user: {
      url: "user",
      other: false,
      defaultValue: {
      },
      card: [
        {
          title: "User",
          type: "form_input",
          action: ["create", "edit", "detail"],
          content: [
            [
              {
                label: "Nama",
                type: "text",
                name: "nama",
                required: true,
              },
              {
                label: "Username",
                type: "text",
                name: "username",
                className: "lowercase",
                required: true,
              },
              {
                label: "Group",
                type: "select",
                name: "user_group_id",
                required: true,
                url: 'usergroup',
                op: 'nama',
              },
            ],
            [
              {
                label: "Password",
                type: "password",
                name: "password",
                min: "6",
                kombinasi: true,
                required: true,
                match: 'password_confirm',
                match_child: false,
              },
              {
                label: "Konfirmasi Password",
                type: "password",
                name: "password_confirm",
                required: true,
                match: 'password',
                match_child: true,
              },
            ],
          ],
        },
        {
          title: false,
          type: "submit",
          validation: true,
          validationMode: ['create'],
          action: ["create", "edit"],
          content: {
            text: "Simpan User",
            className: "btn btn-primary float-right",
          }
        }
      ],
    },
  }
}
