import Swal from "sweetalert2"

// require('dotenv').config()
// export const APIS = process.env.REACT_APP_BACKEND

//dev
// export const APIS = 'http://localhost:4000/'
// export const APIS = "http://10.88.49.27:4000/"
//prod
export const APIS = "http://10.88.56.79:4000/"

export const Engine = {
    GET(path, token = false) {
        return new Promise((resolve, reject) => {
            try {
                const requestOptions = {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: token,
                    },
                }

                return fetch(APIS + path, requestOptions)
                    .then((response) => response.json())
                    .then((data) => {
                        if (data.code == 403) {
                            window.location.href = "/logout"
                        }
                        console.log("GET engine resolve", data)
                        resolve(data)
                    })
            } catch (error) {
                console.log("engine error")
                reject(error)
            }
        })
    },

    POSTFILE(downloadUrl,fileName, param, token, pathBackend = "", type = "xlsx") {
        Swal.queue([{
            title: 'Unduh File ?',
            text: 'Proses unduh mungkin memerlukan beberapa waktu.',
            confirmButtonText: 'Ya, Unduh File',
            showCancelButton: true,
            cancelButtonText: "Batalkan",
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return fetch(APIS + pathBackend + downloadUrl, {
                    method: 'POST',
                    headers: new Headers({
                        "Authorization": token,
                        'Content-Type': 'application/json'
                    }),
                    body:JSON.stringify(param)
                })
                .then(response => response.blob())
                .then(blob => {
                    var url = window.URL.createObjectURL(blob)
                    var a = document.createElement('a')
                    a.href = url
                    a.download = fileName + type
                    document.body.appendChild(a) // we need to append the element to the dom -> otherwise it will not work in firefox
                    a.click()    
                    a.remove()  //afterwards we remove the element again         

                    return Swal.fire({
                        title: "Success",
                        text: "File berhasil di unduh",
                        type: "success",
                    })

                })
                .catch((error) => {
                    console.log('post file error',error)
                    Swal.insertQueueStep({
                        icon: 'error',
                        title: 'Terjadi kesalahan'
                    })
                })
            }
        }])
    },

    GETFILE(path,filename, token = false) {
        const requestOptions = {
            method: "GET",
            headers: {
                "Content-Type": "application/jsonapplication/vnd.openxmlformats-officedocument.spreadsheetml.sheetapplication/vnd.ms-excelapplication/vnd.openxmlformats-officedocument.wordprocessingml.document",
                Authorization: token,
            },
        }

        Swal.queue([{
            title: 'Unduh File ?',
            text: 'Proses unduh mungkin memerlukan beberapa waktu.',
            confirmButtonText: 'Ya, Unduh File',
            showCancelButton: true,
            cancelButtonText: "Batalkan",
            showLoaderOnConfirm: true,
            preConfirm: () => {

                return fetch(APIS + path, requestOptions)
                    .then(response => response.blob())
                    .then(blob => {
                        const href = window.URL.createObjectURL(blob)
                        const a = document.createElement('a')
                        a.download = filename
                        a.href = href
                        a.click()
                        a.remove()  //afterwards we remove the element again

                        return Swal.fire({
                            title: "Success",
                            text: "File berhasil di unduh",
                            type: "success",
                        })

                    })
                    .catch((error) => {
                        console.log('get file error',error)
                        Swal.insertQueueStep({
                            icon: 'error',
                            title: 'Terjadi kesalahan'
                        })
                    })
            }
        }])
    },

    POST(path, method, data, token = false) {
        return new Promise((resolve, reject) => {
            try {
                const requestOptions = {
                    method: method,
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: token,
                    },
                    body: JSON.stringify(data),
                }

                let message = method == 'DELETE' ? "Data yang anda hapus tidak dapat di kembalikan" : "Data yang anda input akan di tambahkan"
                Swal.fire({
                    title: "Apakah sudah yakin ?",
                    text: message,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Simpan !",
                    cancelButtonText: "Batalkan",
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                    
                        return fetch(APIS + path, requestOptions)
                            .then((response) => response.json())
                            .then((response) => {
                                return response
                            })
                            .catch((error) => {
                                return error
                            })

                    },
                    allowOutsideClick: () => !Swal.isLoading(),
                })
                .then((result) => {
                    console.log("result", result)

                    if (result.dismiss !== "cancel" && result.dismiss !== "backdrop") {
                        console.log('result.value.status',result.value.status)
                        if (result.value.status === false) {
                            Swal.fire({
                                title: "Error !",
                                text: result.value.message,
                                type: 'error',
                            }).then((confirm) => {
                                if (confirm.value) {
                                    return false
                                }
                            })
                            return false
                        }

                        Swal.fire({
                            title: "Tersimpan",
                            text: "Success",
                            type: "success",
                        }).then((confirm) => {
                            if (confirm.value) {
                                resolve(result)
                            }
                        })
                    }
                })
                .catch((error) => {
                    console.log("catch error", error)
                    Swal.fire({
                        title: "Error",
                        text: "Something wrong !",
                        type: "error",
                    })
                })
            } catch (error) {
                reject(error)
            }
        })
    },
  
    PostData(path, data, token = false) {
        const requestOptions = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: token,
            },
            body: JSON.stringify(data),
        }

        console.log("ini athnya", APIS + path)
        return fetch(APIS + path, requestOptions)
            .then((response) => response.json())
            .catch((err) => console.log("err engine", err))
    },
}




// import Swal from "sweetalert2";

// // require('dotenv').config()
// // export const APIS = process.env.REACT_APP_BACKEND

// // export const APIS = 'http://localhost:4000/'
// export const APIS = "http://10.88.49.27:4000/"

// export const Engine = {
//   GET(path, token = false) {
//     // console.log('BACKEND ENV', APIS)
//     return new Promise((resolve, reject) => {
//       try {
//         const requestOptions = {
//           method: "GET",
//           headers: {
//             "Content-Type": "application/json",
//             Authorization: token,
//           },
//         };

//         return fetch(APIS + path, requestOptions)
//           .then((response) => response.json())
//           .then((data) => {
//             if (data.code == 403) {
//               window.location.href = "/logout";
//             }
//             console.log("GET engine resolve", data);
//             resolve(data);
//           });
//       } catch (error) {
//         console.log("engine error");
//         reject(error);
//       }
//     });
//   },

//   POSTFILE(downloadUrl,fileName, param, token, pathBackend = "", type = "xlsx"){
//    return fetch(APIS + pathBackend + downloadUrl, {
//         method: 'POST',
//         headers: new Headers({
//             "Authorization": token,
//             'Content-Type': 'application/json'
//         }),
//         body:JSON.stringify(param)
//     })
//     .then(response => response.blob())
//     .then(blob => {
//         var url = window.URL.createObjectURL(blob);
//         var a = document.createElement('a');
//         a.href = url;
//         a.download = fileName + type;
//         document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
//         a.click();    
//         a.remove();  //afterwards we remove the element again         
//     });
//   },

//   GETFILE(path,filename, token = false) {
//     // console.log('BACKEND ENV', APIS)
//     return new Promise((resolve, reject) => {
//       try {
//         const requestOptions = {
//             method: "GET",
//             headers: {
//               "Content-Type": "application/json;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.wordprocessingml.document",
//               Authorization: token,
//             },
//           };
//           Swal.fire({
//             position: 'top-end',
//             showConfirmButton: false,
//             timer: 1800,
//             timerProgressBar: true,          
//             type: 'success',
//             title: 'Successfully downloaded'
//           }).then(() => {
//             return fetch(APIS + path, requestOptions)
//             .then((response) => response.blob())
//             .then((data) => {
              
//               const href = window.URL.createObjectURL(data);
//               const a = document.createElement('a');
//               a.download = filename;
//               a.href = href;
//               a.click();

//               resolve(data);
//             });
//           }
            
//           )
          
//       } catch (error) {
//         console.log("engine error");
//         reject(error);
//       }
//     });
//   },

//   POST(path, method, data, token = false) {
//     return new Promise((resolve, reject) => {
//       try {
//         const requestOptions = {
//           method: method,
//           headers: {
//             "Content-Type": "application/json",
//             Authorization: token,
//           },
//           body: JSON.stringify(data),
//         };

//         Swal.fire({
//           title: "Apakah sudah yakin ?",
//           text: "Data yang anda input akan di tambahkan",
//           type: "warning",
//           showCancelButton: true,
//           confirmButtonText: "Simpan !",
//           cancelButtonText: "Batalkan",
//           showLoaderOnConfirm: true,
//           preConfirm: () => {
//             return fetch(APIS + path, requestOptions)
//               .then((response) => response.json())
//               .then((response) => {
//                 return response;
//               })
//               .catch((error) => {
//                 return error;
//               });
//           },
//           allowOutsideClick: () => !Swal.isLoading(),
//         })
//           .then((result) => {
//             console.log("result", result);

//             if (result.dismiss !== "cancel" && result.dismiss !== "backdrop") {
//               console.log('result.value.status',result.value.status)
//               if (result.value.status === false) {
//                 Swal.fire({
//                   title: "Error !",
//                   text: result.value.message,
//                   type: 'error',
//                 }).then((confirm) => {
//                   if (confirm.value) {
//                       return false
//                   }
//                 });
//                 return false
//               }

//               Swal.fire({
//                 title: "Tersimpan",
//                 text: "Success",
//                 type: "success",
//               }).then((confirm) => {
//                 if (confirm.value) {
//                   resolve(result);
//                 }
//               });
//             }
//           })
//           .catch((error) => {
//             console.log("catch error", error);
//             Swal.fire({
//               title: "Error",
//               text: "Something wrong !",
//               type: "error",
//             });
//           });
//       } catch (error) {
//         reject(error);
//       }
//     });
//   },
//   PostData(path, data, token = false) {
//     const requestOptions = {
//       method: "POST",
//       headers: {
//         "Content-Type": "application/json",
//         Authorization: token,
//       },
//       body: JSON.stringify(data),
//     };

//     console.log("ini athnya", APIS + path)
//     return fetch(APIS + path, requestOptions)
//       .then((response) => response.json())
//       .catch((err) => console.log("err engine", err));
//   },
// };
