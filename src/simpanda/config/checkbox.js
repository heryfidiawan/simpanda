export const checkbox = {
    bukti_pendukung : {
        namefield : "bukti_pendukung",
        menus : [
            {
                vals : "1",
                detail :"Pernyataan saksi",
            },
            {
                vals :"2",
                detail :"Sketsa kejadian",
            },
            {
                vals :"3",
                detail :"Foto / video",
            },
            {
                vals :"4",
                detail :"Lainnya",
            },
        ],
    },
    wujud_cedera : {
        namefield : "wujud_cedera",
        menus : [
            {
                vals: "1",
                detail:"Amputasi",
            },
            {
                vals: "2",
                detail:"Luka Memar",
            },
            {
                vals: "3",
                detail:"Patah Tulang",
            },
            {
                vals: "4",
                detail:"Luka Lecet",
            },
            {
                vals: "5",
                detail:"Terkilir",
            },
            {
                vals: "6",
                detail:"Luka Bakar",
            },
            {
                vals: "7",
                detail:"Kejang Otot",
            },
            {
                vals: "8",
                detail:"Kemasukan Benda Asing",
            },
            {
                vals: "9",
                detail:"Syaraf Tulang Belakang",
            },
            {
                vals: "10",
                detail:"Keracunan",
            },
            {
                vals: "11",
                detail:"Luka Terbuka",
            },
            {
                vals: "12",
                detail:"Luka Dalam",
            },
            {
                vals: "13",
                detail:"Lainnya",
            },
        ],
        // vals : [
        //     "Amputasi",
        //     "Luka Memar",
        //     "Patah Tulang",
        //     "Luka Lecet",
        //     "Terkilir",
        //     "Luka Bakar",
        //     "Kejang Otot",
        //     "Kemasukan Benda Asing",
        //     "Syaraf Tulang Belakang",
        //     "Keracunan",
        //     "Luka Terbuka",
        //     "Luka Dalam",
        //     "Lainnya",
        // ],
    },
    tubuh_cedera : {
        namefield : "bagian_tubuh_cedera",
        menus : [
            {
                vals: "1",
                detail:"Pergelangan Kaki Kanan",
            },
            {
                vals: "2",
                detail:"Jari Tangan Kanan",
            },
            {
                vals: "3",
                detail:"Kepala",
            },
            {
                vals: "4",
                detail:"Pergelangan Kaki Kiri",
            },
            {
                vals: "5",
                detail:"Jari Tangan Kiri",
            },
            {
                vals: "6",
                detail:"Organ Tubuh Dalam",
            },
            {
                vals: "7",
                detail:"Tangan dan Bahu Kanan",
            },
            {
                vals: "8",
                detail:"Lutut Kanan",
            },
            {
                vals: "9",
                detail:"Mulut",
            },
            {
                vals: "10",
                detail:"Tangan dan Bahu Kiri",
            },
            {
                vals: "11",
                detail:"Lutut Kiri",
            },
            {
                vals: "12",
                detail:"Punggung",
            },
            {
                vals: "13",
                detail:"Mata Kanan",
            },
            {
                vals: "14",
                detail:"Mata Kiri",
            },
            {
                vals: "15",
                detail:"Pergelangan Tangan Kiri",
            },
            {
                vals: "16",
                detail:"Jari Kaki Kanan",
            },
            {
                vals: "17",
                detail:"Tungkai Kaki dan Pinggul Kanan",
            },
            {
                vals: "18",
                detail:"Jari Kaki Kiri",
            },
            {
                vals: "19",
                detail:"Tungkai Kaki dan Pinggul Kiri",
            },
            {
                vals: "20",
                detail:"Tangan Kanan",
            },
            {
                vals: "21",
                detail:"Lainnya",
            },
            {
                vals: "22",
                detail:"Tangan Kiri",
            },
            {
                vals: "23",
                detail:"Wajah",
            },
        ],
        // vals : [
        //     "Pergelangan Kaki Kanan",1
        //     "Jari Tangan Kanan",2
        //     "Kepala",3
        //     "Pergelangan Kaki Kiri",4
        //     "Jari Tangan Kiri",5
        //     "Organ Tubuh Dalam",6
        //     "Tangan dan Bahu Kanan",7
        //     "Lutut Kanan",8
        //     "Mulut",9
        //     "Tangan dan Bahu Kiri",10
        //     "Lutut Kiri",11
        //     "Punggung",12
        //     "Mata Kanan",13
        //     "Mata Kiri",14
        //     "Pergelangan Tangan Kiri",15
        //     "Jari Kaki Kanan",16
        //     "Tungkai Kaki dan Pinggul Kanan",17
        //     "Jari Kaki Kiri",18
        //     "Tungkai Kaki dan Pinggul Kiri",19
        //     "Tangan Kanan",20
        //     "Lainnya",21
        //     "Tangan Kiri",22
        //     "Wajah",23
        // ],
    },
    mekanisme_cedera : {
        namefield : "mekanisme_cedera",
        menus : [
            {
                vals: "1",
                detail:"Faktor Biologis",
            },
            {
                vals: "2",
                detail:"Tekanan Mental",
            },
            {
                vals: "3",
                detail:"Kontak Dengan Bahan Kimia",
            },
            {
                vals: "4",
                detail:"Tertabrak Benda Bergerak",
            },
            {
                vals: "5",
                detail:"Temperatur Ekstrim",
            },
            {
                vals: "6",
                detail:"Tertimpa benda dari atas",
            },
            {
                vals: "7",
                detail:"Kelistrikan",
            },
            {
                vals: "8",
                detail:"Kontak Jangka Panjang",
            },
            {
                vals: "9",
                detail:"Jatuh dari Ketinggian",
            },
            {
                vals: "10",
                detail:"Radiasi",
            },
            {
                vals: "11",
                detail:"Jatuh dari Tangga",
            },
            {
                vals: "12",
                detail:"Bunyi yang ekstrim dengan tiba-tiba",
            },
            {
                vals: "13",
                detail:"Jatuh ke Laut",
            },
            {
                vals: "14",
                detail:"Sengatan dan Gigitan",
            },
            {
                vals: "15",
                detail:"Jatuh Tersandung",
            },
            {
                vals: "16",
                detail:"Mengangkat Beban",
            },
            {
                vals: "17",
                detail:"Kebakaran atau Ledakan",
            },
            {
                vals: "19",
                detail:"Tergencet",
            },
            {
                vals: "18",
                detail:"Lainnya",
            },
        ],
        vals : [
            "Faktor Biologis",
            "Tekanan Mental",
            "Kontak Dengan Bahan Kimia",
            "Tertabrak Benda Bergerak",
            "Temperatur Ekstrim",
            "Tertimpa benda dari atas",
            "Kelistrikan",
            "Kontak Jangka Panjang" ,
            "Jatuh dari Ketinggian",
            "Radiasi",
            "Jatuh dari Tangga",
            "Bunyi yang ekstrim dengan tiba-tiba",
            "Jatuh ke Laut",
            "Sengatan dan Gigitan",
            "Jatuh Tersandung",
            "Mengangkat Beban",
            "Kebakaran atau Ledakan",
            "Lainnya",
        ],
    },
    peralatan_kelengkapan : {
        namefield : "peralatan_kelengkapan",
        menus : [
            {
                vals : "A1",
                detail :"Tidak cukup penilaian terhadap kebutuhan",
            },
            {
                vals :"A2",
                detail :"Pertimbangan faktor manusia atau ergonomi yang tidak cukup baik",
            },
            {
                vals :"A3",
                detail :"Standar atau spesifikasi yang tidak mencukupi",
            },
            {
                vals :"A4",
                detail :"Perbaikan dan pemeliharaan yang tidak cukup baik",
            },
            {
                vals :"A5",
                detail :"Gangguan atau kerusakan pada peralatan",
            },
            {
                vals :"A6",
                detail :"Penggunaan yang tidak tepat",
            },
            {
                vals :"A7",
                detail :"Lainnya",
            },
        ],
    },
    alat_pelindung_diri : {
        namefield : "alat_pelindung_diri",
        menus : [
            {
                vals : "B1",
                detail :"Penggunaan APD tidak tepat",
            },
            {
                vals :"B2",
                detail :"Kerusakan APD",
            },
            {
                vals :"B3",
                detail :"Tidak menggunakan APD",
            },
            {
                vals :"B4",
                detail :"Lainnya",
            },
        ],
    },
    perilaku : {
        namefield : "perilaku",
        menus : [
            {
                vals : "C1",
                detail :"Keasyikan dengan pekerjaan atau peristiwa lain",
            },
            {
                vals :"C2",
                detail :"Mangambil jalan pintas",
            },
            {
                vals :"C3",
                detail :"Percaya diri berlebhan",
            },
            {
                vals :"C4",
                detail :"Lainnya",
            },
        ],
    },
    kebersihan_kerapihan : {
        namefield : "kebersihan_kerapihan",
        menus : [
            {
                vals : "D1",
                detail :"Penataan peralatan dan perlengkapan kerja yang tidak tepat",
            },
            {
                vals :"D2",
                detail :"Penyimpanan dan penjagaan yang tidak tepat",
            },
            {
                vals :"D3",
                detail :"Kebersihan dan kerapihan yang kurang",
            },
            {
                vals :"D4",
                detail :"Lainnya",
            },
        ],
    },
    peralatan_perlengkapan : {
        namefield : "peralatan_perlengkapan",
        menus : [
            {
                vals : "A1",
                detail :" Kekurangan terhadap prosedur",
            },
            {
                vals :"A2",
                detail :"Penggunaan prosedur yang salah",
            },
            {
                vals :"A3",
                detail :"Kurang pemahaman terhadap prosedur",
            },
            {
                vals :"A4",
                detail :"Penerapan prosedur yang tidak mencukupi",
            },
            {
                vals :"A5",
                detail :"Kurang komunikasi",
            },
            {
                vals :"A6",
                detail :"Tidak menjalankan prosedur dan kebijakan",
            },
            {
                vals :"A7",
                detail :"Lainnya",
            },
        ],
    },
    kemampuan_kondisi_fisik : {
        namefield : "kemampuan_kondisi_fisik",
        menus : [
            {
                vals : "B1",
                detail :"Gangguan pada panca indera",
            },
            {
                vals :"B2",
                detail :"Ketidakmampuan fisik secara permanent atau sementara",
            },
            {
                vals :"B3",
                detail :"Tidak mampu mempertahankan posisi tubuh",
            },
            {
                vals :"B4",
                detail :"Keterbatasan jarak pergerakan tubuh",
            },
            {
                vals :"B5",
                detail :"Penempatan atau posisi tubuh yang tidak sesuai",
            },
            {
                vals :"B6",
                detail :"Pengerahan tenaga secara berlebihan",
            },
            {
                vals :"B7",
                detail :"Terpapar material berbahaya",
            },
            {
                vals :"B8",
                detail :"Kebersihan perseorangan yang kurang",
            },
            {
                vals :"B9",
                detail :"Kelelahan atau keletihan",
            },
            {
                vals :"B10",
                detail :"Penyalahgunaan obat-obatan dan alkohol",
            },
            {
                vals :"B11",
                detail :"Luka-luka atau sakit yang diderita sebelumnya",
            },
            {
                vals :"B12",
                detail :"Lainnya",
            },
        ],
    },
    pemeliharaan_perbaikan : {
        namefield : "pemeliharaan_perbaikan",
        menus : [
            {
                vals : "C1",
                detail :"Pemeliharaan pencegahan yang tidak mencukupi",
            },
            {
                vals :"C2",
                detail :"Pemeliharaan perbaikan yang tidak mencukupi",
            },
            {
                vals :"C3",
                detail :"Audit, inspeksi, dan pemantauan yang tidak mencukupi",
            },
            {
                vals :"C4",
                detail :"Pemeliharaan yang terlalu berlebihan",
            },
            {
                vals :"C5",
                detail :"Lainnya",
            },
        ],
    },
    design : {
        namefield : "design",
        menus : [
            {
                vals : "D1",
                detail :"Desain tidak cukup baik",
            },
            {
                vals :"D2",
                detail :"Desain tidak sesuai",
            },
            {
                vals :"D3",
                detail :"Kesalahan disain atau cacat",
            },
            {
                vals :"D4",
                detail :"Lainnya",
            },
        ],
    },
    tingkat_kemampuan : {
        namefield : "tingkat_kemampuan",
        menus : [
            {
                vals : "E1",
                detail :"Penilaian terhadap kemampuan yang dibutuhkan kurang baik",
            },
            {
                vals :"E2",
                detail :"Latihan atau performa keahlian yang tidak mencukupi",
            },
            {
                vals :"E3",
                detail :"Lainnya",
            },
        ],
    },
    penjagaan : {
        namefield : "penjagaan",
        menus : [
            {
                vals : "F1",
                detail :"Tidak dilakukan penjagaan",
            },
            {
                vals :"F2",
                detail :"Tidak cukup penjagaan",
            },
            {
                vals :"F3",
                detail :"Penjagaan tidak tepat",
            },
            {
                vals :"F4",
                detail :"Penilaian yang keliru",
            },
            {
                vals :"F5",
                detail :"Lainnya",
            },
        ],
    },
    tindakan_terkait : {
        namefield : "pemeliharaan_perbaikan",
        menus : [
            {
                vals : "G1",
                detail :"Cuaca",
            },
            {
                vals :"G2",
                detail :"Kondisi Lalu Lintas",
            },
            {
                vals :"G3",
                detail :"Jarak Pandang",
            },
            {
                vals :"G4",
                detail :"Tindakan yang dilakukan oleh bukan personil perusahaan",
            },
            {
                vals :"G5",
                detail :"Tindakan yang dilakukan oleh pengemudi kendaraan lain",
            },
            {
                vals :"G6",
                detail :"Tindakan Manajemen",
            },
            {
                vals :"G7",
                detail :"Kerusuhan",
            },
            {
                vals :"G8",
                detail :"Lainnya",
            },
        ],
    },
    
}