import React from "react"
import moment from 'moment'

import { InputDate } from "./InputDate"
import { Image } from "./Image"

export function SertifikatKesehatan(props) {
	return (
		<div className="card mb-5">
            <div className="card-body">
                <h3 className="font-weight-bold mb-3">Sertifikat Kesehatan</h3>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Tanggal Mulai</label>
                            {/*<input type="date" name="skes_tanggal_mulai" 
                                onChange={props.dataHandle} 
                                value={moment(props.data.skes_tanggal_mulai).format('YYYY-MM-DD')}
                                className="form-control bg-white" 
                                disabled={props.disabled}
                            />*/}
                            <InputDate 
                                name="skes_tanggal_mulai" 
                                dataHandle={props.dataHandle} 
                                value={props.data.skes_tanggal_mulai} 
                                disabled={props.disabled}
                            />
                        </div>
                        <div className="form-group">
                            <label className="d-block">Unggah Sertifikat Kesehatan</label>
                                <Image data={props.data} dataHandle={props.dataHandle} name="surat_kesehatan" required={false}/>
                                {<span className="text-danger d-block mt-2">{props.errmessage.surat_kesehatan}</span>}
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Tanggal Selesai</label>
                            {/*<input type="date" name="skes_tanggal_selesai" 
                                onChange={props.dataHandle} 
                                value={moment(props.data.skes_tanggal_selesai).format('YYYY-MM-DD')}
                                className="form-control bg-white" 
                                disabled={props.disabled}
                            />*/}
                            <InputDate 
                                name="skes_tanggal_selesai" 
                                dataHandle={props.dataHandle} 
                                value={props.data.skes_tanggal_selesai} 
                                disabled={props.disabled}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
	)
}