import React, { useEffect, useState, useRef } from 'react'
import DatePicker, { registerLocale } from "react-datepicker"
import id from "date-fns/locale/id"

import "react-datepicker/dist/react-datepicker.css"
import moment from 'moment'
import { Modal } from 'react-bootstrap'

import { Engine } from "../../simpanda/config/Engine"
import { FieldConfig } from "../../simpanda/config/configfield"
import { shallowEqual, useSelector } from "react-redux"

export function Filter(props) {

	registerLocale("id", id)
	const { isAuthorized } = useSelector(({ auth }) => ({ isAuthorized: auth.user }), shallowEqual)
	const [modal, setModal] = useState({ show: false })
	const [cabang, setCabang] = useState([])
	const [cabangc, setCabangc] = useState([])
	const [kapal, setKapal] = useState([])
	const [checksemua, setChecksemua] = useState([])
	const [certificate, setCertificate] = useState([])
	const [filter, setFilter] = useState([])
	const inlineCheckbox = useRef([]);
	const [display, setDisplay] = useState('')
	const laporanEl = useRef(null);
	const TOKEN = isAuthorized.accessToken
	
	const handleClose = () => {
		setModal({ show: false });
		setFilter([])
		setCheck([])
		console.log(check)
	}

	const handleShow = () => {
		setModal({ show: true });		
		setParam([])
		setTimeout(function(){ 
			for (i = 0; i <= inlineCheckbox.current.length - 1; i++) {
				inlineCheckbox.current[i].checked = true; 
			}
		}, 100);
	}

	const setChange = (e, bool) => {
		if (e.target.value === "option1") {
			for (i = 0; i <= inlineCheckbox.current.length - 1; i++) {
			 	inlineCheckbox.current[i].checked = bool; 
			}
			setChecksemua(bool);
		}
	}

	const [check, setCheck] = useState([])
	const [param,setParam] = useState([])

	const [generate,setGenerate] = useState({cabang_id:isAuthorized.cabang_id, date: moment(new Date()).format('YYYY-MM')})
		
	const handleChange = (e) => {

		let field = e.target.name
		let val = e.target.value

		if (e.target.checked) {
			setChange(e, true);
			filter.push({ field, val })
			if (e.target.value == "option1"){
				setChecksemua(true);
			}
			check.push(val)
			
		} else {
			setChange(e, false);
			setChecksemua(false)
			//will remove if already checked
			const index = filter.findIndex((ch) => ch.field === field && ch.val === val);
			filter.splice(index, 1);
			const removecheck = check.findIndex((dh) => val === dh);
			check.splice(removecheck, 1);
		}
	}

	const checkAll = (e) => {
		let checkboxes = document.querySelectorAll('[name="cabang_id"]');
		// let field = e.target.name
		let checked = e.target.checked
		if (checked) {
			checkboxes.forEach(item => {
				check.push(item.value)
			});
		}else{
			checkboxes.forEach(item => {
				check.splice(item.value)
			});
		}
	}


	
	const handleFilter = () => {
		setFilter({ ...filter })
		let param = []

		filter.map((aa, bb) => {
			param[bb] = "&" + aa.field + "=" + aa.val
		})

		props.handleTable(param.join(""))
		props.handleFiltered(param.join(""))
		handleClose()
	}
	
	const handleGenerate = (name, value) => {
		// const {name,value} = e
		console.log('generate name',name)
		console.log('generate value',value)

		setGenerate({ ...generate ,[name]:value})
		
		if(name === 'jenislaporan' && value === '1'){
			setDisplay('none')
		}else if(name === 'date'){
			if (props.handleUrl == "pelaporanmanagement" && laporanEl.current.value == 1){
				setDisplay('none')
			}
		}else{
			setDisplay('')
		}
	}

	
	console.log(generate)
	const handleDownload = () => {
		console.log(generate)
	}

	let date = new Date()
	let months = [];
	for (var i = 0; i < 12; i++) {
		months[i] = moment().month(date.getMonth()).add(i, 'months').format("MMMM")
	}

	const getCabang = async () => {
		await Engine.GET('cabang/', TOKEN)
			.then(data => {
				setCabang(data)				
			})
			.catch(e => {
				console.log(e);
			})
	};

	const getCabangc = async () => {
		await Engine.GET('cabang/?kd_jenis_pelabuhan=1&kd_jenis_pelabuhan=2', TOKEN)
			.then(data => {
				setCabangc(data)				
			})
			.catch(e => {
				console.log(e);
			})
	};

	const getKapal = async () => {
		await Engine.GET('tipeasset/?flag=kapal', TOKEN)
		.then((data) => {
			setKapal(data)
        }).catch(e => console.log(e) )
	}

	const getCertificate = async () => {
		await Engine.GET('jeniscert/', TOKEN)
			.then(data => {
				setCertificate(data)
			})
			.catch(e => {
				console.log(e);
			});
	};
	console.log("FILTER",props.handleUrl)
	console.log("Generate",generate)
	
	useEffect(() => {
		getCabang()
		getCabangc()
		getCertificate()
		getKapal()		
	}, [])

	return (
		<React.Fragment>
			
			<div className="row w-50">
				{props.config.report &&
					<React.Fragment>
						<div className="col-md-4 mb-2 pt-2">Jenis Laporan:</div>
						<div className="col-md-8 mb-2">
							<select name="jenislaporan" id="" className="form-control" ref={laporanEl} 
								onChange={e => handleGenerate('jenislaporan', e.target.value)} 
							>
								<option value="">Pilih Jenis Laporan</option>
								<option value="1">Laporan produksi & pendapatan Global</option>
								<option value="2">Laporan Produksi & Pendapatan Pandu</option>
								<option value="3">Laporan Produksi & Pendapatan Tunda</option>
							</select>
						</div>
					</React.Fragment>
				}
				{props.config.per_month && props.handleUrl !== "crewlist" &&
					<React.Fragment>
						<div className="col-md-4 mb-2 pt-2">Bulan & Tahun:</div>
						<div className="col-md-8 mb-2">
							<DatePicker
								locale="id" 
								className="form-control"
								selected={new Date(generate.date)} 
								onChange={date => handleGenerate('date', moment(date).format('YYYY-MM'))} 
								dateFormat="MMMM, yyyy"
								showMonthYearPicker
							/>
						</div>
					</React.Fragment>
				}
				{props.config.per_month && 
					<React.Fragment>
						<div className="col-md-4 mb-2 pt-2" style={{'display':display}}>Cabang:</div>
						<div className="col-md-8 mb-2" style={{'display':display}}>
							<select name="cabang_id" id="" defaultValue={isAuthorized.cabang_id} className="form-control" 
								onChange={e => handleGenerate('cabang_id', e.target.value)} >
							
								{isAuthorized.cabang_id === 0 && <option value="">Semua</option>}
								{cabang.length > 0 &&
									cabangc.map((a, b) => {
										return(
											<option key={"crew" + b} value={a.id}>{a.nama}</option>
										)	
									})
								}
							
							</select>
						</div>

						<div className="col-md-4 mb-2 pt-2"></div>
						<div className="col-md-8 mb-2">
							<button className="btn btn-primary" onClick={() => props.handleDownloads(generate)}>
								<i className="flaticon2-download icon-1x"></i> Generate Report
							</button>
						</div>
					</React.Fragment>
				}
			</div>

			<div className="d-flex mb-4">
				{props.config.per_branch &&
					<select name="" id="" className="form-control w-25 ml-auto" onChange={e => props.handlePerBranch(e.target.value)} >
						
						<option value="">Pilih Cabang</option>
						{cabang.length > 0 &&
							cabang.map((a, b) => {
								return(
									<option key={"crew" + b} value={a.id}>{a.nama}</option>
								)	
							})
						}
						
					</select>
				}
				{props.config.page_length &&
					<React.Fragment>
						<span className="pt-2 mr-2">Show:</span>
						<select name="" id="" className="form-control mr-1 table-filter" onChange={e => props.handlePerPage(e.target.value)} >
							<option value="50">50</option>
							<option value="100">100</option>
							<option value="500">500</option>
							<option value="1000">1000</option>
							<option value="2000">2000</option>
						</select>
					</React.Fragment>
				}
				{props.config.search &&
					<input type="text" className="form-control mr-1 table-search" placeholder="Search ..." onChange={e => props.handleSearch(e.target.value)} />
				}
				{props.config.filter &&
					<button className="btn btn-outline-secondary btn-sm border bg-white mr-1 d-flex" onClick={handleShow}>
						<i className="flaticon-interface-7 text-primary icon-1x"></i> Filters
		            </button>
				}
			</div>

			{props.config.filter &&
				<Modal show={modal.show} onHide={handleClose}>
					<Modal.Header closeButton>
						<Modal.Title>Filters</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						{/* {props.handleUrl === "saranabantupemandu" && <>
							<div className="mb-5">
								<h5>Date</h5>
								<div className="row px-3">
									<div className="col-md-12">
										<div className="form-group">
											<input type="date" name="tanggal_pemeriksaan" onChange={handleChange} className="form-control" />
										</div>
									</div>
								</div>
							</div>
							</>
						}
						{props.handleUrl === "evaluasipelimpahan" && <>
							<div className="mb-5">
								<h5>Tanggal SK</h5>
								<div className="row px-3">
									<div className="col-md-12">
										<div className="form-group">
											<input type="date" name="tanggal_sk" onChange={handleChange} className="form-control" />
										</div>
									</div>
								</div>
							</div>
							</>
						} */}
						{cabang.length > 0 && <>
							<div className="mb-5">
								<h5>Cabang</h5>
								<div className="row px-3">
									<div className="form-check col-6">
										<input className="form-check-input" checked={checksemua} type="checkbox" onChange={handleChange} id="inlineCheckbox00" value="option1" />
										<label className="form-check-label" htmlFor="inlineCheckbox00">Semua</label>
									</div>
									{cabang.map((a, b) => {
										return (
											<div className="form-check col-6" key={b}>
												<input key={"b" + b} ref={el => inlineCheckbox.current[b] = el}  name="cabang_id" className="form-check-input" type="checkbox" onChange={handleChange} id={"inlineCheckbox" + b} value={a.id} />
												<label className="form-check-label" htmlFor={"inlineCheckbox" + b}>{a.nama}</label>
											</div>
										)
									})}
								</div>
							</div>
							</>
						}
						{props.handleUrl === "assetkapal" && cabang.length > 0 && <>
							<div className="mt-5">
								<h5>Kapal</h5>
								<div className="row px-3">
									{kapal.map((a, b) => {
										return (
											<div className="form-check col-6" key={b}>
												<input key={"ka" + b} name="tipe_asset_id" defaultChecked={check.length > 0 && check.includes(a.id.toString()) ? true : false} className="form-check-input" type="checkbox" onChange={handleChange} id={"kapal" + b} value={a.id} />
												<label className="form-check-label" htmlFor={"kapal" + b}>{a.nama}</label>
											</div>
										)
									})}
								</div>
							</div>
							</>
						}
						
						{/* <h5 className={cabang.length > 0 ? "mt-5" : ""}>Jenis Sertifikat</h5>
						<div className="row px-3">
							{certificate.map((c, d) => {
								return (
									<div className="form-check col-6" key={d}>
										<input key={"a" + d} name="sertifikat_id" className="form-check-input" onChange={handleChange} type="checkbox" id={"onlineCheckbox" + d} value={c.id} />
										<label className="form-check-label" htmlFor={"onlineCheckbox" + d}>{c.nama}</label>
									</div>
								)
							})}
						</div> */}

					</Modal.Body>
					<Modal.Footer>
						<button type="button" className="btn btn-secondary btn-sm" onClick={handleClose}>Batalkan</button>
						<button type="button" className="btn btn-primary btn-sm" onClick={handleFilter}>Terapkan</button>
					</Modal.Footer>
				</Modal>
			}
		</React.Fragment>
	);
}
