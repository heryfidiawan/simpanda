import React from 'react';
import { Pagination } from 'react-bootstrap';

export function Paginate(props) {
    const totalPages = Math.ceil(props.totalResults / props.perPage)
    const pageFirst = []
	const pageLinks = []
    const pageLast  = []
    
    const visible = totalPages <= 10 ? 9 : 5

    let centerStart = props.currentPage - visible >= 1 ? props.currentPage - visible : 1;
    let centerEnd   = props.currentPage + visible <= totalPages ? props.currentPage + visible : totalPages;
    
    const prev = () => {
        if(props.totalResults < 1 ){
            return (
                <React.Fragment>
                    <Pagination.First disabled onClick={() => props.handlePage(1)} />
                    <Pagination.Prev disabled onClick={() => props.handlePage(props.currentPage - 1)} />
                </React.Fragment>
            )
        }else{
            if(props.currentPage === 1 ){
                return (
                    <React.Fragment>
                        <Pagination.First disabled onClick={() => props.handlePage(1)} />
                        <Pagination.Prev disabled onClick={() => props.handlePage(props.currentPage - 1)} />
                    </React.Fragment>
                )
            }else{
                return(
                    <React.Fragment>
                        <Pagination.First onClick={() => props.handlePage(1)} />
                        <Pagination.Prev onClick={() => props.handlePage(props.currentPage - 1)} />
                    </React.Fragment>
                )
            }
        }
        
    }

    const next = () => {
        if (props.currentPage === totalPages) {
            return(
                <React.Fragment>
                    <Pagination.Next disabled onClick={() => props.handlePage(props.currentPage + 1)} />
                    <Pagination.Last disabled onClick={() => props.handlePage(totalPages)} />
                </React.Fragment>)
        }else{
            return(
                <React.Fragment>
                    <Pagination.Next onClick={() => props.handlePage(props.currentPage + 1)} />
                    <Pagination.Last onClick={() => props.handlePage(totalPages)} />
                </React.Fragment>
            )
        }
    }
    
    for (let i = centerStart; i <= centerEnd; i++) {
        if (props.currentPage === i) {
            pageLinks.push(<Pagination.Item key={i} active onClick={() => props.handlePage(i)}>{i}</Pagination.Item>)
        }else {
            pageLinks.push(<Pagination.Item key={i} onClick={() => props.handlePage(i)}>{i}</Pagination.Item>)
        }
    }

    if (centerEnd < totalPages-2) {
        for (let i=totalPages-2; i <= totalPages; i++) {
            if (props.currentPage === i) {
                pageLast.push(<Pagination.Item key={i} active onClick={() => props.handlePage(i)}>{i}</Pagination.Item>)
            }else {
                pageLast.push(<Pagination.Item key={i} onClick={() => props.handlePage(i)}>{i}</Pagination.Item>)
            }
        }
    }

    if (centerStart > 1) {
        for (let i=1; i <= 3; i++) {
            if (props.currentPage === i) {
                pageFirst.push(<Pagination.Item key={i} active onClick={() => props.handlePage(i)}>{i}</Pagination.Item>)
            }else {
                pageFirst.push(<Pagination.Item key={i} onClick={() => props.handlePage(i)}>{i}</Pagination.Item>)
            }
        }
    }

	return (
		<div className="d-flex mt-5">
            <div className="mx-auto">
                <Pagination>
                    {prev()}
                    
                    {pageFirst}
                        {centerStart > 1 && <Pagination.Ellipsis />}
                    {pageLinks}
                        {centerEnd < totalPages-2 && <Pagination.Ellipsis />}
                    {pageLast}

                    {next()}
                </Pagination>
                
            </div>
            {/* <div  style={{position: "relative",left:"-30px",display: "inline-block"}}>
                <button className="btn btn-primary btn-sm float-right btn-radius">
                    <i className="fa fa-excel"></i>Download
                </button>
            </div> */}
        </div>
	)
}
