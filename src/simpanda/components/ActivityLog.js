import React from "react"
import moment from 'moment'

export function ActivityLog({ activityLog }) {
	return (
		<div className="card">
            <div className="card-body">
                <h3 className="font-weight-bold mb-3">Riwayat Aktivitas</h3>

                    <div className="scroll-custom" style={{'maxHeight':'230px','overflowY':'auto'}}>
                        <div className="col-12 card bg-light p-5">
                            {activityLog.map((v,i) => {
                                return(
                                    <div key={i} className="row mb-1">
                                        <div className="col-md-2 border-right">
                                            <p className="pt-3 font-weight-bold">{moment(v.date).format('DD/MMM/YYYY')}</p>
                                            <p className="ml-3">{moment(v.date).format('HH:mm:ss')}</p>
                                        </div>
                                        <div className="col-md-10">
                                            <p className="pt-3">{v.remark}</p>
                                            <p>{v.keterangan}</p>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    
            </div>
        </div>
	)
}