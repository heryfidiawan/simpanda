import React from "react"
import moment from 'moment'

import { InputDate } from "./InputDate"
import { Image } from "./Image"

export function TempatTugas(props) {
	return (
		<div className="card mb-5">
            <div className="card-body">
                <h3 className="font-weight-bold mb-3">Penempatan Tugas</h3>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Tempat Tugas <span className="text-red">*</span></label>
                            <select name="cabang_id" 
                                onChange={props.dataHandle} 
                                value={props.data.cabang_id}
                                className="form-control bg-white"
                                required={true}
                                disabled={props.disabled}
                            >
                                <option value="">Pilih</option>
                                {props.cabang.map((v,i) => {
                                    return(<option key={i} value={v.id}>{v.nama}</option>)
                                })}
                            </select>
                        </div>
                        <div className="form-group">
                            <label>Tanggal Mulai SK/PKL <span className="text-red">*</span></label>
                            {/*<input type="date" name="tanggal_mulai" 
                                onChange={props.dataHandle} 
                                value={moment(props.data.tanggal_mulai).format('YYYY-MM-DD')}
                                className="form-control bg-white" 
                                required={true}
                                disabled={props.disabled}
                            />*/}
                            <InputDate 
                                name="tanggal_mulai" 
                                dataHandle={props.dataHandle} 
                                value={props.data.tanggal_mulai} 
                                required={true}
                                disabled={props.disabled}
                            />
                        </div>
                        <div className="form-group">
                            <label>Tanggal Selesai SK/PKL <span className="text-red">*</span></label>
                            {/*<input type="date" name="tanggal_selesai" 
                                onChange={props.dataHandle} 
                                value={moment(props.data.tanggal_selesai).format('YYYY-MM-DD')}
                                className="form-control bg-white" 
                                required={true}
                                disabled={props.disabled}
                            />*/}
                            <InputDate 
                                name="tanggal_selesai" 
                                dataHandle={props.dataHandle} 
                                value={props.data.tanggal_selesai} 
                                required={true}
                                disabled={props.disabled}
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Nomor SK/PKL <span className="text-red">*</span></label>
                            <input type="text" name="nomor_sk" 
                                onChange={props.dataHandle} 
                                value={props.data.nomor_sk}
                                className="form-control bg-white" 
                                required={true}
                                disabled={props.disabled}
                            />
                        </div>
                        <div className="form-group">
                            <label className="d-block">Unggah SK/PKL {props.data['sk'] ? "" : <span className="text-red">*</span>}</label>
                                <Image data={props.data} dataHandle={props.dataHandle} name="sk" required={props.data['sk'] ? false : true}/>
                                {<span className="text-danger d-block mt-2">{props.errmessage.sk}</span>}
                        </div>
                    </div>
                </div>
            </div>
        </div>
	)
}