import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { Paginate } from "./Paginate";
import { Modal, OverlayTrigger, Tooltip } from "react-bootstrap";

import { Engine } from "../../simpanda/config/Engine";
import { shallowEqual, useSelector } from "react-redux";

import moment from "moment";

export function Table(props) {
    const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)
    const [modalRequest, setModalRequest] = useState({ show: false })
    const [modalDecline, setModalDecline] = useState({ show: false })
    const [request, setRequest] = useState("accept")
    const [primary, setPrimary] = useState(false)
    const [input, setInput] = useState("")
    const [remarks, setRemarks] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    const [countChar, setCountChar] = useState(0)
    const [expanded, setExpanded] = useState(false)
    const [show, setShow] = useState("")
    const [validasi, setValidasi] = useState("form-control")
    const [tabhead, setTabhead] = useState(props.config.table.thead)
    const handleCloseRequest = () => {
        setModalRequest({ show: false })
    }

    const handleShowRequest = (req, key) => {
        setPrimary(key)
        setRequest(req)
        setModalRequest({ show: true })
        setValidasi("form-control")
        setErrorMessage("")
    }

    const downloadFile = (ids) => {
        var downloadUrl = "report/"+props.config.backend+"/"+ids.id;
        var filename = props.config.backend;
        var x = Math.floor((Math.random() * 10000) + 100);
        
        var fileName = ""
        if (filename === "investigasiinsiden") {
            fileName = "Report-"+filename+"_"+ids.no_report+".docx";
        }else{
            fileName = "Report-"+filename+"_"+x+".xlsx";
        }

        Engine.GETFILE(downloadUrl,fileName, isAuthorized.accessToken)
    }
    
    useEffect(() => {
        return (
            () => {
                if (isAuthorized.cabang_id > 0 && props.config.table.thead.includes("Request")) {
                    setTabhead(props.config.table.thead.pop())
                }

                // console.log(props.config.table.thead.indexOf("Status"))
                if (isAuthorized.cabang_id < 1 && props.config.table.thead.includes("Status")) {
                    setTabhead(props.config.table.thead.splice(props.config.table.thead.indexOf("Status"),1))
                }
            }
        )
    }, [])

    const handleChange = (inputs) => {
        setCountChar(inputs.length)
        setInput(inputs)
    }

    const toggleExpander = (data,ids) => {
        if(data.id === ids){
            setExpanded(!expanded)
            setShow(ids)
            // console.log(data)
        }
    }

    const dates = () => {
        let date_create = moment().format("YYYY-MM-DD HH:mm:ss")
        return date_create
    }

    const handleRequest = (req) => {
        let params = {}
        if (req === "accept") {
            params = {
                approval_status_id: "1",
                date : dates(),
                item :props.config.backend,
                keterangan: input,
                user_id:"1",
            }
        } else {
            params = {
                approval_status_id: "2",
                date : dates(),
                item :props.config.backend,
                keterangan: input,
                user_id:"1",
            }
        }
        
        if (input === "" && req === "decline") {
            setValidasi("form-control inValid")
            setErrorMessage("Kolom komentar wajib diisi !!")
        }else if (parseInt(params.keterangan.length) < 20 && req === "decline") {
            setValidasi("form-control inValid")
            setErrorMessage('Keterangan minimal 20 karakter')
        }else if (!/([a-z]+)/g.test(params.keterangan) && req === "decline"){
            setErrorMessage('Keterangan tidak valid')
            setValidasi("form-control inValid")
        } else {
            setValidasi("form-control")
            setErrorMessage('')
            Engine.POST(props.config.backend+'/'+primary, 'PUT', params, isAuthorized.accessToken)
            .then(res => {
                setModalRequest({ show: false })
                setInput("")
                // console.log(params)
                // history.push(`${props.config.url}`)
                setTimeout(() =>  window.location.reload(), 500)
                // window.location.reload()
            })
            
        }
    }

    const unlockLogin = (req) => {
       
        let params = {
            flag : 0,
            type:'unlock'
        }
        
        Engine.POST(props.config.backend+'/'+req, 'PUT', params, isAuthorized.accessToken)
        .then(res => {
            setModalRequest({ show: false })
            setTimeout(() =>  window.location.reload(), 500)
        })
            
        
    }

    const handleButton = (a, b, data) => {
        let disable = ""
        let classes = ""
        if(props.config.backend ===  "pemeriksaankapal" && data.jumlah_temuan !== null) {
            disable = "disabled"
            classes = "badge badge-default border-0 mr-2"
        }else{
            disable = ""
            classes = "badge badge-primary border-0 mr-2"
        }
        if (data.approval_status_id !== 2){
            if (a === "accept") {
                return (
                    <button key={b} className={classes} disabled={disable} onClick={() => handleShowRequest("accept", data.id)}>
                        Terima
                    </button>
                )
            } else {
                return (
                    <button key={b} className="badge badge-danger border-0" onClick={() => handleShowRequest("decline", data.id)}>
                        Tolak
                    </button>
                )
            }
        }else{
            if (a === "decline") {
                return (
                    <label className="badge badge-danger border-0 disabled" onClick={() => handleShowDecline(data.id)}>
                        Tolak
                    </label>
                )
            }
        }
    }

    const handleCloseDecline = () => {
        setModalDecline({ show: false })
    }

    const handleShowDecline = (ids) => {
        setModalDecline({ show: true })
        // console.log(ids)
        Engine.GET("activitylog?koneksi="+ids+"&item="+props.config.backend+"&action=2", isAuthorized.accessToken)
            .then(res => {
                setRemarks(res[0].keterangan)
            })       
    }

    const handleStatus = (a, b, data,ids) => {
        if (b === 0)
        if (data === 0) {
            return (
                <button key={b} className="badge badge-default border-0">On Progress</button>
            )
        } else {
            return (
                <label key={b} className="badge badge-danger border-0"  onClick={() => handleShowDecline(ids)}>Tolak</label>
            )
        }
    }

    return (
        <React.Fragment>
            {tabhead.length > 0 && 
                <React.Fragment>
                    <div className="table-responsive">
                        <table className="table table-hover w-100">
                            <thead>
                                <tr>
                                    {tabhead.map((head, index) => {
                                        return (
                                        <th scope="col" colSpan={
                                            props.config.table.chead &&
                                            props.config.table.chead[index]
                                            ? "2"
                                            : "1"
                                        } className="text-center align-middle" key={index}>
                                            {head}
                                            {/* {console.log(isAuthorized.cabang_id !== null ? head.pop() : head)} */}
                                        </th>
                                        )
                                    })}
                                </tr>
                            </thead>
                            <tbody>
                                {props.config.table.chead && (
                                <tr>
                                    {props.config.table.chead.map((ch, ci) => {
                                        return ch ? <th key={ci}>{ch}</th> : <td key={ci}></td>
                                    })}
                                </tr>
                                )}

                                {props.loading === false && props.data.length === 0 && 
                                    <tr className="text-center">
                                    <td colSpan={tabhead.length}>No Data Found</td>
                                    </tr>
                                }
                                
                                {props.loading ? (
                                    <tr>
                                        <td>
                                            <div className="spinner spinner-warning mt-5 spinner-table-loading"></div>
                                        </td>
                                    </tr>
                                    
                                ) : (props.data.map((data, index) => {
                                    return (
                                        <tr key={index} className="text-center" onClick={() => toggleExpander(data,data.id)}>
                                            <td>{(parseInt(props.pagination.currentPage) - 1) * parseInt(props.pagination.perPage) + index + 1}</td>
                                                {props.config.table.tbody.map((tb, i) => {
                                                    return <td key={i}>{tb === "date" || tb === "tanggal_pemeriksaan" || tb === "tanggal_sk" || tb === "tanggal_selesai" ? data[tb] !== null ? moment(data[tb]).format('DD MMMM YYYY') : "" : data[tb]}</td>
                                                    
                                                })}
                                                
                                                {props.config.aksi && (
                                            <td>
                                                {props.config.aksi.map((a, b) => {
                                                    console.log(a)
                                                    return a === "show" ? (
                                                        <OverlayTrigger
                                                            key={b}
                                                            placement="top"
                                                            overlay={
                                                                <Tooltip id={`tooltip-${b}`} className="tooltip-dark">Lihat</Tooltip>
                                                            }
                                                        >
                                                            <NavLink
                                                                key={b}
                                                                className="flaticon-eye text-success icon-1x mr-3 bg-light px-3 py-2 rounded"
                                                                to={`${props.config.url}/detail/${data.id}`}
                                                            />
                                                        </OverlayTrigger>
                                                    ) : (
                                                        a === "edit" ? ( isAuthorized.cabang_id > 0 ? 
                                                                <OverlayTrigger
                                                                    key={b}
                                                                    placement="top"
                                                                    overlay={
                                                                        <Tooltip id={`tooltip-${b}`} className="tooltip-dark">Edit</Tooltip>
                                                                    }
                                                                >
                                                                    <NavLink
                                                                        key={b}
                                                                        className="flaticon2-edit text-warning icon-1x mr-3 bg-light px-3 py-2 rounded"
                                                                        to={`${props.config.url}/edit/${data.id}`}
                                                                    />
                                                                </OverlayTrigger>

                                                            :  props.config.backend === 'user' || props.config.backend === 'usergroup' && isAuthorized.cabang_id === 0 ?
                                                                <OverlayTrigger
                                                                    key={b}
                                                                    placement="top"
                                                                    overlay={
                                                                        <Tooltip id={`tooltip-${b}`} className="tooltip-dark">Edit</Tooltip>
                                                                    }
                                                                >
                                                                    <NavLink
                                                                        key={b}
                                                                        className="flaticon2-edit text-warning icon-1x mr-3 bg-light px-3 py-2 rounded"
                                                                        to={`${props.config.url}/edit/${data.id}`}
                                                                    /> 
                                                                </OverlayTrigger>
                                                            : <></>
                                                            
                                                        ) : (
                                                            a === "download" ? (
                                                                <OverlayTrigger
                                                                    key={b}
                                                                    placement="top"
                                                                    overlay={
                                                                        <Tooltip id={`tooltip-${b}`} className="tooltip-dark">Download</Tooltip>
                                                                    }
                                                                >
                                                                    <button
                                                                        key={b}
                                                                        className=" bg-light px-3 py-2 rounded"
                                                                        style={{border: "none"}}
                                                                        onClick={() => downloadFile(data)}
                                                                    ><i className="flaticon2-download text-warning icon-1x"> </i></button>
                                                                </OverlayTrigger>
                                                            )  : ( 
                                                                a === "unlock" &&  props.config.backend === 'user' && isAuthorized.cabang_id === 0 && data.flag === 3 && 
                                                                <OverlayTrigger
                                                                    key={b}
                                                                    placement="top"
                                                                    overlay={
                                                                        <Tooltip id={`tooltip-${b}`} className="tooltip-dark">Unlock</Tooltip>
                                                                    }
                                                                >
                                                                    <button
                                                                        key={b}
                                                                        className=" bg-light px-3 py-2 rounded"
                                                                        style={{border: "none"}}
                                                                        onClick={() => unlockLogin(data.id)}
                                                                    ><i className="flaticon2-lock text-danger icon-1x"> </i></button>
                                                                </OverlayTrigger>
                                                            )
                                                            
                                                        )
                                                    ) 
                                                })}
                                            </td>
                                            )}
                                            {isAuthorized.cabang_id > 0 && props.config.status && (
                                                <td>
                                                    {props.config.status.map((a, b) => {
                                                        return handleStatus(a, b, data.approval_status_id,data.id)
                                                    })}
                                                </td>
                                            )}
                                            {isAuthorized.cabang_id < 1 && props.config.request && (
                                                <td className="">
                                                    {props.config.request.map((a, b) => {
                                                        return handleButton(a, b, data)
                                                    })}
                                                </td>
                                            )}
                                        </tr>
                                    )
                                }))}
                                
                            </tbody>
                        </table>
                        
                    </div>
                                
                    {!props.loading && (
                        <Paginate
                            perPage={props.pagination.perPage}
                            currentPage={props.pagination.currentPage}
                            handlePage={props.pagination.handlePage}
                            totalResults={props.pagination.totalResults}
                        />
                    )}

                    {props.config.request && (
                        <Modal show={modalRequest.show} onHide={handleCloseRequest}>
                            <Modal.Header className="modal-header-custom" closeButton>
                                <Modal.Title></Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="text-center">
                                    <img src="/media/icons/request-frame.png" alt="frame" className="margin-auto"/>
                                    {request === "accept" && (
                                        <React.Fragment>
                                            <h3 className="text-primary">Pengajuan Disetujui</h3>
                                            <p>Apakah anda yakin akan menyetujui pengajuan ini ?</p>
                                        </React.Fragment>
                                    )}
                                    {request === "decline" && (
                                        <React.Fragment>
                                            <h3 className="text-primary mt-5">Pengajuan Ditolak</h3>
                                            <p className="mt-5">Tuliskan alasan pengajuan ditolak !</p>
                                            <textarea name="" value={input} id="" onChange={(e) => handleChange(e.target.value)} 
                                                rows="5" className={validasi} placeholder="Tuliskan komentar anda..."
                                            ></textarea>
                                            <small className="text-danger float-left">{errorMessage}</small>
                                            <small className="float-right">{countChar}</small>
                                        </React.Fragment>
                                    )}
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button type="button" className="btn btn-secondary btn-sm" onClick={handleCloseRequest}>
                                    Batalkan
                                </button>
                                <button type="button" className="btn btn-primary btn-sm" onClick={() => handleRequest(request)}>
                                    {request === "accept" ? "Terima" : "Kirim"}
                                </button>
                            </Modal.Footer>
                        </Modal>
                    )}

                    {
                        <Modal show={modalDecline.show} onHide={handleCloseDecline}>
                            <Modal.Header className="modal-header-custom" closeButton>
                                <Modal.Title></Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="text-center">
                                    <React.Fragment>
                                        <h3 className="text-center mb-10">Keterangan Pengajuan Ditolak</h3>
                                        <textarea name="remark" readOnly className="form-control" value={remarks} ></textarea>
                                    </React.Fragment>
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button type="button" className="btn btn-secondary btn-sm" onClick={handleCloseDecline}>
                                    Tutup
                                </button>
                            </Modal.Footer>
                        </Modal>
                    }
                </React.Fragment>
            }
        </React.Fragment>
    )
}
