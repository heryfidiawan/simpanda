import React, { useState, useEffect } from "react"
import { shallowEqual, useSelector } from "react-redux"

import moment from 'moment'

import { Image } from "./Image"
import { InputDate } from "./InputDate"

import { APIS, Engine } from "../config/Engine"
import { OverlayTrigger, Tooltip } from "react-bootstrap"
const backEnd   = APIS

// const defaultData = {
//     jenis_cert_id:'',
//     tipe_cert_id:'',
//     jenis_cert:'',
//     tipe_cert:'',
//     no_sertifikat:'',
//     issuer:'',
//     tanggal_keluar_sertifikat:'',
//     tanggal_expire:'',
//     sertifikat:'',
// }

export function Sertifikat(props) {
    const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)
    
    const TOKEN   = isAuthorized.accessToken

    const [data, setData] = useState([])
    const [table, setTable] = useState([])

    const [jenisCert, setJenisCert] = useState([])
    const [tipeCert, setTipeCert] = useState([])
    const [jenisCertId, setJenisCertId] = useState(false)
	const [errMessage, setErrMessage] = useState(false)

    const getJenisCert = async () => {
        let link = 'jeniscert'
        if (props.radio > 0) {
            link += '/?radio='+props.radio
        }
        await Engine.GET(link, TOKEN)
        .then(data => {
            setJenisCert(data)
        }).catch(e => console.log(e))
    }

    const getTipeCert = async () => {
        let link = 'tipecert/?jenis_cert_id='+jenisCertId
        if (props.radio > 0) {
            link += '&radio='+props.radio
        }
        await Engine.GET(link, TOKEN)
        .then(data => {
            setTipeCert(data)
        }).catch(e => console.log(e))
    }

    useEffect(() => {
        if (props.action !== 'detail') {
            getJenisCert()
            getTipeCert()
        }
    }, [jenisCertId, props.radio])

    useEffect(() => {
        setTable(props.sertifikat)
    }, [props])

    const dataHandle = async (e, dateName=false) => {
        const target = e.target

        if (dateName) {
            setData(prev => ({ ...prev, [dateName]: e }))
            return false
        }

        if (target.type === 'file') {
            if (target.files[0] ) {
				if (target.files[0].size <= 15000000) {
					let base64 = await convertBase64(target.files[0])
                    setData(prev => ({ ...prev, [target.name]: base64, 'blob': URL.createObjectURL(target.files[0]) }))
					setErrMessage('') 
				}else{
					setData(prev => ({ ...prev, [target.name]: '' })) 
					setErrMessage('Ukuran file tidak boleh melebihi 15 MB') 
					
				}
            	return false
            }
            // if (target.files[0]) {
            //     let base64 = await convertBase64(target.files[0])
            //     setData(prev => ({ ...prev, [target.name]: base64, 'blob': URL.createObjectURL(target.files[0]) }))
            //     return false
            // }
        }

        if (target.type === 'select-one') {
            setData(prev => ({ ...prev, [target.dataset.view]: target.selectedOptions[0].text }))
            if (target.name == 'jenis_cert_id') {
                setJenisCertId(target.value)
            }
        }

        setData(prev => ({ ...prev, [target.name]: target.value }))
    }

    const addTable = async () => {
        setTable(prev => ([...prev, data]))
        props.setSertifikat(prev => ([...prev, data]))
        setData([])
    }

    const removeTable = async (index, val) => {
        if (val.sertifikat.match(/(personil|pemeriksaan_kapal|evaluasi_pelimpahan|investigasi_insiden)/g)) {
            await Engine.POST(`sertifikat/${val.id}`, 'DELETE', null, TOKEN)
            .then(response => {
                console.log('delete sertifikat',response)
            })
        }
        const newData = [...table]
        newData.splice(index, 1)
        props.setSertifikat(newData)
        setTable(prev => ([...prev, newData]))
    }

    const ImgUrl = (data) => {
        if(data.blob) {
            return data.blob
        }else if (data.sertifikat) {
            if (data.sertifikat.includes('personil')) {
                return `${backEnd}${data.sertifikat}`
            }
            return data.sertifikat
        }else {
            return data.sertifikat
        }
    }

    console.log('props sertifikat',props.sertifikat)
    console.log('sertifikat data',data)

	return (
        <React.Fragment>
            {props.action !== 'detail' &&
        		<div className="card mb-5">
                    <div className="card-body">
                        <h3 className="font-weight-bold mb-3">Sertifikat</h3>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Jenis</label>
                                    <select name="jenis_cert_id" 
                                        onChange={dataHandle} 
                                        value={data.jenis_cert_id || ''}
                                        data-view={'jenis_cert'}
                                        className="form-control bg-white"
                                        disabled={props.disabled}
                                    >
                                        <option value="">Pilih</option>
                                        {jenisCert.map((v,i) => {
                                            return(<option key={i} value={v.id}>{v.nama}</option>)
                                        })}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label>Kategori</label>
                                    <select name="tipe_cert_id" 
                                        onChange={dataHandle} 
                                        value={data.tipe_cert_id || ''}
                                        data-view={'tipe_cert'}
                                        className="form-control bg-white"
                                        disabled={props.disabled}
                                    >
                                        <option value="">Pilih</option>
                                        {tipeCert.map((v,i) => {
                                            return(<option key={i} value={v.id}>{v.nama}</option>)
                                        })}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <label>Nomor Sertifikat</label>
                                    <input type="text" name="no_sertifikat" 
                                        onChange={dataHandle} 
                                        value={data.no_sertifikat || ''}
                                        className="form-control bg-white" 
                                        disabled={props.disabled}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Lembaga yang Mengeluarkan</label>
                                    <input type="text" name="issuer" 
                                        onChange={dataHandle} 
                                        value={data.issuer || ''}
                                        className="form-control bg-white" 
                                        disabled={props.disabled}
                                    />
                                </div>
                                <div className="form-group">
                                    <button 
                                        type="button" 
                                        className="btn btn-primary"
                                        onClick={addTable}
                                        disabled={jenisCertId !== "6" ? Object.keys(data).length < 10 : Object.keys(data).length < 9}
                                    >Tambahkan</button>
                                </div>
                            </div>
                            {console.log(Object.keys(data))}
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Tanggal Terbit</label>
                                    {/*<input 
                                        type="date" 
                                        name="tanggal_keluar_sertifikat" 
                                        onChange={dataHandle} 
                                        value={
                                            data.tanggal_keluar_sertifikat 
                                            ? moment(data.tanggal_keluar_sertifikat).format('YYYY-MM-DD')
                                            : ''
                                        }
                                        max={moment(new Date()).add(1, 'days').format('YYYY-MM-DD')}
                                        className="form-control bg-white" 
                                        disabled={props.disabled}
                                    />*/}
                                    <InputDate 
                                        name="tanggal_keluar_sertifikat" 
                                        dataHandle={dataHandle} 
                                        value={
                                            data.tanggal_keluar_sertifikat 
                                            ? moment(data.tanggal_keluar_sertifikat).format('YYYY-MM-DD')
                                            : ''
                                        } 
                                        max={moment(new Date()).format('YYYY-MM-DD')}
                                        disabled={props.disabled}
                                    />
                                </div>
                                
                                {jenisCertId !== "6" ? <div className="form-group">
                                    <label>Tanggal Kadaluarsa</label>
                                    {/*<input 
                                        type="date" 
                                        name="tanggal_expire" 
                                        onChange={dataHandle} 
                                        value={
                                            data.tanggal_expire
                                            ? moment(data.tanggal_expire).format('YYYY-MM-DD')
                                            : ''
                                        }
                                        min={moment(new Date()).add(1, 'days').format('YYYY-MM-DD')}
                                        className="form-control bg-white" 
                                        disabled={props.disabled}
                                    />*/}
                                    <InputDate 
                                        name="tanggal_expire" 
                                        dataHandle={dataHandle} 
                                        value={
                                            data.tanggal_expire
                                            ? moment(data.tanggal_expire).format('YYYY-MM-DD')
                                            : ''
                                        } 
                                        min={moment(new Date()).add(1, 'days').format('YYYY-MM-DD')}
                                        disabled={props.disabled}
                                    />
                                </div> : ''}
                                <div className="form-group">
                                    <label className="d-block">Sertifikat</label>
                                        <Image data={data} dataHandle={dataHandle} name="sertifikat" required={false}/>
                                        {<span className="text-danger d-block mt-2">{errMessage}</span>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            <div className="card mb-5">
                <div className="card-body">
                    <div className="row">
                        <div className="table-responsive">
                            <table className="table table-hover w-100">
                                <thead>
                                    <tr>
                                        <th>Jenis</th>
                                        <th>Kategori</th>
                                        <th>Nomor</th>
                                        <th>Lembaga</th>
                                        <th>Tanggal Terbit</th>
                                        <th>Tanggal Kadaluarsa</th>
                                        <th>File</th>
                                        {props.action !== 'detail' &&
                                            <th>Aksi</th>
                                        }
                                    </tr>
                                </thead>
                                <tbody>
                                    {table.map((val, i) => {
                                        return (
                                            <tr key={i}>
                                                <td>{val.jenis_cert}</td>
                                                <td>{val.tipe_cert}</td>
                                                <td>{val.no_sertifikat}</td>
                                                <td>{val.issuer}</td>
                                                <td>{moment(val.tanggal_keluar_sertifikat).format('DD-MMM-YYYY')}</td>
                                                <td>{jenisCertId !== "6" ? moment(val.tanggal_expire).format('DD-MMM-YYYY') : '-'}</td>
                                                <td>
                                                    <OverlayTrigger
                                                            placement="top"
                                                            overlay={
                                                                <Tooltip id={`tooltip-sertifikat-${i}`} className="tooltip-dark">Lihat</Tooltip>
                                                            }
                                                        >
                                                            <a 
                                                                href={ImgUrl(val)} 
                                                                target="_blank" className="badge badge-light">
                                                                <i className="far fa-file-alt text-danger"></i>
                                                            </a>
                                                    </OverlayTrigger>
                                                </td>
                                                {props.action !== 'detail' &&
                                                    <td>
                                                        <OverlayTrigger
                                                            placement="top"
                                                            overlay={
                                                                <Tooltip id={`tooltip-sertifikat-${i}`} className="tooltip-dark">Hapus</Tooltip>
                                                            }
                                                        >
                                                            <a className="badge badge-light" onClick={() => removeTable(i, val)}>
                                                                <i className="far fa-window-close text-danger"></i>
                                                            </a>
                                                        </OverlayTrigger>
                                                    </td>
                                                }
                                            </tr>
                                        )                                    
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
	)
}

const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
        const fileReader = new FileReader()
        fileReader.readAsDataURL(file)
        // fileReader.readAsText(file)
        fileReader.onload = () => {
            resolve(fileReader.result)
        }
        fileReader.onerror = (error) => {
            reject(error)
        }
    })
}