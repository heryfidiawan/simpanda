import React from "react"
import moment from 'moment'

import { Image } from "./Image"

export function Skppp(props) {
	return (
		<div className="card mb-5">
            <div className="card-body">
                <h3 className="font-weight-bold mb-3">SKPPP</h3>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Tanggal Mulai <span className="text-red">*</span></label>
                            <input type="date" name="skpp_tanggal_mulai" 
                                onChange={props.dataHandle} 
                                value={moment(props.data.skpp_tanggal_mulai).format('YYYY-MM-DD')}
                                className="form-control bg-white" 
                                required={true}
                                disabled={props.disabled}
                            />
                        </div>
                         <div className="form-group">
                            <label className="d-block">Unggah SK {props.data['skpp'] ? "" : <span className="text-red">*</span>}</label>
                                <div className={`upload-wrapper d-block`}>
                                    {props.action !== 'detail' &&
                                        <input
                                            type="file"
                                            name="skpp" 
                                            onChange={props.dataHandle}
                                            required={props.data['skpp'] ? false : true}
                                        />
                                    }
                                    <Image data={props.data} name="skpp"/>
                                </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Tanggal Selesai <span className="text-red">*</span></label>
                            <input type="date" name="skpp_tanggal_selesai" 
                                onChange={props.dataHandle} 
                                value={moment(props.data.skpp_tanggal_selesai).format('YYYY-MM-DD')}
                                className="form-control bg-white" 
                                required={true}
                                disabled={props.disabled}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
	)
}