import React, { useState } from "react"
import DatePicker, { registerLocale } from "react-datepicker"
import id from "date-fns/locale/id"
import "react-datepicker/dist/react-datepicker.css"
import moment from 'moment'

export function InputDate(props) {
    registerLocale("id", id)
    return (
        <DatePicker
            locale="id"
            className={`form-control bg-white ${props.className}`}
            selected={props.value ? new Date(props.value) : ''} 
            onChange={
                date => {
                    props.dataHandle({
                        target: {
                            name: props.name, 
                            value: moment(date).format('YYYY-MM-DD'),
                            dataset: {
                                table: props.dataTable, 
                                view: props.dataView
                            }
                        }
                    })
                    if (props.handleCustom) {
                        props.handleCustom({
                            target: {
                                name: props.name, 
                                value: moment(date).format('YYYY-MM-DD'),
                                dataset: {
                                    table: props.dataTable, 
                                    view: props.dataView
                                }
                            }
                        }, 
                        props.config, 
                        props.data)
                    }
                }
            } 
            dayClassName={date =>
                // console.log('nama hari',moment(date).format('dddd'))
                moment(date).format('dddd') === 'Sunday' || moment(date).format('dddd') === 'Saturday' ? "text-danger" : ''
            }
            dateFormat="dd-MM-yyyy"
            minDate={new Date(props.min)}
            maxDate={new Date(props.max)}
            required={props.required}
            disabled={props.disabled}
            readOnly={props.readOnly}
            name={props.name}
            id={props.id}
        />
    )
}