import React, { useState, useEffect } from "react"
import { Modal } from "react-bootstrap"

export function Remark(props) {

    const handleClose = () => {
        props.modalHandle(false)
    }

    const handleShow = () => {
        props.modalHandle(true)
    }

    // useEffect(() => {
    //     if (props.remark.length > 0) {
    //         props.confirmHandle(true)
    //     }else {
    //         props.confirmHandle(false)
    //     }
    // }, [props.remark])

    // console.log('Modal Remark',props.modal)
    // console.log('Remark',props.remark)
    // console.log('Remark length',props.remark.length)

    return (
        <Modal show={props.modal} onHide={handleClose} backdrop="static" keyboard={false}>
            <Modal.Header className="modal-header-custom" closeButton>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="text-center">
                    <img src="/media/icons/request-frame.png" alt="frame" className="margin-auto"/>
                    <h3 className="text-primary mt-5">Tuliskan Keterangan</h3>
                    <textarea name="remark" value={props.remark} onChange={props.remarkHandle} 
                        rows="5" className="form-control" placeholder="Tuliskan keterangan"
                    ></textarea>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <button type="button" className="btn btn-secondary" onClick={handleClose}>
                    Batalkan
                </button>
                <button type="button" disabled={props.remark.length > 0 ? false : true} className="btn btn-primary" onClick={props.submitHandler}>
                    Simpan
                </button>
            </Modal.Footer>
        </Modal>
    )
}