import React, { useState, useEffect } from "react"

import { APIS } from "../config/Engine"
import { OverlayTrigger, Tooltip } from "react-bootstrap"

const iconUplod = '/media/icons/upload.png'
const pdfThumb  = '/media/simpanda/pdf-thumb.png'
const backEnd   = APIS

export function Image(props) {
    // console.log('props image',backEnd+props.data[props.name])
    // console.log('props image',props.data)
    // console.log('props image',props)
    console.log('URL',window.location.pathname.split("/")[3])

    const ACTION = window.location.pathname.split("/")[3]
    const [tempData, setTempData] = useState([])

    const previewHandle = (e) => {
        if (e.target.files[0]) {
            setTempData(prev => ({ ...prev, [e.target.name]: URL.createObjectURL(e.target.files[0]) }))
        }else {
            delete tempData[e.target.name]
        }
    }

    const Html = () => {
        let preview = ''
        let url = iconUplod

        if (props.data[props.name] !== null 
            && props.data[props.name] !== 'null' 
            && props.data[props.name] !== '' 
            && props.data[props.name] !== undefined ) {
            if (props.data[props.name].includes('data:image')) {
                url = props.data[props.name]
                preview = tempData[props.name]
            }else if ( props.data[props.name].match(/(personil|pemeriksaan_kapal|evaluasi_pelimpahan|investigasi_insiden)/g) ) {
                url = pdfThumb
                if (props.data[props.name].match(/\.(jpg|png|gif)/g)) {
                    url = `${backEnd}${props.data[props.name]}`
                }
                preview = `${backEnd}${props.data[props.name]}`
            }else {
                url = pdfThumb
                preview = tempData[props.name]
            }            
        }else {
            url = iconUplod
        }

        // console.log('blob image',tempData)

        return (
            <div className={`upload-wrapper d-block`}>
                <img 
                    src={url} 
                    className="upload-preview img-thumbnail"
                    alt="preview" 
                />
                <div className="upload-overlay"></div>
                {ACTION !== 'detail' &&
                    <a href="#" className="icon-upload bg-secondary p-3 ml-1">
                        <i className="fa fa-upload text-primary"></i>
                    </a>
                }
                <OverlayTrigger
                    placement="right"
                    overlay={
                        <Tooltip id={`tooltip-show-${props.name}`} className="tooltip-dark">
                            {ACTION !== 'detail' ? 'Lihat' : 'Lihat dan Unduh'}
                        </Tooltip>
                    }
                >
                    <a 
                        href={preview || '#'} 
                        target={preview ? '_blank' : ''}
                        className="icon-show bg-secondary p-3 ml-1"
                        style={ACTION !== 'detail' ? {top: '33%'} : {width: '110px', height: '125px', left: '0', top: '0', opacity: '0'}}
                    >
                        {ACTION !== 'detail' && <i className="fa fa-eye text-primary"></i>}
                    </a>
                </OverlayTrigger>
                {ACTION !== 'detail' &&
                    <OverlayTrigger
                        placement="right"
                        overlay={
                            <Tooltip id={`tooltip-upload-${props.name}`} className="tooltip-dark">Upload</Tooltip>
                        }
                    >
                        <input
                            type="file"
                            name={props.name} 
                            onChange={e => {
                                props.dataHandle(e)
                                previewHandle(e)
                            }}
                            required={props.required}
                            data-table={props.dataTable}
                            data-view={props.dataView}
                        /> 
                    </OverlayTrigger>
                }
            </div>
        )
    }

	return (
		<Html />
	)
}