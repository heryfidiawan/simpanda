import React from 'react'
import moment from 'moment'
import { OverlayTrigger, Tooltip } from "react-bootstrap"

export function FormTable(props) {
    // console.log('props.result',props.result)
	return (
		<div className="table-responsive">
            <table className="table table-hover w-100">
                <thead>
                    <tr>
                        {props.content.thead.map((head, index) => {
                            return (
                                <th key={index}>{head}</th>
                            )
                        })}
                        {props.action !== 'detail' && <td>Aksi</td>}
                    </tr>
                </thead>
                <tbody>
                	{Object.keys(props.result).length === 0 || props.result.length > 0 && props.result.map((val, index) => {
                        return (
                            <tr key={index}>
                                {props.content && props.content.tbody.map((tb, i) => {
                                    return (
                                    	<td key={i}>
                                    		{tb === 'sertifikat' ? 
                                                <OverlayTrigger
                                                    placement="top"
                                                    overlay={
                                                        <Tooltip id={`tooltip-${index}`} className="tooltip-dark">Lihat</Tooltip>
                                                    }
                                                >
                                        			<a href={
                                                        val[tb].match(/(personil|pemeriksaan_kapal|evaluasi_pelimpahan|investigasi_insiden)/g) ? `${props.backEnd}${val[tb]}` : val[tb]
                                                    } target="_blank" className="badge badge-light">
                                                        <i className="far fa-file-alt text-danger"></i>
                                                    </a>
                                                </OverlayTrigger>
                                    			: tb === 'tanggal_keluar_sertifikat' || tb === 'tanggal_expire' ? moment(val[tb]).format('DD-MMM-YYYY') : val[tb]
                                    		}
                                    	</td> 
                                    )
                                })}
                                {props.action !== 'detail' &&
                                    <td>
                                        <OverlayTrigger
                                            placement="top"
                                            overlay={
                                                <Tooltip id={`tooltip-sertifikat-${index}`} className="tooltip-dark">Hapus</Tooltip>
                                            }
                                        >
                                            <a className="badge badge-light" onClick={() => props.removeTable(index, val)}>
                                                <i className="far fa-window-close text-danger"></i>
                                            </a>
                                        </OverlayTrigger>
                                    </td>
                                }
                            </tr>
                        )
                	})}
                </tbody>
            </table>
        </div>
	)
}