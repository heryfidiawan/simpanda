/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import moment from 'moment';
import {Link} from "react-router-dom";

export function BreadCrumbs({items}) {
    if (!items || !items.length) {
      return "";
    }

    return (
        <ul className="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2">
        {items.length > 1 &&
            <li className="breadcrumb-item">
                <Link to="/dashboard" className="text-muted">
                    {/*<i className="flaticon2-shelter text-muted icon-1x" />*/}
                    Dashboard
                </Link>
            </li>
        }
        {
            items.length > 1 ?
                items.map((item, index) => (
                    <li key={`bc${index}`} className="breadcrumb-item">
                        <Link className="text-muted" to={{ pathname: item.pathname }}>
                            {item.title}
                        </Link>
                    </li>
                ))
            : <span className="text-muted">{moment().format("DD MMMM YYYY")}</span>
        }
        </ul>
    );
}
