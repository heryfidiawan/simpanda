import React, {useMemo} from "react";
import {Link} from "react-router-dom";
import {NavLink}  from "react-router-dom";
import objectPath from "object-path";
import SVG from "react-inlinesvg";
import {useHtmlClassService} from "../../_core/MetronicLayout";
import {toAbsoluteUrl} from "../../../_helpers";
import { shallowEqual, useSelector } from "react-redux";

export function Brand() {
  const uiService = useHtmlClassService();

  const layoutProps = useMemo(() => {
    return {
      brandClasses: uiService.getClasses("brand", true),
      asideSelfMinimizeToggle: objectPath.get(
          uiService.config,
          "aside.self.minimize.toggle"
      ),
      headerLogo: uiService.getLogo(),
      headerStickyLogo: uiService.getStickyLogo()
    };
  }, [uiService]);

    const {isAuthorized} = useSelector(
        ({auth}) => ({
            // isAuthorized: auth.user != null,
            isAuthorized: auth.user,
        }),
        shallowEqual
    );

    // console.log('isAuthorized Brand',isAuthorized)

  return (
    <>
      {/* begin::Brand */}
      <div
          className={`brand flex-column-auto ${layoutProps.brandClasses}`}
          id="kt_brand"
      >
        {/* begin::Logo */}
        <Link to="" className="brand-logo mt-5">
          <img alt="logo" src={layoutProps.headerLogo}/>
        </Link>
        {/* end::Logo */}

        {layoutProps.asideSelfMinimizeToggle && (
          <>
            {/* begin::Toggle */}
            <button className="brand-toggle btn btn-sm px-0 mt-5" id="kt_aside_toggle">
              <span className="svg-icon svg-icon-xl">
                  <SVG src={toAbsoluteUrl("/media/svg/icons/Navigation/Angle-double-left.svg")}/>
              </span>
            </button>
            {/* end::Toolbar */}
            </>
        )}
      </div>
      {/* end::Brand */}
      <span className="brand-border mx-5 mt-5"></span>
      <div className="auth-user mx-5 mt-5 p-3">
          <NavLink to="/dashboard">
            <div className="row">
                <div className="col-md-4">
                  <img src="/media/users/100_10.jpg" alt="user" className="rounded-circle mt-2 img-auth-user" />
                </div>
                <div className="col-md-8 pl-0 auth-info">
                    <h6 className="text-white mt-5">{isAuthorized.nama}</h6>
                    {/*<span className="text-muted">Staff Admin</span>*/}
                </div>
            </div>
          </NavLink>
      </div>
      </>
  );
}
