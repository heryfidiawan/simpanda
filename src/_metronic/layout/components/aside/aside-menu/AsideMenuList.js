/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, {useState} from "react";
import {useLocation} from "react-router";
import {NavLink}  from "react-router-dom"
import { shallowEqual, useSelector } from "react-redux";
// import SVG from "react-inlinesvg";
// import {toAbsoluteUrl, checkIsActive} from "../../../../_helpers";
import {checkIsActive} from "../../../../_helpers";

export function AsideMenuList({ layoutProps }) {
    const location = useLocation();
    const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
        ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
        : "";
    };

    const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)
    // console.log('isAuthorized Aside Menu',isAuthorized)
    
    const [handle, setHandle] = useState([]) 
    const [parents, setParents] = useState([])
    let temp = {}

    isAuthorized.menu.map(menu => {
        if (handle.indexOf(menu.parent) === -1 && menu.config !== null) {
            setHandle([...handle, menu.parent])
            temp.id = menu.id
            temp.nama = menu.parent
            temp.icon = menu.icon
            setParents([...parents, temp])
        }
    })

    const menuparent = parents.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
    const menus = isAuthorized.menu.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))

    // console.log('handle',handle)
    // console.log('parent menu',parents)

    return (
        <React.Fragment>
            {/* begin::Menu Nav */}
            <ul className={`menu-nav ${layoutProps.ulClasses}`}>
                {/*begin::1 Level*/}
                {isAuthorized.menu.map((menu, mn) => {
                    return(
                        <React.Fragment key={mn}>
                        {menu.config === null &&
                            <li
                                className={`menu-item ${getMenuItemActive(menu.url, false)}`}
                                aria-haspopup="true"
                            >
                                <NavLink className="menu-link" to="/dashboard">
                                <span className="svg-icon menu-icon">
                                    <img src="/media/icons/dashboard.png" alt="dashboard"/>
                                </span>
                                    <span className="menu-text">{menu.parent}</span>
                                </NavLink>
                            </li>
                        }
                        </React.Fragment>
                    )
                })}

                {menuparent.map((parent, pi) => {
                    return(
                        <li
                            className={`menu-item menu-item-submenu ${getMenuItemActive(parent.nama, true)}`}
                            aria-haspopup="true"
                            data-menu-toggle="hover"
                            key={pi}
                        >
                            <NavLink className="menu-link menu-toggle" to={parent.nama}>
                                <span className="svg-icon menu-icon">
                                  <img src={`/media/icons/${parent.icon}`} alt="group" />
                                </span>
                                <span className="menu-text">{parent.nama}</span>
                                <i className="menu-arrow"/>
                            </NavLink>
                            <div className="menu-submenu ">
                                <ul className="menu-subnav">
                                    {menus.map((menu, mn) => {
                                        return(
                                            <React.Fragment key={mn}>
                                            {menu.config !== null && menu.parent === parent.nama &&
                                                <li
                                                    className={`menu-item ${getMenuItemActive(menu.url)}`}
                                                    aria-haspopup="true"
                                                >
                                                    <NavLink className="menu-link" to={`${menu.url}`}>
                                                        <i className="menu-bullet menu-bullet-dot">
                                                            <span/>
                                                        </i>
                                                        <span className="menu-text">{menu.nama}</span>
                                                    </NavLink>
                                                </li>
                                            }
                                            </React.Fragment>
                                        )
                                    })}
                                </ul>
                            </div>
                        </li>
                    )
                })}
                {/*end::1 Level*/}

                {/* Administrator */}
                {/*begin::1 Level*/}
                {isAuthorized.cabang_id < 1 &&
                    <li
                        className={`menu-item menu-item-submenu ${getMenuItemActive("/administrator", true)}`}
                        aria-haspopup="true"
                        data-menu-toggle="hover"
                    >
                        <NavLink className="menu-link menu-toggle" to="/administrator">
                            <span className="svg-icon menu-icon">
                                <img src="/media/icons/administrator.png" alt="administrator" />
                            </span>
                            <span className="menu-text">Administrator</span>
                            <i className="menu-arrow"/>
                        </NavLink>
                        <div className="menu-submenu ">
                            <ul className="menu-subnav">

                                <li
                                    className={`menu-item ${getMenuItemActive("/administrator/group-user")}`}
                                    aria-haspopup="true"
                                >
                                    <NavLink className="menu-link" to="/administrator/group-user">
                                        <i className="menu-bullet menu-bullet-dot">
                                            <span/>
                                        </i>
                                        <span className="menu-text">User Group</span>
                                    </NavLink>
                                </li>
                                
                                <li
                                    className={`menu-item ${getMenuItemActive("/administrator/user")}`}
                                    aria-haspopup="true"
                                >
                                    <NavLink className="menu-link" to="/administrator/user">
                                        <i className="menu-bullet menu-bullet-dot">
                                            <span/>
                                        </i>
                                        <span className="menu-text">User</span>
                                    </NavLink>
                                </li>

                            </ul>
                        </div>
                    </li>
                }
                {/*end::1 Level*/}

                {/* end::Administrator */}
                <li
                    className={`menu-item menu-item-submenu`}
                    aria-haspopup="true"
                    data-menu-toggle="hover"
                >
                    <NavLink className="menu-link menu-toggle" to="/logout">
                        <span className="svg-icon menu-icon">
                            <i className="fas fa-arrow-left text-warning"></i>
                        </span>
                        <span className="menu-text text-warning">Keluar</span>
                    </NavLink>
                </li>

            </ul>
            {/* end::Menu Nav */}
        </React.Fragment>
  );
}
