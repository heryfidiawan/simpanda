import {
    LOGIN_URL,
    ME_URL,
    REGISTER_URL,
    REQUEST_PASSWORD_URL
} from "../_redux/authCrud";
import userTableMock from "./userTableMock";
import { Engine } from "../../../../simpanda/config/Engine";

export default function mockAuth(mock) {
    // mock.onPost(LOGIN_URL).reply( async ({ data }) => {
    //     const { username, password } = JSON.parse(data);
    //     // console.log('mockAuth data',data)
    //     let user = false
        
    //     if (username && password) {
            
    //         await Engine.PostData('login', {username: username, password: password})
    //         .then(data => {
    //             console.log('mockAuth LOGIN',data)
    //             user = data
    //             if (data.status !== false) {
    //                 userTableMock.push(user)
    //                 return [200, { ...user,password: undefined  }];
    //             }

    //             return [200, { ...user, accessToken: '"Password salah lebih dari 3, untuk reset silahkan hubungi Kantor Pusat"'}];
    //             // user.accessToken = "access-token-" + Math.random()
    //             // user.refreshToken = "access-token-"+ Math.random()
    //             // userTableMock.push(user)
    //         })
    //         .catch(err => console.log('err',err))
    //         console.log('mockAuth USER',user)

            
    //     }

    //     return [200, { ...user, accessToken: "Password salah lebih dari 3, untuk reset silahkan hubungi Kantor Pusat" }];
    // });


    mock.onPost(LOGIN_URL).reply( async ({ data }) => {
        const { username, password } = JSON.parse(data);
        // console.log('mockAuth data',data)

        let message = {}
        if (username && password) {
            let user = false
            await Engine.PostData('login', {username: username, password: password})
            .then(data => {
                console.log('mockAuth then',data)
                if (data.accessToken) {
                    user = data
                    
                }else{
                    message = data
                    console.log("data",data)
                }
                
                // user.accessToken = ""access-token-"" + Math.random()
                // user.refreshToken = ""access-token-""+ Math.random()
                // userTableMock.push(user)
            })
            .catch(err => {
                console.log('mockAuth catch',err)
                return [200, {accessToken: err}]
            })

            if(user){
                userTableMock.push(user)
                return [200, { ...user, password: undefined }];
            }else{
                
                return [200, {accessToken: message}]
            }
        }

        console.log('di luar',message)
        // return [200, {accessToken: message}]
        return [400];
    });

    mock.onPost(REGISTER_URL).reply(({ data }) => {
        const { email, nama, username, password } = JSON.parse(data);

        if (email && nama && username && password) {
            const user = {
                id: generateUserId(),
                email,
                nama,
                username,
                password,
                roles: [2], // Manager
                accessToken: "access-token-" + Math.random(),
                refreshToken: "access-token-" + Math.random(),
                pic: process.env.PUBLIC_URL + "/media/users/default.jpg"
            };

            userTableMock.push(user);

            return [200, { ...user, password: undefined }];
        }

        return [400];
    });

    mock.onPost(REQUEST_PASSWORD_URL).reply(({ data }) => {
        const { email } = JSON.parse(data);

        if (email) {
            const user = userTableMock.find(
                x => x.email.toLowerCase() === email.toLowerCase()
            );

            if (user) {
                user.password = undefined;

                return [200, { ...user, password: undefined }];
            }
        }

        return [400];
    });

    mock.onGet(ME_URL).reply(({ headers: { Authorization } }) => {
    const accessToken =
        Authorization &&
        Authorization.startsWith("Bearer ") &&
        Authorization.slice("Bearer ".length);

        if (accessToken) {
            const user = userTableMock.find(x => x.accessToken === accessToken);

            if (user) {
                return [200, { ...user, password: undefined }];
            }
        }

        return [401];
    });

    function generateUserId() {
        const ids = userTableMock.map(el => el.id);
        const maxId = Math.max(...ids);
        return maxId + 1;
    }
  
}
