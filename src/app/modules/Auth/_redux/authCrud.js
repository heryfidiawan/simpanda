import axios from "axios";
import {APIS} from "../../../../simpanda/config/Engine";

export const LOGIN_URL = APIS+"login";
export const REGISTER_URL = APIS+"register";
export const REQUEST_PASSWORD_URL = APIS+"forgot-password";

export const ME_URL = APIS+"/user";

export function login(username, password) {
	const data = { username: username, password: password}
	console.log('AuthCrud login', data)
	return axios.post(LOGIN_URL, data);
}

export function register(email, fullname, username, password) {
 	return axios.post(REGISTER_URL, { email, fullname, username, password });
}

export function requestPassword(email) {
	return axios.post(REQUEST_PASSWORD_URL, { email });
}

export function getUserByToken() {
	// Authorization head should be fulfilled in interceptor.
	return axios.get(ME_URL);
}
