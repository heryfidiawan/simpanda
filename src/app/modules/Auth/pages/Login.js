import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { FormattedMessage, injectIntl } from "react-intl";
import * as auth from "../_redux/authRedux";
import { login } from "../_redux/authCrud";
import * as mock from "../__mocks__/mockAuth";

// import { loadCaptchaEnginge, LoadCanvasTemplate, LoadCanvasTemplateNoReload, validateCaptcha } from 'react-simple-captcha';
import { loadCaptchaEnginge, LoadCanvasTemplate, LoadCanvasTemplateNoReload, validateCaptcha } from "../../../../simpanda/config/captcha"

// import ReCAPTCHA from "react-google-recaptcha";
// import { Engine } from "../../../../simpanda/config/Engine";

/*
  INTL (i18n) docs:
  https://github.com/formatjs/react-intl/blob/master/docs/Components.md#formattedmessage
*/

/*
  Formik+YUP:
  https://jaredpalmer.com/formik/docs/tutorial#getfieldprops
*/

const initialValues = {username: '',password: ''}


// function chaptcha(value) {
//     console.log("Captcha value:", value)
// }

function Login(props) {

const [fail, setFail] = useState([])
    const { intl } = props
    const {isGoing} = useState(false)
    const [loading, setLoading] = useState(false)
    const LoginSchema = Yup.object().shape({
        username: Yup.string()
        .min(3, "Minimal 3 karakter")
        .max(9, "Maksimal 9 karakter")
        .required(
            intl.formatMessage({
                id: "AUTH.VALIDATION.REQUIRED_FIELD",
            })
        ),
        password: Yup.string()
        .min(6, "Minimal 6 karakter")
        .max(50, "Maksimal 50 karakter")
        .required(
            intl.formatMessage({
                id: "AUTH.VALIDATION.REQUIRED_FIELD",
            })
        ),
    })

    const enableLoading = () => {
        setLoading(true)
    }

    const disableLoading = () => {
        setLoading(false)
    }

    const getInputClasses = (fieldname) => {
        if (formik.touched[fieldname] && formik.errors[fieldname]) {
            return "is-invalid"
        }

        if (formik.touched[fieldname] && !formik.errors[fieldname]) {
            return "is-valid"
        }

        return ""
    }
    
    useEffect(() => {
        loadCaptchaEnginge(6); 
    }, [])

    const formik = useFormik({
        initialValues,
        validationSchema: LoginSchema,
        onSubmit: (values, { setStatus, setSubmitting }) => {
           let user_captcha = document.getElementById('user_captcha_input').value;
	 
	       if (validateCaptcha(user_captcha)==false){
	           document.getElementById('user_captcha_input').value = "";
	           return false;
	       }
            enableLoading()
            setTimeout(() => {

                login(values.username, values.password)
                .then(({ data: { accessToken } }) => {
                    disableLoading();
                    
                    if (accessToken.status === false) {
                    	document.getElementById('user_captcha_input').value = "";
                    	loadCaptchaEnginge(6);
                    	setFail(accessToken)
                    }else{
                        setFail({})
                        props.login(accessToken);
                    }
                   
                    // setStatus(
                    //             intl.formatMessage({
                    //                 id: "AUTH.VALIDATION.FAILURE",
                    //             })
                    //         )
                })
                // .then(data => {
                //     console.log('data login bawaan', data)
                //     disableLoading()
                //     props.login(data.accessToken)
                // })
                .catch((e) => {
                    console.log('accessToken Login JS',e)
                    disableLoading()
                    setSubmitting(false)
                    setStatus(
                        intl.formatMessage({
                            id: "AUTH.VALIDATION.INVALID_LOGIN",
                        })
                    )
                    

                    // setFail(parseInt(fail) + 1)
                    // if (fail < 3) {
                    //     setStatus(
                    //         intl.formatMessage({
                    //             id: "AUTH.VALIDATION.INVALID_LOGIN",
                    //         })
                    //     )
                    // }else{
                    //     setStatus(
                    //         intl.formatMessage({
                    //             id: "AUTH.VALIDATION.FAILURE",
                    //         })
                    //     )
                    // }
                    
                    
                })

            }, 1000)
        },
    })

    return (
        <div className="login-form login-signin" id="kt_login_signin_form">
            {/* begin::Head */}
            <div className="text-left mb-10 mb-lg-20">
                <h3 className="font-size-h1">
                    <FormattedMessage id="AUTH.LOGIN.TITLE" />
                </h3>
                <p className="text-muted font-weight-bold">
                    SISTEM INFORMASI PEMANDUAN DAN PENUNDAAN
                </p>
            </div>
            {/* end::Head */}

            {/*begin::Form*/}
            <form onSubmit={formik.handleSubmit} className="form fv-plugins-bootstrap fv-plugins-framework">
                {fail.status === false ? (
                    <div className="mb-10 alert alert-custom alert-light-danger alert-dismissible">
                        <div className="alert-text font-weight-bold">{formik.status}{fail.message}</div>
                    </div>
                ) : ""}                

                <div className="form-group fv-plugins-icon-container">
                    <input placeholder="Username" type="text"
                        className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses("username")}`}
                        name="username" {...formik.getFieldProps("username")}/>
                    {formik.touched.username && formik.errors.username ? (
                        <div className="fv-plugins-message-container">
                            <div className="fv-help-block">{formik.errors.username}</div>
                        </div>
                    ) : null}
                </div>

                <div className="form-group fv-plugins-icon-container">
                    <input placeholder="Password" type="password"
                        className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses("password")}`}
                        name="password" {...formik.getFieldProps("password")}/>
                    {formik.touched.password && formik.errors.password ? (
                        <div className="fv-plugins-message-container">
                            <div className="fv-help-block">{formik.errors.password}</div>
                        </div>
                    ) : null}
                </div>

                <div className="form-group fv-plugins-icon-container">
                    <LoadCanvasTemplate />
                </div>

                <div className="form-group fv-plugins-icon-container">
                    <input placeholder="Username" type="text"
                        className={`form-control form-control-solid h-auto py-5 px-6`}
                        placeholder="Enter Captcha Value" id="user_captcha_input" name="user_captcha_input" />                    
                </div>

                {/*<div className="form-group d-flex flex-wrap justify-content-between align-items-center">
                    <label>
                        <input name="isGoing" type="checkbox" className="text-dark-50 text-hover-primary my-3 mr-2"
                            checked={isGoing} onClick={() => !isGoing}/>
                        Remember Me
                    </label>

                    <Link to="/auth/forgot-password" className="text-dark-50 text-hover-primary my-3 mr-2" id="kt_login_forgot">
                        <FormattedMessage id="AUTH.GENERAL.FORGOT_BUTTON" />
                    </Link>
                </div>*/}
            
                {/*<div className="text-center recaptcha " >
                    <ReCAPTCHA
                        sitekey="6LfeUykaAAAAAI3yKHigbOeAx9XjDsyqgr95tc_n"
                        size="normal"
                        hl="en"
                        onChange={chaptcha}
                    />
                </div>*/}

                <button id="kt_login_signin_submit" type="submit"
                    className={`btn btn-primary font-weight-bold px-4 py-4 pr-0 my-3`}>
              
                    <span className="pr-15">
                        Masuk
                        {loading && <span className="ml-3 spinner spinner-white"></span>}
                    </span>
                    <i className="fas fa-arrow-right"></i>
                </button>
            </form>
            {/*end::Form*/}
        </div>
    )
}

export default injectIntl(connect(null, auth.actions)(Login))
