// const initialState = {
//   primary: "#2269FF",
//   textBlack: "#202023",
//   textGray: "#A3A3A3",
//   textDark: "#636363",
//   textWhite1: "#FFFFFF",
//   textWhite2: "#CBDAF9",
//   textDisable: "#CCCCCC",
//   backgroundGray: "#F4F5F7",
//   backgroundWhite: "#FFFFFF",

// };

// export const global = initialState;



import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  primary: "#2269FF",
  textBlack: "#202023",
  textBlack2: "#636363",
  textGray: "#A3A3A3",
  textDark: "#636363",
  textWhite1: "#FFFFFF",
  textWhite2: "#CBDAF9",
  textDisable: "#CCCCCC",
  backgroundGray: "#F4F5F7",
  backgroundWhite: "#FFFFFF",
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const global = createSlice({
  name: "global",
  initialState: initialState,
});
