import React, { useEffect } from "react";
import { useSubheader } from "../../_metronic/layout";
import engine from "../../simpanda/config/Engine";

// export const MyPage = (props) => {
export function MyPage(props) {
  console.log(props.match.params.url);
  const suhbeader = useSubheader();
  suhbeader.setTitle(
    "Page with route param type = " +
      props.match.params.type +
      " and key = " +
      props.match.params.key
  );

  useEffect(() => {
    engine.actions
      .getDetails(props.match.params.url, props.match.params.key)
      .then((data) => {
        if (props.match.params.key !== "null") {
          console.log(data);
          console.log("view & edit");
        } else {
          console.log("create");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  return <>My Page</>;
}
