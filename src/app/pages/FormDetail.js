import React, { useState, useEffect } from "react"
import { useHistory } from "react-router-dom"
import { OverlayTrigger, Tooltip } from "react-bootstrap"

import { useSubheader } from "../../_metronic/layout"
import { APIS, Engine } from "../../simpanda/config/Engine"
import { DetailConfig } from "../../simpanda/config/detail"
import { FormTable } from "../../simpanda/components/FormTable"
import { Remark } from "../../simpanda/components/Remark"
import { shallowEqual, useSelector } from "react-redux"
import { Image } from "../../simpanda/components/Image"
import { InputDate } from "../../simpanda/components/InputDate"

import Swal from "sweetalert2";
import moment from 'moment';

export function FormDetail(props) {
    const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)

    const TOKEN  = isAuthorized.accessToken
    const PARENT = props.match.url.split("/")[1]
    const MENU   = props.match.params.menu.includes('stasiun-pandu') ? 'stasiun_pandu_and_equipment' : props.match.params.menu.replace("-", "_")
    const ACTION = props.match.url.split("/")[3]
    const KEY    = props.match.params.key
    const TITLE  = MENU.replace("_", " ")

    const history 	= useHistory()
    const suhbeader = useSubheader()
    suhbeader.setTitle(ACTION+' '+TITLE)

    const PARENTURL = `${PARENT}/${props.match.params.menu}`
    const CONFIG    = DetailConfig[PARENT][MENU]
    const backEnd   = APIS
    const iconUplod = '/media/icons/upload.png'
    const pdfThumb  = '/media/simpanda/pdf-thumb.png'
    const optionurl = []

    const [data, setData] = useState([])
    const [other, setOther] = useState([])
    const [otherTemp, setOtherTemp] = useState([])
    const [tableTemp, setTableTemp] = useState([])
    const [tableView, setTableView] = useState([])
    const [option, setOption] = useState([])
    const [activityLog, setActivityLog] = useState([])

    const getOptions = async () => {
        if (optionurl.length > 0) {
            for (let i = 0; i < optionurl.length; i++) {
                await Engine.GET(optionurl[i], TOKEN)
                .then(data => {
                    setOption(current => {
                        return({ ...current, [optionurl[i]]: data })
                    })
                }).catch(e => console.log(e) )
            }
        }
    }

    useEffect(() => {
        CONFIG["card"].map(card => {
            for (let i = 0; i < card.content.length; i++) {
                card.content[i].map(content => {
                    if (ACTION === 'create') {
                        !content.table && content.type !== 'table' && setData(prev => ({...prev, [content.name]: ''}))
                        content.table && content.name && setTableTemp(prev => ({...prev, [content.name]: ''}))
                    }
                    if (content.url && optionurl.indexOf(content.url) === -1) {
                        optionurl.push(content.url)
                    }
                })
            }
        })
        getOptions()
        if (ACTION !== "create") {
            getData()
        }
        validation()
    }, [CONFIG, KEY, ACTION, validType, PARENT])
  
    const [remark, setRemark] = useState('')
    const [modal, setModal] = useState(false)

    const modalHandle = (e) => {
        setModal(e)
    }

    const remarkHandle = (e) => {
        setRemark(e.target.value)
    }

    // const cekNamaPersonil = async (data) => {
    // 	const nama = data.nama, nipp = data.nipp
    // 	await Engine.GET(`personil?nama=${nama}&nipp=${nipp}`, TOKEN)
    //     .then(async (data) => {
    //     	console.log("kemari 1")
    //     	if (data.length > 0){
    //     		console.log("kemari 2")
    //     		const Toast = Swal.mixin({
    //                 toast: true,
    //                 position: 'center',
    //                 showConfirmButton: false,
    //                 timer: 3000,
    //                 timerProgressBar: true,
    //             })
                
    //             return Toast.fire({
    //                 background: '#F4F5F7',
    //                 type: 'error',
    //                 title: `Nama ${nama} dengan NIPP ${nipp} sudah terdaftar.`
    //             })
    //     	}
    //     }).catch(e => console.log(e) )
    // }

    // console.log('Remark',remark)
    // console.log('modal',modal)

    const submitHandler = async (e) => {
        e.preventDefault()

        // if (MENU == "pandu" || MENU == "pendukung_pandu"){
        // 	cekNamaPersonil(data);
        // }

        if (CONFIG['modal'] && modal === false) {
            setModal(true)
            return false
        }

        delete data['activityLog']
        data[CONFIG['other']] = other
        Object.assign(data, CONFIG['defaultValue'])

        if (CONFIG['modal']) {
            Object.assign(data, {'activity_keterangan': remark})
        }
        data.approval_status_id = 0;
        // console.log("SUBMIT DATAnya ya ", data)

        

        const method = ACTION === 'create' ? 'POST' : 'PUT'
        const URL    = ACTION === 'create' ? CONFIG["url"] : CONFIG["url"]+'/'+KEY

        await Engine.POST(URL, method, data, TOKEN)
        .then(response => {
            console.log('FORM SUBMIT',data)
            if (response) {
                history.push(`/${PARENTURL}`)
            }
        })
    }
    const [reqs, setReqs] = useState('none')
    const [mandatory, setMandatory] = useState(false)
    const dataHandle = async (e) => {
        const target = e.target
        let valueData = target.value
        let valueTemp = target.value        

        if (target.type === 'file') {
            if (target.files[0] && target.files[0].size <= 15000000) {
                valueData = await convertBase64(target.files[0])
                valueTemp = URL.createObjectURL(target.files[0])
            }else{
            	const Toast = Swal.mixin({
                    toast: true,
                    position: 'center',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                })
                
                return Toast.fire({
                    background: '#F4F5F7',
                    type: 'error',
                    title: "File upload yang diperbolehkan maksimal 15 mb"
                })
            }
        }
        
        if (target.type === 'select-one') {
            valueTemp = target.selectedOptions[0].text
            if(MENU === 'stasiun_pandu_and_equipment'){
                if(target.name === 'tipe_asset_id'){
                    if(target.value === '4'){
                        if (document.getElementsByTagName('kategori_equipment')  || document.getElementsByTagName('lokasi_pemegang') || document.getElementsByTagName('total_aktif')) {
                            setReqs('none')
                        }else{
                            setReqs('')
                        }
                    }else if(target.value === '5'){
                        if (document.getElementsByTagName('alamat') || document.getElementsByTagName('nilai_perolehan') ) {
                            setReqs('none')
                        }else{
                            setReqs('')
                        }
                    }else{
                        setReqs('')
                    }
                }

            }else if(MENU === 'rumah_dinas'){
                if(target.name === 'status_kepemilikan'){
					const no_assetI = document.getElementsByName('no_asset')[0].parentNode;
					const tahun_perolehanI = document.getElementsByName('tahun_perolehan')[0].parentNode;
					const nilai_perolehanI = document.getElementsByName('nilai_perolehan')[0].parentNode;
					const nilai_bukuI = document.getElementsByName('nilai_buku')[0].parentNode;
					if(target.value === 'Sewa'){
                        no_assetI.style.display = 'none';
						tahun_perolehanI.style.display = 'none';
						nilai_perolehanI.style.display = 'none';						
						nilai_bukuI.style.display = 'none';						
                    }else{
                        no_assetI.style.display = 'block';
						tahun_perolehanI.style.display = 'block';
						nilai_perolehanI.style.display = 'block';						
						nilai_bukuI.style.display = 'block';						
                    }
                }

            }
        }

        if (target.name === 'nipp') {
            if (parseInt(target.value.length) <= 9) {
                setErrMessage(prev => ({...prev, 'max': null}))
            }else {
                setErrMessage(prev => ({...prev, 'max': 'NIPP maksimal 9 digit'}))
				return false;
            }
        }

        if (target.dataset.table !== undefined) {
            setOtherTemp({ ...otherTemp, [target.name]: valueData })
            if (target.dataset.view !== undefined) {
                setTableTemp({ ...tableTemp, [target.dataset.view]: valueTemp })
            }else {
                setTableTemp({ ...tableTemp, [target.name]: valueTemp })
            }
        }else {
            setData(prev => ({ ...prev, [target.name]: valueData }))
        }

        if (target.dataset.parent) {
            await getChildOptions(data, target.dataset.child_url, target.dataset.param, target.value)
        }
    }

    const [childOption, setChildOption] = useState([])
    const getChildOptions = async (data, url, param, attr) => {
        param = param.split(',')
        let ulrParam = `/?${param[0]}=${attr}`
        if (param.length == 2) {
            ulrParam += `&${param[1]}=${data[param[1]]}`
        }

        await Engine.GET(url+ulrParam, TOKEN)
        .then(data => {
            setChildOption(data)
        }).catch(e => console.log(e) )
    }

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader()
            fileReader.readAsDataURL(file)
            fileReader.onload = () => {
                resolve(fileReader.result)
            }
            fileReader.onerror = (error) => {
                reject(error)
            }
        })
    }

    const convertRupiah = (angka, prefix) => {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
          split  = number_string.split(","),
          sisa   = split[0].length % 3,
          rupiah = split[0].substr(0, sisa),
          ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
          let separator = sisa ? "." : "";
          rupiah += separator + ribuan.join(".");
        }

        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
    }

    const addTable = () => {
        setOther([...other, otherTemp])
        setTableView([...tableView, tableTemp])

        setOtherTemp([])
        setTableTemp([])
        Array.from(document.getElementsByClassName("table")).forEach(
            (input) => (input.value = "")
        )
    }

    const removeTable = async (index, val) => {
        console.log("DELETE ", index)
        console.log("remove table ", val.sertifikat)
        if (val.sertifikat.match(/(personil|pemeriksaan_kapal|evaluasi_pelimpahan|investigasi_insiden)/g)) {
            await Engine.POST(`sertifikat/${val.id}`, 'DELETE', null, TOKEN)
            .then(response => {
                console.log('delete sertifikat',response)
            })
        }
        const newTable = [...tableView]
        newTable.splice(index, 1)
        setTableView(newTable)

        const newOther = [...other]
        newOther.splice(index, 1)
        setOther(newOther)
    }
    
    const getData = async () => {
        await Engine.GET(`${CONFIG["url"]}/${KEY}`, TOKEN)
        .then(async (data) => {
            setData(data)
            setTableView(data[CONFIG['other']])
            if(MENU === 'stasiun_pandu_and_equipment' ){
                
                if (parseInt(data.tipe_asset_id) === 4  ) {
                    setReqs('none')
                }else if (parseInt(data.tipe_asset_id) === 5  ){
                    setReqs('none')
                }else{
                    setReqs('')
                }
            }
            
            

            ACTION === 'edit' && setOther(data[CONFIG['other']])
            ACTION === 'detail' && setActivityLog(data['activityLog'])
            
        }).catch(e => console.log(e) )
    }

   

    // console.log('URL', CONFIG['url'])
    console.log("DATA", data)
    console.log("OTHER", other)
    console.log("OTHER TEMP", otherTemp)
    // console.log("TABLE TEMP", tableTemp)
    // console.log("TABLE VIEW", tableView)
    // console.log("activityLog", activityLog)

    const [errMessage, setErrMessage] = useState(false)

    const handleCustom = (event, config, data) => {
        const target = event.target
        if (config.match !== undefined) {
            if (target.value == data[config.match]) {
                setErrMessage(prev => ({...prev, 'match': null}))
            }else {
                setErrMessage(prev => ({...prev, 'match': config.label + ' tidak sesuai'}))
            }
        }

        if (config.min !== undefined) {
            if (parseInt(target.value.length) >= parseInt(config.min)) {
                setErrMessage(prev => ({...prev, 'min': null}))
            }else {
                setErrMessage(prev => ({...prev, 'min': config.label + ' minimal 6 digit'}))
            }
        }

        if (config.max !== undefined) {
            if (parseInt(target.value.length) <= parseInt(config.max)) {
                setErrMessage(prev => ({...prev, 'max': null}))
            }else {
                setErrMessage(prev => ({...prev, 'max': config.label + ' maksimal '+config.max+' digit'}))
            }
        }

        if (config.kombinasi !== undefined) {
            if (!/([A-Z]+)/g.test(target.value) || !/([0-9]+)/g.test(target.value)) {
                setErrMessage(prev => ({...prev, 'kombinasi': config.label + ' harus kombinasi minimal 1 huruf Kapital & 1 Angka'}))
            }else{
                setErrMessage(prev => ({...prev, 'kombinasi': null}))
            }
        }

        if (config.divider) {
            if (!isNaN(target.value)) {
                setData(prev => ({ ...prev, [config.divider]: target.value / config.pembagi }))
            }
        }
        if (config.className == 'decimal') {
            setData(prev => ({ ...prev, [target.name]: convertRupiah(target.value) }))
        }
    }

    // console.log('errMessage',errMessage)

    const readonly = ACTION === "detail" ? true : false
    
    const [validType, setValidType] = useState(['create','edit','detail'])
    const validation = () => {
        if (isAuthorized.cabang_id > 0 && PARENT === 'administrator') {
            setValidType([])
        }
        if (isAuthorized.cabang_id < 1 && PARENT !== 'administrator') {
            setValidType(['detail'])
        }
        validType.includes(ACTION) === false && history.push('/error/error-v1')
    }

  

    // console.log('PARENT',PARENT)
    // console.log('MENU',MENU)
    // console.log('CONFIG',CONFIG)

    if (CONFIG === undefined) {
        window.location.href = '/error/error-v1'
    }

    return (
        <React.Fragment>
            {CONFIG !== undefined &&
                <React.Fragment>
                    {CONFIG['modal'] && 
                        <Remark 
                            modal={modal} 
                            modalHandle={modalHandle} 
                            remarkHandle={remarkHandle} 
                            remark={remark}
                            submitHandler={submitHandler}
                        />
                    }
                    <form onSubmit={submitHandler}>
                        {CONFIG["card"].map((a, b) => {
                            return (
                                <div key={b} className="row">
                                    <div className="col-md-12">
                                        {a.type !== "button" && a.type !== "submit" && a.action.includes(ACTION) && (
                                            <div className="card p-5 mb-5">
                                                <h3>{a.title}</h3>
                                                <div className="row">
                                                    {a.type === "form_input" && a.content.map((c, d) => {
                                                        return (
                                                            <div key={d} className={"col-md-" + 12 / a.content.length}>
                                                                {c.map((e, f) => {
                                                                    return (
                                                                        <React.Fragment key={f}>
                                                                            {e.type === "table" && data && (
                                                                                <FormTable
                                                                                    backEnd={backEnd}
                                                                                    action={ACTION}
                                                                                    content={e.content}
                                                                                    result={tableView}
                                                                                    removeTable={removeTable}
                                                                                />
                                                                            )}
                                                                            <div className="form-group" style={
                                                                                MENU === 'stasiun_pandu_and_equipment' ? parseInt(data.tipe_asset_id) === 4 && (e.name === 'kategori_equipment' || e.name === 'lokasi_pemegang' || e.name === 'total_aktif') 
                                                                                        ? {display:`${reqs}`} 
                                                                                            : parseInt(data.tipe_asset_id) === 5 && (e.name === 'alamat' || e.name === 'nilai_perolehan' ) 
                                                                                        ? {display:`${reqs}`} 
                                                                                    : {}
                                                                                : {}
                                                                            }>
                                                                                <label htmlFor={e.type} className="d-block">{e.label} {e.required ? <span className="text-red">*</span> : ''}</label>

                                                                                {e.type === "text" && (                                                                        
                                                                                    <input
                                                                                        type="text"
                                                                                        name={e.name}
                                                                                        value={
                                                                                            e.value 
                                                                                            ? e.value
                                                                                            : e.table 
                                                                                                ? tableTemp[e.name] ?? '' : data[e.name] ?? ''
                                                                                        }
                                                                                        data-table={e.table}
                                                                                        data-view={e.view}
                                                                                        onChange={ev => {
                                                                                            dataHandle(ev)
                                                                                            handleCustom(ev, e, data)
                                                                                        }}
                                                                                        required={
                                                                                            MENU === 'stasiun_pandu_and_equipment' 
                                                                                                ? parseInt(data.tipe_asset_id) === 4 && (e.name === 'lokasi_pemegang') 
                                                                                                    ? false 
                                                                                                        : parseInt(data.tipe_asset_id) === 5 && (e.name === 'nomor_sertifikat' || e.name === 'tempat_keluar') 
                                                                                                    ? false
                                                                                                : e.required 
                                                                                            : e.required
                                                                                        }
                                                                                        readOnly={MENU === 'kapal' && ACTION === 'create' ? false : e.readonly || readonly}
                                                                                        className={`form-control ${e.className}`}
                                                                                    />
                                                                                )}

                                                                                {e.type === "group-append" && (
                                                                                    <div className="input-group">
                                                                                        <input
                                                                                            type="text"
                                                                                            name={e.name}
                                                                                            value={
                                                                                                e.value 
                                                                                                ? e.value
                                                                                                : e.table ? tableTemp[e.name] ?? '' : data[e.name] ?? ''
                                                                                            }
                                                                                            data-table={e.table}
                                                                                            data-view={e.view}
                                                                                            onChange={ev => {
                                                                                                dataHandle(ev)
                                                                                                handleCustom(ev, e, data)
                                                                                            }}
                                                                                            required={e.required}
                                                                                            readOnly={e.readonly || readonly}
                                                                                            className={`form-control ${e.className}`}
                                                                                        />
                                                                                        <div className="input-group-append">
                                                                                            <span className="input-group-text">{e.grouptext}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                )}
                                                                                {e.type === "number" && (
                                                                                    <input
                                                                                        type="text"
                                                                                        name={e.name}
                                                                                        value={
                                                                                            e.value 
                                                                                            ? e.value
                                                                                            : e.table ? tableTemp[e.name] ?? '' : data[e.name] ?? ''
                                                                                        }
                                                                                        data-table={e.table}
                                                                                        data-view={e.view}
                                                                                        onChange={ev => {
                                                                                            dataHandle(ev)
                                                                                            handleCustom(ev, e, data)
                                                                                        }}
                                                                                        required={
                                                                                            MENU === 'stasiun_pandu_and_equipment' 
                                                                                                ? parseInt(data.tipe_asset_id) === 5 && e.name === 'nilai_perolehan' 
                                                                                                    ? false 
                                                                                                        :  parseInt(data.tipe_asset_id) === 4 && e.name === 'total_aktif' 
                                                                                                    ? false 
                                                                                                : e.required 
                                                                                            : e.required
                                                                                        }
                                                                                        readOnly={e.readonly || readonly}
                                                                                        className={`form-control ${e.className}`}
                                                                                    />
                                                                                )}
                                                                                {e.type === "email" && (
                                                                                    <input
                                                                                        type="email"
                                                                                        name={e.name}
                                                                                        value={
                                                                                            e.value 
                                                                                            ? e.value
                                                                                            : e.table ? tableTemp[e.name] ?? '' : data[e.name] ?? ''
                                                                                        }
                                                                                        data-table={e.table}
                                                                                        data-view={e.view}
                                                                                        onChange={ev => {
                                                                                            dataHandle(ev)
                                                                                            handleCustom(ev, e, data)
                                                                                        }}
                                                                                        required={e.required}
                                                                                        readOnly={e.readonly || readonly}
                                                                                        className={`form-control ${e.className}`}
                                                                                    />
                                                                                )}
                                                                                {e.type === "password" && (
                                                                                    <input
                                                                                        type="password"
                                                                                        name={e.name}
                                                                                        value={
                                                                                            e.table ? tableTemp[e.name] ?? '' : data[e.name] ?? ''
                                                                                        }
                                                                                        data-table={e.table}
                                                                                        data-view={e.view}
                                                                                        onChange={ev => {
                                                                                            dataHandle(ev)
                                                                                            handleCustom(ev, e, data)
                                                                                        }}
                                                                                        required={ACTION === 'create' ? true : false}
                                                                                        readOnly={e.readonly || readonly}
                                                                                        className={`form-control ${e.className}`}
                                                                                    />
                                                                                )}
                                                                                {e.type === "select" && (
                                                                                    <select
                                                                                        name={e.name}
                                                                                        value={e.table ? otherTemp[e.name] : data[e.name]}
                                                                                        data-table={e.table}
                                                                                        data-view={e.view}
                                                                                        data-parent={e.parent}
                                                                                        data-child_url={e.child_url}
                                                                                        data-param={e.param}
                                                                                        onChange={dataHandle}
                                                                                        required={MENU === 'stasiun_pandu_and_equipment' ? 
                                                                                                    reqs === 'none' && parseInt(data.tipe_asset_id) === 4 && (e.name === 'kategori_equipment') ? 
                                                                                                    false 
                                                                                                    : e.required 
                                                                                                : e.required}
                                                                                        disabled={readonly}
                                                                                        readOnly={e.readonly || readonly}
                                                                                        className={`form-control ${e.className}`}
                                                                                    >
                                                                                        <option value="">Pilih</option>
                                                                                        {e.option ? e.option.map((vv, ii) => {
                                                                                            return (
                                                                                                <option key={ii} value={vv[e.val]}>{vv.name}</option>
                                                                                            )
                                                                                        }) : !e.child ? option.hasOwnProperty(e.url) && option[e.url].map((v,i) => {
                                                                                            return (
                                                                                                <option key={i} value={v.id}>{v[e.op]}</option>
                                                                                            )
                                                                                        }) : childOption.map((v,i) => {
                                                                                            return (
                                                                                                <option key={i} value={v.id}>{v[e.op]}</option>
                                                                                            )
                                                                                        })}
                                                                                    </select>
                                                                                )}
                                                                                {e.type === "date" && (
                                                                                    <InputDate 
                                                                                        name={e.name} 
                                                                                        dataHandle={dataHandle} 
                                                                                        handleCustom={handleCustom}
                                                                                        value={
                                                                                            e.value 
                                                                                            ? e.value
                                                                                            : e.table
                                                                                                ? otherTemp[e.name]
                                                                                                    ? moment(otherTemp[e.name]).format('YYYY-MM-DD')
                                                                                                : '' 
                                                                                                : data[e.name] 
                                                                                                    ? moment(data[e.name]).format('YYYY-MM-DD')
                                                                                                : '' 
                                                                                        } 
                                                                                        required={e.required}
                                                                                        readOnly={e.readonly || readonly}
                                                                                        min={e.minDate}
                                                                                        max={e.maxDate}
                                                                                        dataTable={e.table}
                                                                                        dataView={e.view}
                                                                                        className={e.className}
                                                                                        data={data}
                                                                                        config={e}
                                                                                    />
                                                                                )}
                                                                                {e.type === "time" && (
                                                                                    <input
                                                                                        type="time"
                                                                                        name={e.name}
                                                                                        value={
                                                                                            e.value 
                                                                                            ? e.value
                                                                                            : e.table ? tableTemp[e.name] ?? '' : data[e.name] ?? ''
                                                                                        }
                                                                                        data-table={e.table}
                                                                                        data-view={e.view}
                                                                                        onChange={ev => {
                                                                                            dataHandle(ev)
                                                                                            handleCustom(ev, e, data)
                                                                                        }}
                                                                                        required={e.required}
                                                                                        readOnly={e.readonly || readonly}
                                                                                        className={`form-control ${e.className}`}
                                                                                    />
                                                                                )}
                                                                                {e.type === "textarea" && (
                                                                                    <textarea
                                                                                        rows="5"
                                                                                        name={e.name}
                                                                                        value={
                                                                                            e.value 
                                                                                            ? e.value
                                                                                            : data[e.name]
                                                                                        }
                                                                                        data-table={e.table}
                                                                                        data-view={e.view}
                                                                                        onChange={ev => {
                                                                                            dataHandle(ev)
                                                                                            handleCustom(ev, e, data)
                                                                                        }}
                                                                                        required={MENU === 'stasiun_pandu_and_equipment' ? parseInt(data.tipe_asset_id) === 5 && e.name === 'alamat' ? false : e.required : e.required }
                                                                                        readOnly={e.readonly || readonly}
                                                                                        className={`form-control ${e.className}`}
                                                                                    ></textarea>
                                                                                )}

                                                                                {e.match_child && <span className="text-danger d-block mt-2">{errMessage.match}</span>}
                                                                                {e.min && <span className="text-danger d-block mt-2">{errMessage.min}</span>}
                                                                                {e.max && <span className="text-danger d-block mt-2">{errMessage.max}</span>}
                                                                                {e.kombinasi && <span className="text-danger d-block mt-2">{errMessage.kombinasi}</span>}

                                                                                {e.type === "file" && (
                                                                                    <Image 
                                                                                        data={e.table ? otherTemp : data} 
                                                                                        dataHandle={dataHandle} 
                                                                                        name={e.name} 
                                                                                        dataTable={e.table}
                                                                                        dataView={e.view}
                                                                                        required={RequiredImage(ACTION, MENU, data, e)}
                                                                                    />
                                                                                )}
                                                                                {e.type === "button" && (
                                                                                    <button 
                                                                                        type={e.type} 
                                                                                        className={e.className} 
                                                                                        onClick={addTable}
                                                                                        disabled={Object.keys(otherTemp).length < e.length}
                                                                                    >
                                                                                        {/*{Object.keys(otherTemp).length} - {e.length}*/}
                                                                                        {e.text}
                                                                                    </button>
                                                                                )}
                                                                            </div>
                                                                        </React.Fragment>
                                                                    )
                                                                })}
                                                            </div>
                                                        )
                                                    })}
                                                    {a.type === "table" && data && (
                                                        <FormTable
                                                            backEnd={backEnd}
                                                            action={ACTION}
                                                            content={a.content}
                                                            result={tableView}
                                                            removeTable={removeTable}
                                                        />
                                                    )}
                                                </div>
                                            </div>
                                        )}
                                        {a.type === "submit" && a.action.includes(ACTION) && 
                                            <button 
                                                type={a.type} 
                                                className={a.content.className}
                                                disabled={
                                                    a.validation && a.validationMode.includes(ACTION)
                                                    ? errMessage.match === null && errMessage.min === null 
                                                    ? false
                                                    : true
                                                    : false
                                                }
                                            >
                                                {a.content.text}
                                            </button>
                                        }
                                    </div>
                                </div>
                            )
                        })}
                        {ACTION === 'detail' && CONFIG['more'] && activityLog !== undefined && (
                            <div className="card p-5">
                                <h3>{CONFIG['more']}</h3>
                                <div className="scroll-custom" style={{'maxHeight':'230px','overflowY':'auto'}}>
                                    <div className="col-12 card bg-light p-5">
                                        {activityLog.map((v,i) => {
                                            return(
                                                <div key={i} className="row mb-1">
                                                    <div className="col-md-2 border-right">
                                                        <p className="pt-3 font-weight-bold">{moment(v.date).format('DD/MMM/YYYY')}</p>
                                                        <p className="ml-3">{moment(v.date).format('HH:mm:ss')}</p>
                                                    </div>
                                                    <div className="col-md-10">
                                                        <p className="pt-3">{v.remark}</p>
                                                        <p>{v.keterangan}</p>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        )}
                    </form>
                </React.Fragment>
            }
        </React.Fragment>
    )
}

const RequiredImage = (ACTION, MENU, data, config) => {
    // ACTION === 'edit' ? false 
    // : MENU === 'stasiun_pandu_and_equipment' ? 
    //     parseInt(data.tipe_asset_id) === 5 && e.name === 'sertifikat_equipment' ? 
    //     true 
    //     : e.required 
    // : e.required
    if (data[config.name]) {
        return false
    }else if(MENU === 'stasiun_pandu_and_equipment') {
        if (parseInt(data.tipe_asset_id) === 5 && config.name === 'sertifikat_equipment') {
            return config.required
        }else {
            return config.required
        }
    }else {
        return config.required
    }
}
