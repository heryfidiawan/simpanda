import React, { useEffect, useState } from "react"
import DatePicker, { registerLocale } from "react-datepicker"
import id from "date-fns/locale/id"

import { Engine } from "../../../simpanda/config/Engine"

import moment from 'moment'

export function PilotShipAvailability({auth}) {

    registerLocale("id", id)
    const TOKEN  = auth.accessToken
    const [cabangMst, setCabangMst] = useState([])

    const getCabang = async () => {
        await Engine.GET('cabang', TOKEN)
        .then(data => {
            setCabangMst(data)
        }).catch(e => console.log(e))
    }

    useEffect(() => {
        getCabang()
    }, [])

    // 1 = Bandar 2 = Laut
    const [panduLaut, setPanduLaut] = useState([])
    const [panduBandar, setPanduBandar] = useState([])
    const [armada, setArmada] = useState([])
    const [totalArmada, setTotalArmada] = useState(0)
    const [loading, setLoading] = useState(false)

    const [cabang, setCabang] = useState(auth.cabang_id)
    const [date, setDate] = useState(moment(new Date()).format('YYYY-MM-DD'))

    const getPanduBandar = async () => {
        setLoading(true)
        await Engine.GET(`pandujaga/?pandu_bandar_laut_id=1&date=${date}&kehadiran=1&cabang_id=${cabang}&approval_status_id=1`, TOKEN)
        .then(data => {
            setPanduBandar(data)
            setLoading(false)
        }).catch(e => console.log(e))
    }

    const getPanduLaut = async () => {
        setLoading(true)
        await Engine.GET(`pandujaga/?pandu_bandar_laut_id=2&date=${date}&kehadiran=1&cabang_id=${cabang}&approval_status_id=1`, TOKEN)
        .then(data => {
            setPanduLaut(data)
            setLoading(false)
        }).catch(e => console.log(e))
    }

    const getArmada = async () => {
        setLoading(true)
        setTotalArmada(0)
        // console.log(`armadajaga/?date=${date}`)
        // &available=1
        await Engine.GET(`armadajaga/?date=${date}&cabang_id=${cabang}&enable=1&approval_status_id=1`, TOKEN)
        .then(data => {
            setArmada(data)
            
            let check = []
            data.map(armada => {
                if (moment.duration(moment(armada.to).diff(armada.from)).asHours() <= 23) {
                    if (check.indexOf(armada.asset_kapal_id) === -1) {
                        check.push(armada.asset_kapal_id)
                        setTotalArmada(prev => {
                            return prev + 1
                        })
                    }
                }
            })

            setLoading(false)
        }).catch(e => console.log(e))
    }

    useEffect(() => {
        if (date) {
            getPanduBandar()
            getPanduLaut()
            getArmada()
        }
    }, [cabang, date])


    let uniqArmada = []

    const Armada = ({armada, uniqArmada}) => {
        let html = []

        armada.map((arm, ar) => {    
            if (moment.duration(moment(arm.to).diff(arm.from)).asHours() <= 23) {
                if (uniqArmada.indexOf(arm.asset_kapal_id) === -1) {
                    uniqArmada.push(arm.asset_kapal_id)
                    html.push(<div key={ar} className="row border-bottom py-2 mx-0">
                        <div className="col">
                           <img src="/media/icons/boat.png" alt="icon" width="25" className="mr-2" />
                        </div>
                        <div className="col-10">{arm.nama_asset}</div>
                    </div>)
                }
            }
        })

        return html
    }

    // console.log('uniqArmada',uniqArmada)
    // console.log('panduLaut',panduLaut)
    // console.log('panduBandar',panduBandar)
    // console.log('armada',armada)

	return(
		<div className="row mt-3">
            <div className="col-6">
                <h2>Ketersediaan Pandu & Armada</h2>
            </div>
            <div className="col-6 mb-3 d-flex">
                <select id="cabang" className="form-control mr-1" value={cabang} 
                    onChange={(e) => setCabang(e.target.value)} >
                    {cabangMst.map((val, i) => {
                        return(
                            val.id > 0 && <option value={val.id} key={i}>{val.nama}</option>
                        )
                    })}
                </select>
                {/*<input type="date" id="date" className="form-control" value={date} onChange={(e) => setDate(e.target.value)} />*/}
                <DatePicker 
                    locale="id"
                    className="form-control"
                    selected={date ? new Date(date) : new Date()} 
                    onChange={date => setDate(moment(date).format('YYYY-MM-DD'))} 
                    dayClassName={date =>
                        // console.log('nama hari',moment(date).format('dddd'))
                        moment(date).format('dddd') === 'Sunday' || moment(date).format('dddd') === 'Saturday' ? "text-danger" : ''
                    }
                    dateFormat="dd-MM-yyyy"
                />
            </div>
            <div className="col-lg-4 col-md-6 mb-3">
                <div className="card card-custom px-4 py-5">
                    <h4>Pandu Bandar Jaga ({panduBandar.length})</h4>
                    <hr/>
                    <div className="pilot-card scroll-custom">
                        {loading
                            ? <div className="spinner spinner-warning dashboard-spinner"></div>
                            : panduBandar.map((bandar, bn) => {
                            return(
                                <div key={bn} className="row border-bottom py-2 mx-0">
                                    <div className="col">
                                       <img src="/media/svg/avatars/001-boy.svg" alt="icon" width="25" className="mr-2" />
                                    </div>
                                    <div className="col-10">{bandar.nama}</div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
            <div className="col-lg-4 col-md-6 mb-3">
                <div className="card card-custom px-4 py-5">
                    <h4>Pandu Laut Jaga ({panduLaut.length})</h4>
                    <hr/>
                    <div className="pilot-card scroll-custom">
                        {loading
                            ? <div className="spinner spinner-warning dashboard-spinner"></div>
                            : panduLaut.map((laut, lt) => {
                            return(
                                <div key={lt} className="row border-bottom py-2 mx-0">
                                    <div className="col">
                                       <img src="/media/svg/avatars/001-boy.svg" alt="icon" width="25" className="mr-2" />
                                    </div>
                                    <div className="col-10">{laut.nama}</div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
            <div className="col-lg-4 col-md-6 mb-3">
                <div className="card card-custom px-4 py-5">
                    <h4>Daftar Armada ({totalArmada})</h4>
                    <hr/>
                    <div className="pilot-card scroll-custom">
                        {loading
                            ? <div className="spinner spinner-warning dashboard-spinner"></div>
                            : <Armada armada={armada} uniqArmada={uniqArmada} />
                        }
                    </div>
                </div>
            </div>
        </div>
	)
}
