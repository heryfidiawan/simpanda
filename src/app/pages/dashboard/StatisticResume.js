import React, {useMemo, useEffect, useState} from "react";
import objectPath from "object-path";
import ApexCharts from "apexcharts";
import {Dropdown} from "react-bootstrap";
import {useHtmlClassService} from "../../../_metronic/layout";
import {DropdownMenu4, DropdownCustomToggler} from "../../../_metronic/_partials/dropdowns";
import {KTUtil} from "../../../_metronic/_assets/js/components/util";

import { Engine } from "../../../simpanda/config/Engine";

export function StatisticResume({ className, auth }) {

    const TOKEN  = auth.accessToken
    const uiService = useHtmlClassService()

    const layoutProps = useMemo(() => {
        return {
            colorsGrayGray100: objectPath.get(uiService.config, "js.colors.gray.gray100"),
            colorsGrayGray700: objectPath.get(uiService.config, "js.colors.gray.gray700"),
            colorsThemeBaseSuccess: objectPath.get(
                uiService.config,
                "js.colors.theme.base.success"
            ),
            colorsThemeLightSuccess: objectPath.get(
                uiService.config,
                "js.colors.theme.light.success"
            ),
            fontFamily: objectPath.get(uiService.config, "js.fontFamily")
        }
    }, [uiService])

    const labelsTemp = []
    const seriesTemp = []
    const [loading, setLoading] = useState(true)
    const [series, setSeries] = useState([])
    const [labels, setLabels] = useState([])
    
    const getStatisticResume = async () => {
        // console.log('LOADING...')
        setLoading(true)
        await Engine.GET('dashboard/statisticresume?cabang_id='+auth.cabang_id, TOKEN)
        .then(data => {
            data.map(data => {
                labelsTemp.push(data.nama)
                seriesTemp.push(data.total)
            })
            setSeries(seriesTemp)
            setLabels(labelsTemp)
            setLoading(false)
            // console.log('SELESAI')
        }).catch(e => console.log(e))
    }

    useEffect(() => {
        getStatisticResume()
        // console.log('labelsTemp',labelsTemp)
        // console.log('seriesTemp',seriesTemp)
        // console.log('labels',labels)
        // console.log('series',series)
    }, [])

    const Option = () => {
        const element = document.getElementById("StatisticResume")
        if (!element) {return}

        const height = parseInt(KTUtil.css(element, 'height'))
        const options = getChartOptions(layoutProps, height, series, labels)

        const chart = new ApexCharts(element, options)
        chart.render()
        return function cleanUp() {
            chart.destroy()
        }
    }

    useEffect(() => {
        if (!loading) {
            Option()
        }
    }, [loading])

    return (
        <div className={`card card-custom ${className}`}>
            <div className="card-header border-0 pt-5">
                <h3 className="card-title font-weight-bolder ">Statistik Motor Pandu</h3>
            </div>
            <div className="card-body d-flex flex-column">
                <div className="flex-grow-1">
                    {!loading && <div id="StatisticResume" style={{height: "360px", width: "100%"}}></div>}
                </div>
            </div>
        </div>
    )
}


function getChartOptions(layoutProps, height, series, labels) {
    const options = {
        series: series,
        labels: labels,
        chart: {
            height: height,
            type: 'donut',
        },
        legend: {
            position: 'bottom',
        },
        plotOptions: {
            pie: {
                donut: {
                    labels: {
                        show: true,
                        name: {
                            show: true,
                        },
                        value: {
                            show: true,
                        },
                        total: {
                            show: true,
                        },
                    },
                }
            }
        },
        dataLabels: {
            enabled: true,
        },
        tooltip: {
            custom: ({series, seriesIndex, dataPointIndex, w}) => {
                // console.log('series',series)
                // console.log('seriesIndex',seriesIndex)
                // console.log('dataPointIndex',dataPointIndex)
                // console.log('w',w)
                return '<div class="arrow_box" style="background-color: '+w.globals.colors[seriesIndex]+'; padding: 5px; font-size: 12px;">' + '<span>' 
                + w.config.labels[seriesIndex] + ': ' + series[seriesIndex] 
                + ' (' + Math.round(w.globals.seriesPercent[seriesIndex] * 10) / 10 + '%)'
                + '</span>' + '</div>'
            },
        },
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom'
                }
            }
        }],
    }
    return options
}
