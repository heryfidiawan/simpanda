import React, {useMemo, useEffect, useState} from "react"
import { useHistory } from "react-router-dom"
import objectPath from "object-path"
import ApexCharts from "apexcharts"
import {Dropdown} from "react-bootstrap"
import {useHtmlClassService} from "../../../_metronic/layout"
import {DropdownMenu4, DropdownCustomToggler} from "../../../_metronic/_partials/dropdowns"
import {KTUtil} from "../../../_metronic/_assets/js/components/util"

import { Engine } from "../../../simpanda/config/Engine"
import moment from 'moment'

export function CertivicateValidity({ className, auth }) {

    const TOKEN  = auth.accessToken
    const history = useHistory()
    const uiService = useHtmlClassService()

    const layoutProps = useMemo(() => {
        return {
            colorsGrayGray100: objectPath.get(uiService.config, "js.colors.gray.gray100"),
            colorsGrayGray700: objectPath.get(uiService.config, "js.colors.gray.gray700"),
            colorsThemeBaseSuccess: objectPath.get(
                uiService.config,
                "js.colors.theme.base.success"
            ),
            colorsThemeLightSuccess: objectPath.get(
                uiService.config,
                "js.colors.theme.light.success"
            ),
            fontFamily: objectPath.get(uiService.config, "js.fontFamily")
        }
    }, [uiService])

    const [pandu, setPandu] = useState([0,0,0])
    const [pendukung, setPendukung] = useState([0,0,0])
    const [kapal, setKapal] = useState([0,0,0])
    const [pelimpahan, setPelimpahan] = useState([0,0,0])

    const [loading, setLoading] = useState(true)

    const getCertivicate = async () => {
        // console.log('LOADING...')
        setLoading(true)
        await Engine.GET('dashboard/cetivicatevalidity?cabang_id='+auth.cabang_id, TOKEN)
        .then(data => {
            // console.log('DATA', data)
            data.personil.map(data => {
                let expired = moment(new Date(data.tanggal_expire)).valueOf()
                let tiga = moment(new Date()).add(3, 'months').valueOf()
                let tujuh = moment(new Date()).add(7, 'months').valueOf()
                let duabelas = moment(new Date()).add(12, 'months').valueOf()

                if (expired <= tiga) {
                    if (data.tipe_personil_id === 1) {
                        pandu[0] += 1
                    }
                    if (data.tipe_personil_id > 1) {
                        pendukung[0] += 1
                    }
                }

                if(expired > tiga && expired <= tujuh) {
                    if (data.tipe_personil_id === 1) {
                        pandu[1] += 1
                    }
                    if (data.tipe_personil_id > 1) {
                        pendukung[1] += 1
                    }
                }

                if (expired  > tujuh && expired <= duabelas) {
                    if (data.tipe_personil_id === 1) {
                        pandu[2] += 1
                    }
                    if (data.tipe_personil_id > 1) {
                        pendukung[2] += 1
                    }
                }
            })

            setPandu(pandu)
            setPendukung(pendukung)

            data.kapal.map(data => {
                let expired = moment(new Date(data.tanggal_expire)).valueOf()
                let tiga = moment(new Date()).add(3, 'months').valueOf()
                let tujuh = moment(new Date()).add(7, 'months').valueOf()
                let duabelas = moment(new Date()).add(12, 'months').valueOf()

                if (expired <= tiga) {
                    kapal[0] += 1
                }
                if(expired > tiga && expired <= tujuh) {
                    kapal[1] += 1
                }
                if (expired  > tujuh && expired <= duabelas) {
                    kapal[2] += 1
                }
            })
            
            setKapal(kapal)

            data.evaluasi.map(data => {
                let expired = moment(new Date(data.tanggal_expire)).valueOf()
                let tiga = moment(new Date()).add(3, 'months').valueOf()
                let tujuh = moment(new Date()).add(7, 'months').valueOf()
                let duabelas = moment(new Date()).add(12, 'months').valueOf()

                if (expired <= tiga) {
                    pelimpahan[0] += 1
                }
                if(expired > tiga && expired <= tujuh) {
                    pelimpahan[1] += 1
                }
                if (expired  > tujuh && expired <= duabelas) {
                    pelimpahan[2] += 1
                }
            })
            
            setPelimpahan(pelimpahan)

            setLoading(false)
            // console.log('SELESAI')
        }).catch(e => console.log(e))
    }

    useEffect(() => {
        getCertivicate()
        // console.log('pandu',pandu)
        // console.log('pendukung',pendukung)
        // console.log('kapal',kapal)
        // console.log('pelimpahan',pelimpahan)
        // console.log('GENERATE')
    }, [])

    const Option = () => {
        const element = document.getElementById("CertivicateValidity")
        if (!element) {
            return
        }

        const height = parseInt(KTUtil.css(element, 'height'))
        const options = getChartOptions(layoutProps, height, pandu, pendukung, kapal, pelimpahan, history)

        const chart = new ApexCharts(element, options)
        chart.render()
        return function cleanUp() {
            chart.destroy()
        }
    }

    useEffect(() => {
        if (!loading) {
            Option()
        }
        // console.log('pandu',pandu)
    }, [loading, pandu, pendukung, kapal, pelimpahan])

    return (
        <div className={`card card-custom ${className}`}>
            <div className="card-header border-0 pt-5">
                <h3 className="card-title font-weight-bolder ">Validasi Sertifikat</h3>
            </div>
            <div className="card-body d-flex flex-column pt-0">
                <div className="flex-grow-1">
                    {!loading && <div id="CertivicateValidity" style={{height: "360px", width: "100%"}}></div>}
                </div>
            </div>
        </div>
    )
}


function getChartOptions(layoutProps, height, pandu, pendukung, kapal, pelimpahan, history) {
    const options = {
        series: [
            {
                name: "Pandu",
                url: "sertifikat/pandu",
                data: pandu
            },
            {
                name: "Pendukung Pandu",
                url: "sertifikat/pendukung-pandu",
                data: pendukung
            },
            {
                name: "Kapal",
                url: "sertifikat/kapal",
                data: kapal
            },
            {
                name: "Pelimpahan dan Pemanduan",
                url: 'sertifikat/evaluasi-pelimpahan',
                data: pelimpahan
            }
        ],
        chart: {
            type: "bar",
            height: height,
            toolbar: {
              show: false
            },
            events: {
                click: function(event, chartContext, config) {
                    // console.log('config',config)
                    if (config.seriesIndex > -1) {
                        // console.log(config.config.series[config.seriesIndex]['name'])
                        // console.log(config.config.xaxis.categories[config.dataPointIndex])
                        let param = ['filter-1', 'filter-2', 'filter-3']
                        let url = config.config.series[config.seriesIndex]['url']
                        let label = param[config.dataPointIndex]
                        history.push(`/${url}/${label}`)
                    }
                }
            }
        },
        plotOptions: {
            bar: {
                borderRadius: 6,
                horizontal: false,
                columnWidth: "70%",
                // endingShape: "rounded"
            }
        },
        dataLabels: {
            enabled: false
        },
        theme: {
            mode: 'light',
            palette: 'palette10',
        },
        stroke: {
            show: true,
            width: 2,
            colors: ["transparent"]
        },
        xaxis: {
            categories: [
                '<= 3 Bulan', '3 - 7 Bulan', '7 - 12 Bulan'
            ],
            labels: {
                style: {
                    fontSize: "14px",
                    cssClass: ".apexcharts-margin"
                },
                hideOverlappingLabels: false,
                show: true,
                rotate: 0,
                rotateAlways: false,
                minHeight: 50,
                // maxHeight: 2000
            }
        },
        yaxis: {
            title: {
                // text: "$ (thousands)"
            }
        },
        fill: {
            opacity: 1
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    // return "$ " + val + " thousands"
                    return val
                }
            }
        }
    }
    return options;
}
