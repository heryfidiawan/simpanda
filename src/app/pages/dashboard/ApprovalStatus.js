import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { Engine } from "../../../simpanda/config/Engine";

export function ApprovalStatus({auth}) {

    const TOKEN  = auth.accessToken
	// Resource
    const [pandu, setPandu] = useState(0)
    const [pendukungPandu, setPendukungPandu] = useState(0)
    // Asset
    const [kapal, setKapal] = useState(0)
    const [station, setStation] = useState(0)
    const [rumah, setRumah] = useState(0)
    // Inpection
    const [saranaBantuPemandu, setSaranaBantuPemandu] = useState(0)
    const [pemeriksaanKapal, setPemeriksaanKapal] = useState(0)
    const [investigasiInsiden, setInvestigasiInsiden] = useState(0)
    const [evaluasiPelimpahan, setEvaluasiPelimpahan] = useState(0)

	const getResource = async () => {
        const url = 'personil/?&enable=0&enable=1&approval_status_id=0&approval_status_id=2'
		await Engine.GET(url, TOKEN)
		.then(data => {
			data.map((v,i) => {
				v.flag === 'pandu' && setPandu(current => {return current + 1})
				v.flag === 'pendukung' && setPendukungPandu(current => {return current + 1})
			})
        }).catch(e => console.log(e))
	}

	const getKapal = async () => {
        const url = 'assetkapal/?flag=kapal&enable=1&enable=0&approval_status_id=0&approval_status_id=2'
		await Engine.GET(url, TOKEN)
		.then(data => {
            data.map(result => { return setKapal(current => {return current + 1}) })
        }).catch(e => console.log(e))
	}

	const getStation = async () => {
        const url = 'assetstasiunequipment/?flag=stasiun&enable=1&enable=0&approval_status_id=0&approval_status_id=2'
		await Engine.GET(url, TOKEN)
		.then(data => {
            data.map(result => { return setStation(current => {return current + 1}) })
        }).catch(e => console.log(e))
	}

	const getRumah = async () => {
        const url = 'assetrumahdinas?approval_status_id=0&approval_status_id=2&enable=1&enable=0'
		await Engine.GET(url, TOKEN)
		.then(data => {
            data.map(result => { return setRumah(current => {return current + 1}) })
        }).catch(e => console.log(e))
	}

	const getSaranaBantuPemandu = async () => {
        const url = 'saranabantupemandu?approval_status_id=0&approval_status_id=2'
		await Engine.GET(url, TOKEN)
		.then(data => {
            data.map(result => { return setSaranaBantuPemandu(current => {return current + 1}) })
        }).catch(e => console.log(e))
	}

	const getPemeriksaanKapal = async () => {
        const url = 'pemeriksaankapal?approval_status_id=0&approval_status_id=2'
		await Engine.GET(url, TOKEN)
		.then(data => {
            data.map(result => { return setPemeriksaanKapal(current => {return current + 1}) })
        }).catch(e => console.log(e))
	}

	const getInvestigasiInsiden = async () => {
        const url = 'investigasiinsiden?approval_status_id=0&approval_status_id=2'
		await Engine.GET(url, TOKEN)
		.then(data => {
            data.map(result => { return setInvestigasiInsiden(current => {return current + 1}) })
        }).catch(e => console.log(e))
	}

	const getEvaluasiPelimpahan = async () => {
        const url = 'evaluasipelimpahan?approval_status_id=0&approval_status_id=2'
		await Engine.GET(url, TOKEN)
		.then(data => {
            data.map(result => { return setEvaluasiPelimpahan(current => {return current + 1}) })
        }).catch(e => console.log(e))
	}

	useEffect(() => {
		getResource()
		getKapal()
		getStation()
		getRumah()
		getSaranaBantuPemandu()
		getPemeriksaanKapal()
		getInvestigasiInsiden()
		getEvaluasiPelimpahan()
	}, [])

	return(
		<div className="row">
        	<h2 className="col-md-12 ml-1">Status Persetujuan</h2>
        	<div className="col-lg-4 col-md-6 col-sm-6 mb-3">
        		<div className="card card-custom px-4 py-5">
        			<h5>SDM Pemanduan</h5>
        			<h1 className="font-weight-bold">{pandu + pendukungPandu}</h1>
        			<hr/>
                    <div className="approve-card scroll-custom">
        				<NavLink className="text-secondary mb-2" to="/resource/pandu">
        					<p className="font-weight-bold">Pandu <span className="float-right">{pandu}</span></p>
    			        </NavLink>
            			<NavLink className="text-secondary mb-2" to="/resource/pendukung-pandu">
            				<p className="font-weight-bold">Pendukung Pandu <span className="float-right">{pendukungPandu}</span></p>
            			</NavLink>
                    </div>
        		</div>
        	</div>
        	<div className="col-lg-4 col-md-6 col-sm-6 mb-3">
        		<div className="card card-custom px-4 py-5">
        			<h5>Sarana & Prasarana Pemanduan</h5>
        			<h1 className="font-weight-bold">{kapal + station + rumah}</h1>
        			<hr/>
                    <div className="approve-card scroll-custom">
            			<NavLink className="text-secondary mb-2" to="/asset/kapal">
            				<p className="font-weight-bold">Kapal <span className="float-right">{kapal}</span></p>
            			</NavLink>
            			<NavLink className="text-secondary mb-2" to="/asset/stasiun-pandu">
            				<p className="font-weight-bold">Stasiun Pandu <span className="float-right">{station}</span></p>
            			</NavLink>
            			<NavLink className="text-secondary mb-2" to="/asset/rumah-dinas">
            				<p className="font-weight-bold">Rumah Dinas <span className="float-right">{rumah}</span></p>
            			</NavLink>
                    </div>
        		</div>
        	</div>
        	<div className="col-lg-4 col-md-6 col-sm-6 mb-3">
        		<div className="card card-custom px-4 py-5">
        			<h5>Inspeksi</h5>
        			<h1 className="font-weight-bold">
        				{saranaBantuPemandu + pemeriksaanKapal + investigasiInsiden + evaluasiPelimpahan}
        			</h1>
        			<hr/>
                    <div className="approve-card scroll-custom">
            			<NavLink className="text-secondary mb-2" to="/inspection/sarana-bantu-pemandu">
            				<p className="font-weight-bold">Sarana Bantu Pemanduan <span className="float-right">{saranaBantuPemandu}</span></p>
            			</NavLink>
            			<NavLink className="text-secondary mb-2" to="/inspection/pemeriksaan-kapal">
            				<p className="font-weight-bold">Pemeriksaan Kapal <span className="float-right">{pemeriksaanKapal}</span></p>
            			</NavLink>
            			<NavLink className="text-secondary mb-2" to="/inspection/investigasi-insiden">
            				<p className="font-weight-bold">Investigasi Insiden <span className="float-right">{investigasiInsiden}</span></p>
            			</NavLink>
            			<NavLink className="text-secondary mb-2" to="/inspection/evaluasi-pelimpahan">
            				<p className="font-weight-bold">Evaluasi Pelimpahan <span className="float-right">{evaluasiPelimpahan}</span></p>
            			</NavLink>
                    </div>
        		</div>
        	</div>
        </div>
	)
}
