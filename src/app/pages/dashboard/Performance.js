import React, { useEffect, useState } from "react"
import DatePicker, { registerLocale } from "react-datepicker"
import id from "date-fns/locale/id"
import { Engine } from "../../../simpanda/config/Engine"

import moment from 'moment';

export function Performance({auth}) {

    registerLocale("id", id)
    const TOKEN  = auth.accessToken
    const [cabangMst, setCabangMst] = useState([])

    const getCabang = async () => {
        await Engine.GET('cabang', TOKEN)
        .then(data => {
            setCabangMst(data)
        }).catch(e => console.log(e))
    }

    useEffect(() => {
        getCabang()
    }, [])

    const [personel, setPersonel] = useState([])
    const [ship, setShip] = useState([])
    const [loading, setLoading] = useState(false)

    const [cabang, setCabang] = useState(auth.cabang_id)
    const [date, setDate] = useState(new Date())

    const getPersonel = async () => {
        setLoading(true)
        let cb = cabang < 10 ? "0" + cabang.toString() : cabang
        let dt = moment(date).format('YYYY-MM')
        await Engine.PostData(`report/personelpeformance`, {"date" : dt, "cabang_id" : cb}, TOKEN)
        .then(data => {
        	setPersonel(data)
            setLoading(false)
        }).catch(e => console.log(e))
    }

    const getShip = async () => {
        setLoading(true)
    	let cb = cabang < 10 ? "0" + cabang.toString() : cabang
        let dt = moment(date).format('YYYY-MM')
        await Engine.PostData(`report/shippeformance`, {"date" : dt, "cabang_id" : cb}, TOKEN)
        .then(data => {
        	setShip(data)
            setLoading(false)
        }).catch(e => console.log(e))
    }

    useEffect(() => {
        if (date) {
            getPersonel()
            getShip()
        }
    }, [cabang, date])

    const Loading = () => {
        return (
            <tr>
                <td>
                    <span className="spinner spinner-warning dashboard-spinner"></span>
                </td>
            </tr>
        )
    }

    // console.log('panduLaut',panduLaut)
    // console.log('panduBandar',panduBandar)
    // console.log('armada',armada)

    return(
        <div className="row mt-3">
            <div className="col-12 mt-5 px-0">
                <div className="col-6 ml-auto">
                    <div className="d-flex mb-3">
                        <select id="cabang" className="form-control mr-1 w-75" value={cabang} 
                            onChange={(e) => setCabang(e.target.value)} >
                            {cabangMst.map((val, i) => {
                                return(val.id > 0 && <option value={val.id} key={i}>{val.nama}</option>)
                            })}
                        </select>
                        {/*<input type="month" id="date" className="form-control" value={date} 
                            onChange={(e) => setDate(e.target.value))} />*/}
                        <DatePicker
                            locale="id"
                            selected={date}
                            onChange={(e) => setDate(e)}
                            dateFormat="MMMM, yyyy"
                            className="form-control w-100"
                            showMonthYearPicker
                        />
                    </div>
                </div>
            </div>

            <div className="col-lg-6 col-md-6 mb-3">
                <h5>Kinerja Bulanan Personil</h5>
                <div className="card card-custom px-4 py-5">
                    <table className="table table-hover">
                        <thead style={{'display': 'table', 'width': '100%', 'tableLayout': 'fixed', 'height':'85px'}}>
                            <tr>
                                <th style={{'width':'50%'}}>Nama Pandu</th>
                                <th className="text-center">Total Gerakan</th>
                                <th className="text-center">Total GT</th>
                                <th className="text-center">Waiting Time (Menit)</th>
                            </tr>
                        </thead>
					    <tbody className="scroll-custom" style={{'display':'block','height':'220px','overflowY':'auto'}}>
                        {loading
                            ? <Loading/>
                            : personel !== undefined && personel.length > 0 && personel.map((item, i) => {
				            return (
	                            <tr key={i} style={{'display': 'table', 'width': '100%', 'tableLayout': 'fixed'}}>
	                                <td style={{'width':'50%'}}>
                                        <div className="row">
                                            <div className="col">
                                               <img src="/media/svg/avatars/001-boy.svg" alt="icon" width="25" className="mr-2" />
                                            </div>
                                            <div className="col-10">
                                               {item.nm_pers_pandu}
                                            </div>
                                        </div>
	                                </td>
	                                <td className="text-center">{item.gerakan}</td>
	                                <td className="text-center">{item.total_gt}</td>
	                                <td className="text-center">{item.waiting_time}</td>
	                            </tr>		                            
							)
				        })}
			           	</tbody>  
                    </table>
                </div>
            </div>

            <div className="col-lg-6 col-md-6 mb-3">
                <h5>Kinerja Bulanan Kapal</h5>
                <div className="card card-custom px-4 py-5">
                    <table className="table table-hover">
                        <thead style={{'display': 'table', 'width': '100%', 'tableLayout': 'fixed', 'height':'85px'}}>
                            <tr>
                                <th style={{'width':'50%'}}>Nama Kapal</th>
                                <th className="text-center">Total Gerakan</th>
                                <th style={{'width':'30%'}} className="text-center">Total Waktu Penundaan (Menit)</th>
                            </tr>
                        </thead>
                        <tbody className="scroll-custom" style={{'display':'block','height':'220px','overflowY':'auto'}}>
                        {loading
                            ? <Loading/>
                            : ship !== undefined && ship.length > 0 && ship.map((item, i) => {
				            return (
	                            <tr key={i} style={{'display': 'table', 'width': '100%', 'tableLayout': 'fixed'}}>
	                                <td style={{'width':'50%'}}>
                                        <div className="row">
                                            <div className="col">
    	                                       <img src="/media/icons/boat.png" alt="icon" width="25" className="mr-2" />
                                            </div>
                                            <div className="col-10">
    	                                       {item.nm_kapal}
                                            </div>
                                        </div>
	                                </td>
	                                <td className="text-center">{item.gerakan}</td>
	                                <td className="text-center" style={{'width':'30%'}}>{item.total_waktu}</td>
	                            </tr>		                            
							)
                        })}
			           	</tbody>                        
                    </table>
                </div>
            </div>
        </div>
    )
}