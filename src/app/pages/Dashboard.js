import React from "react"
import { useSubheader } from '../../_metronic/layout'

import { ApprovalStatus } from "./dashboard/ApprovalStatus"
import { StatisticResume } from "./dashboard/StatisticResume"
import { CertivicateValidity } from "./dashboard/CertivicateValidity"
import { PilotShipAvailability } from "./dashboard/PilotShipAvailability"
import { Performance } from "./dashboard/Performance"

export function Dashboard({auth}) {
	const suhbeader = useSubheader()
    suhbeader.setTitle('Hello ' + auth.nama)
    // console.log('auth Dashboard',auth)

    return (
    	<React.Fragment>
	        <ApprovalStatus auth={auth}/>

	        <div className="row mt-5">
	        	<div className="col-xl-8 col-lg-12 col-md-12 col-sm-12">
	        		<CertivicateValidity className="card-stretch gutter-b" auth={auth}/>
	        	</div>
	        	<div className="col-xl-4 col-lg-12 col-md-12 col-sm-12">
	        		<StatisticResume className="card-stretch gutter-b" auth={auth}/>
	        	</div>
	        </div>

	        <PilotShipAvailability auth={auth}/>

	        <Performance auth={auth}/>
        </React.Fragment>
    )
}
