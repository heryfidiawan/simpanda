import React, { useState, useEffect } from "react"
import { Tab, Tabs } from "react-bootstrap"
import { useHistory } from "react-router-dom"
import { shallowEqual, useSelector } from "react-redux"

import { InputDate } from "../../../simpanda/components/InputDate"
import { Engine } from "../../../simpanda/config/Engine"
import moment from 'moment'

export function ArmadaSchedule(props) {
	const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)
	
	const TOKEN   = isAuthorized.accessToken
	const history = useHistory()
	const PARENT  = props.match.url.split("/")[1]
	const MENU 	  = props.match.url.split("/")[2]
	const ACTION  = props.match.params.action
	const KEY 	  = props.match.params.key

	const PARENTURL = `${PARENT}/${MENU}`

	const [data, setData] = useState({cabang_id: isAuthorized.cabang_id, date: '', tipe_asset_id: ''})
	const [time, setTime] = useState([])
	const [cabang, setCabang] = useState([])
	const [kategori, setKategori] = useState([])
	const [armada, setArmada] = useState([])
	const [subdata, setSubData] = useState([])
	const [loading, setLoading] = useState(true)
	const [cabangId, setCabangId] = useState(data.cabang_id)

	const getCabang = async () => {
		await Engine.GET('cabang', TOKEN)
		.then(data => {
			setCabang(data)
        }).catch(e => console.log(e))
	}

	const getKategori = async () => {
		await Engine.GET('tipeasset/?flag=kapal', TOKEN)
		.then((data) => {
			setKategori(data)
        }).catch(e => console.log(e) )
	}

	const getArmada = async () => {
		setLoading(true)
		await Engine.GET('assetkapal/?tipe_asset_id='+data.tipe_asset_id+"&cabang_id="+data.cabang_id+"&enable=1&approval_status_id=1", TOKEN)
		.then(async (data) => {
			await data.map(result => {
				setSubData(prev => ([...prev, 
					[
						{
							'asset_kapal_id': result.id,
							'nama_asset': result.nama_asset,
							'available': 1,
							'to': '',
							'from': '',
							'keterangan': '',
						}
					]
				]))
			})

			setArmada(data)
			setLoading(false)
        }).catch(e => console.log(e) )
	}

	const getData = async () => {
		setLoading(true)
		await Engine.GET(`armadaschedule/${KEY}`, TOKEN)
		.then((data) => {
			console.log("GET DATA", data)
			setData(data)
			setSubData(data['notAvailable'])
			setCabangId(data.cabang_id)
			setLoading(false)
        }).catch(e => console.log(e) )
	}

	useEffect(() => {
		getKategori()
		if (data.tipe_asset_id && data.cabang_id && ACTION === 'create') {
			getArmada()
		}
		getCabang()
		if (ACTION !== 'create') {
			getData()
		}
		validation()
	}, [data.tipe_asset_id, data.cabang_id, ACTION, validType])

	const newInputHandle = (armada, index) => {
		// console.log('Armada',armada)

		let parent = [...subdata]
		let child = parent[index]
		child.push({
			'asset_kapal_id': armada.asset_kapal_id, 
			'nama_asset': armada.nama_asset, 
			'available': 0, 
			'from': '', 
			'to': '', 
			'keterangan': ''
		})
		parent[index] = child
		setSubData(parent)
	}

	const removeInputHandle = (p, a) => {
		let parent = [...subdata]
		let child = parent[p]
		child.splice(a, 1)
		parent[p] = child
		setSubData(parent)
	}

	// console.log('data', data)
	// console.log('cabang', cabang)
	// console.log('armada', armada)
	console.log('subdata', subdata)
	// console.log('loading',loading)

	const dataHandle = (e) => {
		const {name, value} = e.target
		setData(prev => ({ ...prev, [name]: value }))
		if (name == 'cabang_id' || name == 'tipe_asset_id') {
			setSubData([])
		}
		if (name == 'cabang_id') {
			setCabangId(value)
		}
	}

	const subHandle = (e) => {
		const target = e.target
		let name = target.name
		let value = target.value
		
		let parent = [...subdata]
		let child = parent[target.dataset.parent]
		let val = { ...child[target.dataset.child] }

		val[name] = val[name] = value
		val['available'] = 0

		if (name === 'from' && value === '') {
			val['available'] = 1
		}

		child[target.dataset.child] = val
		parent[target.dataset.parent] = child
		setSubData(parent)
	}

	const submitHandle = async (e) => {
		e.preventDefault()

	    console.log('SUBMIT DATA',data)
	    data['notAvailable'] = subdata

		const method = ACTION === 'create' ? 'POST' : 'PUT'
		const URL    = ACTION === 'create' ? 'armadaschedule' : 'armadaschedule'+'/'+KEY
		await Engine.POST(URL, method, data, TOKEN)
		.then(response => {
			if (response) {
				history.push(`/${PARENTURL}`)
			}
		})
	}

	const dataSorted = subdata.sort((a,b) => (a.asset_kapal_id > b.asset_kapal_id) ? 1 : ((b.asset_kapal_id > a.asset_kapal_id) ? -1 : 0))
	const disabledParent = ACTION !== 'create'
	const disabledChild = ACTION === 'detail'

	const [validType, setValidType] = useState(['create','edit','detail'])
    const validation = () => {
        if (isAuthorized.cabang_id < 1) {
            setValidType(['detail'])
        }
        validType.includes(ACTION) === false && history.push('/error/error-v1')
    }

    return (
		<div className="row">
			<div className="col-12">
				<form onSubmit={submitHandle}>
					<div className="card mb-5">
						<div className="card-body">
						<h3 className="font-weight-bold mb-10">Downtime Kapal</h3>
							<div className="row">
								<div className="col-md-4">
									<div className="form-group">
										<label>Cabang <span className="text-red">*</span></label>
										<select name="cabang_id" 
											onChange={dataHandle} 
											value={data.cabang_id}
											className="form-control bg-white"
											required={true}
											disabled={disabledParent}
											>
											<option value="">Pilih Cabang</option>
											{cabang.map((v,i) => {
												return(<option key={i} value={v.id}>{v.nama}</option>)
											})}
										</select>
									</div>
								</div>
								<div className="col-md-4">
									<div className="form-group">
										<label>Tanggal <span className="text-red">*</span></label>
										{/*<input type="date" name="date" 
											onChange={dataHandle} 
											value={moment(data.date).format('YYYY-MM-DD')}
											className="form-control bg-white" 
											required={true}
											disabled={disabledParent}
										/>*/}
										<InputDate 
			                            	name="date" 
			                              	dataHandle={dataHandle} 
			                              	value={data.date} 
			                              	required={true}
			                              	disabled={disabledParent}
			                            />
									</div>
								</div>
								<div className="col-md-4">
									<div className="form-group">
										<label>Kategori Armada <span className="text-red">*</span></label>
										<select name="tipe_asset_id" 
											onChange={dataHandle} 
											value={data.tipe_asset_id}
											className="form-control bg-white"
											required={true}
											disabled={disabledParent}
											>
											<option value="">Pilih Kategori Armada</option>
											{kategori.map((v,i) => {
												return(<option key={i} value={v.id}>{v.nama}</option>)
											})}
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div className="card mb-2">
						{loading === false || <h5 className="text-center my-5">Pilih Kategori Armada</h5>}
					</div>

					{dataSorted.map((armada,p) => {
						return(
							<div className="card mb-2" key={p}>
								<div className="card-body">
									
									<div className="row">
										<div className="col-md-3"><label>Armada</label></div>
										<div className="col-2"><label>Waktu Mulai Kerusakan <span className="text-red">*</span></label></div>
										<div className="col-2"><label>Waktu Selesai Perbaikan <span className="text-red">*</span></label></div>
										<div className="col-md-4"><label>Keterangan <span className="text-red">*</span></label></div>
										<div className="col"><label className="d-block">Aksi</label></div>
									</div>
									{armada.map((ar, a) => {
										return(
											<div className="row mt-2" key={a}>
												<div className="col-md-3">
													<input type="text" 
														className="form-control bg-white"
														name="asset_kapal_id" 
														value={ar.nama_asset} 
														disabled={true}
													/>
												</div>
												<div className="col-2">
													<input 
														type="time" 
														max={subdata[p][a]['to']}
		                                                className="form-control bg-white" 
		                                                name="from" 
		                                                value={subdata[p][a]['from']}
		                                                onChange={subHandle}
		                                                data-parent={p}
		                                                data-child={a}
		                                                required={
		                                                	subdata[p][a]['to'] !== '' || subdata[p][0]['to'] !== '' 
		                                                	? true : false
		                                                }
		                                                disabled={disabledChild}
		                                            />
												</div>
												<div className="col-2">
													<input 
														type="time" 
														min={subdata[p][a]['from']}
		                                                className="form-control bg-white" 
		                                                name="to" 
		                                                value={subdata[p][a]['to']}
		                                                onChange={subHandle}
		                                                data-parent={p}
		                                                data-child={a}
		                                                required={
		                                                	subdata[p][a]['from'] !== '' || subdata[p][0]['from'] !== '' 
		                                                	? true : false
		                                                }
		                                                disabled={disabledChild}
		                                            />
												</div>
												<div className="col-md-4">
													<input type="text" 
		                                                name="keterangan" className="form-control bg-white" 
		                                                value={
		                                                	subdata[p][a]['keterangan'] !== 'null'
		                                                	? subdata[p][a]['keterangan']
		                                                	: ''
		                                                }
		                                                onChange={subHandle}
		                                                data-parent={p}
		                                                data-child={a}
		                                                required={
		                                                	ACTION === 'edit'
		                                                	? false
		                                                	: subdata[p][a]['from'] !== '' || subdata[p][0]['from'] !== '' 
		                                                		? true : false
		                                                }
		                                                disabled={disabledChild}
		                                            />
		                                            {/*<div className="form-check mt-2">
	                                                    <input 
	                                                    	type="radio" 
	                                                        name={`available_${p}_${a}`} className="form-check-input" 
	                                                        onChange={subHandle}
	                                                        value="1"
	                                                        checked={
	                                                        	subdata[p][a]['available'] > 0 ? true : false
	                                                        }
	                                                        disabled={disabledChild}
	                                                    />
	                                                    <label className="form-check-label" htmlFor={`available_${p}`}>
	                                                        Available
	                                                    </label>
	                                                </div>*/}
												</div>
												<div className="col">
													{a == 0 &&
														<button 
															type="button"
															className="btn btn-outline-primary"
															onClick={() => newInputHandle(ar, p) }
														>
															<i className="fas fa-plus ml-1"></i>
														</button>
													}
													{a > 0 &&
														<button 
															type="button"
															className="btn btn-outline-primary"
															onClick={() => removeInputHandle(p, a) }
														>
															<i className="fas fa-minus ml-1"></i>
														</button>
													}
												</div>
											</div>
										)
									})}
								</div>
							</div>
						)
					})}

					{/*<div className="card">
						{loading === false
							? 
							<div className="card-body">
								<table className="table">
									<thead>
										<tr>
											<th>Armada</th>
											<th colSpan="2">Available</th>
											<th>From</th>
											<th>To</th>
											<th>Keterangan</th>
										</tr>
									</thead>
									<tbody>
										{subdata.length > 0 && dataSorted.map((pr,p) => {
											return(
												<tr key={p}>
													<td>
														<input type="text" 
															className="form-control bg-white"
															name="asset_kapal_id" 
															value={pr.nama_asset} 
															disabled={true}
														/>
													</td>
													<td>
														<div className="form-check mt-2">
		                                                    <input type="radio" id={`available_${p}`} 
		                                                        name={`available_${p}`} className="form-check-input" 
		                                                        onChange={subHandle}
		                                                        value="1"
		                                                        data-index={p}
		                                                        checked={
		                                                        	subdata.length ? subdata[p]['available'] == 1 ? true : false : false
		                                                        }
		                                                        disabled={disabledChild}
		                                                    />
		                                                    <label className="form-check-label" htmlFor={`available_${p}`}>
		                                                        Available
		                                                    </label>
		                                                </div>
													</td>
													<td>
														<div className="form-check mt-2">
		                                                    <input type="radio" id={`notavailable${p}`} 
		                                                        name={`notavailable${p}`} className="form-check-input" 
		                                                        onChange={subHandle}
		                                                        value="0"
		                                                        data-index={p}
		                                                        checked={
		                                                        	subdata.length ? parseInt(subdata[p]['available']) == 0 ? true : false : false
		                                                        }
		                                                        disabled={disabledChild}
		                                                    />
		                                                    <label className="form-check-label" htmlFor={`notavailable${p}`}>
		                                                        Not Available
		                                                    </label>
		                                                </div>
													</td>
													<td>
														<input type="time" 
	                                                        className="form-control bg-white" 
	                                                        name="from" 
	                                                        value={subdata.length ? subdata[p]['from'] : ''}
	                                                        onChange={subHandle}
	                                                        data-index={p}
	                                                        required={true}
	                                                        disabled={disabledChild}
	                                                    />
													</td>
													<td>
	                                                    <input type="time" 
	                                                        className="form-control bg-white" 
	                                                        name="to" 
	                                                        value={subdata.length ? subdata[p]['to'] : ''}
	                                                        onChange={subHandle}
	                                                        data-index={p}
	                                                        required={true}
	                                                        disabled={disabledChild}
	                                                    />
													</td>
													<td>
														<input type="text" 
	                                                        name="keterangan" className="form-control bg-white" 
	                                                        value={subdata.length ? subdata[p]['keterangan'] : ''}
	                                                        onChange={subHandle}
	                                                        data-index={p}
	                                                        disabled={disabledChild}
	                                                    />
													</td>
												</tr>
											)
										})}
									</tbody>
								</table>
							</div>
							: <h5 className="text-center my-5">Pilih Kategori Armada</h5>
						}
					</div>*/}
					{ACTION !== 'detail' &&
						<button type="submit" className="btn btn-primary float-right px-5 mt-5">
							Simpan
						</button>
					}
				</form>
			</div>
		</div>
    )
}
