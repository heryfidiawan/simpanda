import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Engine } from "../../simpanda/config/Engine";
import { shallowEqual, useSelector } from "react-redux";

export function UserGroup(props) {
	const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)
	
	const TOKEN   = isAuthorized.accessToken
	const history = useHistory()
	const PARENT  = props.match.url.split("/")[1]
	const MENU 	  = props.match.url.split("/")[2]
	const ACTION  = props.match.params.action
	const KEY 	  = props.match.params.key

	const PARENTURL = `${PARENT}/${MENU}`

	const [parentMenu, setParentMenu] = useState([])
	const [menus, setMenus] = useState([])
	const [cabang, setCabang] = useState([])
	const [data, setData] = useState({nama:'', cabang_id:'', keterangan:''})
	const [check, setCheck] = useState([])

	const getMenu = async () => {
		await Engine.GET('menu', TOKEN)
		.then(data => {
			setMenus(data)
			data.reverse().map( async (menu) => {
				if (parentMenu.indexOf(menu.parent) === -1) {
					parentMenu.push(menu.parent)
				}
				// if (ACTION === 'create') {
				// 	setCheck(prev => ([...prev, {'menu_id': 0}]))
				// }
			})
			console.log("GET MENU", data)
		}).catch(e => console.log(e))
	}

	const getData = async () => {
		await Engine.GET(`usergroup/${KEY}`, TOKEN)
		.then(data => {
			console.log("GET DATA", data)
			setData(data)
			setCheck(data['user_access'])
		}).catch(e => console.log(e))
		return false
	}

	const getCabang = async () => {
		await Engine.GET('cabang', TOKEN)
		.then(data => {
			setCabang(data)
			// console.log("CABANG", data)
        }).catch(e => console.log(e))
	}

	useEffect(() => {
		getMenu()
		getCabang()
		if (ACTION !== 'create') {
			getData()
		}
	}, [])

	const dataHandle = (e) => {
		const {name, value} = e.target
		setData(prev => ({ ...prev, [name]: value }))
	}

	const checkHandle = (e) => {
		const {name, value} = e.target
		console.log('CHECK', e.target.checked)

		e.target.checked 
		? setCheck(prev => ({ ...prev, [name]: value }))
		: setCheck(prev => ({ ...prev, [name]: false }))
	}

	console.log('data', data)
	console.log('check', check)
	console.log('disabled', disabled)
	console.log('parentMenu',parentMenu)
	console.log('menus', menus)

	const submitHandle = async (e) => {
		e.preventDefault()
		data['user_access'] = check

	    console.log('SUBMIT DATA', data)

		const method = ACTION === 'create' ? 'POST' : 'PUT';
		const URL    = ACTION === 'create' ? 'usergroup' : 'usergroup'+'/'+KEY
		await Engine.POST(URL, method, data, TOKEN)
		.then(response => {
			if (response) {
				history.push(`/${PARENTURL}`)
			}
		})
	}

	// const validType = ['create','edit','detail']
	// if (validType.includes(ACTION) === false) {
	//     history.push('/error/error-v1')
	// }

	const disabled = ACTION === 'detail'

	const [validType, setValidType] = useState(['create','edit','detail'])
    const validation = () => {
        if (isAuthorized.cabang_id > 0) {
            setValidType(['detail'])
        }
        validType.includes(ACTION) === false && history.push('/error/error-v1')
    }

    useEffect(() => {
        validation()
    }, [ACTION, validType])

    return (
		<div className="row">
			<div className="col-12">
				<form onSubmit={submitHandle}>
					<div className="card mb-5">
						<div className="card-body">
							<h4 className="font-weight-bold mb-3">User Group</h4>
							<div className="row">
								<div className="col-md-6">
									<div className="form-group">
										<label>Nama User Group</label>
										<input type="text" name="nama" 
											className="form-control bg-white" 
											onChange={dataHandle} 
											value={data.nama} 
											disabled={disabled}
											required 
										/>
									</div>
								</div>
								<div className="col-md-6">
									<div className="form-group">
										<label>Cabang</label>
										<select name="cabang_id" 
											onChange={dataHandle} 
											value={data.cabang_id}
											className="form-control bg-white"
											disabled={disabled}
											required 
										>
											<option value="">Pilih Cabang</option>
											{cabang.map((v,i) => {
												return(<option key={i} value={v.id}>{v.nama}</option>)
											})}
										</select>
									</div>
								</div>
								<div className="col-md-12">
									<div className="form-group">
										<label>Keterangan</label>
										<textarea rows="5" name="keterangan" 
											className="form-control bg-white" 
											onChange={dataHandle}
											value={data.keterangan}
											disabled={disabled}
											required
										></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div className="card mb-5">
						<div className="card-body">
							<h4 className="font-weight-bold mb-5">Access Control Menu</h4>
							<div className="row">
								{parentMenu.map((parent, pr) => {
									return(
										<div className="col-md-3" key={pr}>
											<h6>{parent}</h6>
											{menus.map((menu, mn) => {
												return(
													<div className="form-group" key={mn}>
														{menu.parent === parent &&
															<div className="form-check">
																<input type="checkbox" id={`menu_${menu.id}`} 
																	name={menu.nama} className="form-check-input"
																	onChange={checkHandle}
																	value={menu.id}
																	checked={
																		check[menu.nama] == menu.id ? true : false
																	}
																	disabled={disabled}
																/>
																<label className="form-check-label font-weight-bold text-primary" 
																	htmlFor={`menu_${menu.id}`}>{menu.nama}</label>
															</div>
														}
													</div>
												)
											})}
										</div>
									)
								})}
							</div>
						</div>
					</div>
					{ACTION !== 'detail' &&
						<button type="submit" className="btn btn-primary float-right px-5 mx-3">Simpan User Group</button>
					}
				</form>
			</div>
		</div>
    )
}
