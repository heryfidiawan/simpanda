import React, { useState, useEffect } from "react";
import { Tab, Tabs } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useSubheader } from "../../_metronic/layout";

import { TableConfig } from "../../simpanda/config/table";
import { Table } from "../../simpanda/components/Table";
import { Filter } from "../../simpanda/components/Filter";
import { Engine } from "../../simpanda/config/Engine";
import { shallowEqual, useSelector } from "react-redux";
import moment from "moment";

import Swal from "sweetalert2";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { CollectionsBookmarkOutlined } from "@material-ui/icons";

export function IndexPage(props) {

    const CONFIGMENU = TableConfig[props.parent][props.menu]

    const actionUrl = (param) => {
        let valid = param
        if (param[1] == 'sertifikat') {
            if (param[2] == 'pandu') {
                valid = '/resource/pandu'
            }else if(param[2] == 'pendukung-pandu') {
                valid = '/resource/pendukung-pandu'
            }else if(param[2] == 'kapal') {
                valid = '/asset/kapal'
            }else if(param[2] == 'evaluasi-pelimpahan') {
                valid = '/inspection/evaluasi-pelimpahan'
            }
        }else {
            valid = CONFIGMENU.front_url
        }
        return valid
    }

    const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)
    const TOKEN = isAuthorized.accessToken

    const TabNonAktif = CONFIGMENU.tab.nonaktif
    const TabPermintaan = CONFIGMENU.tab.permintaan
    
    const suhbeader = useSubheader()
    suhbeader.setTitle(CONFIGMENU.title)

    const [key, setKey] = useState("aktif")
    const [loading, setLoading] = useState(false)

    const [data, setData] = useState([])
    const [perPage, setPerPage] = useState(50)
    const [currentPage, setCurrentPage] = useState(1)
    const [totalResults, setTotalResults] = useState(0)

    const [start, setStart] = useState(0)
    const [limit, setLimit] = useState(perPage)
    const [single, setSingle] = useState("")
    
    const [filters, setFilters] = useState("")
    const [dataFilterDownload, setFilterDownload] = useState([])
    let paths = ''

    const MENU = props.menu
    const BACKEND = CONFIGMENU.tab[key].tab_url

    const handlePerPage = (length) => {
        setPerPage(length)
        setStart((currentPage - 1) * perPage) 
        setLimit(length) // 25
    }
    
    const handlePerMonth = (input) => {
        setSingle(input)
        // console.log('per_month '+input)
    }

    const handlePerBranch = (input = 1) => {
        setSingle(input)
        // console.log('per_branch '+input)
    }

    const handlePage = (page) => {
        paths = filters
        setCurrentPage(page)
        setStart((parseInt(page) - 1) * parseInt(perPage))
        setLimit(perPage)
        setPerPage(perPage)
        // console.log((page - 1) * perPage)
    }

    const handleFiltered = (pages) => {
        setFilters(pages)
        // setCurrentPage(page)
        // setStart((page - 1) * perPage)
        // setLimit(perPage)
    }

    const Tables = () => {
        return (
            <Table
                loading={loading}
                data={data}
                dataReport={dataFilterDownload}
                pagination={{
                    totalResults: totalResults,
                    perPage: perPage,
                    currentPage: currentPage,
                    handlePage: handlePage,
                }}
                config={{
                    backend: CONFIGMENU.url,
                    url: actionUrl(window.location.pathname.split("/")),
                    table: CONFIGMENU.tab[key],
                    aksi: CONFIGMENU.tab[key].action,
                    request: CONFIGMENU.tab[key].request,
                    status: CONFIGMENU.tab[key].status,
                    tab_url: BACKEND,
                }}
            />
        )
    }

    paths = BACKEND

    if(single !== ""){
        // console.log(CONFIGMENU.tab[key].tab_url.split("?")[1])
        if(CONFIGMENU.tab[key].tab_url.split("?")[1] !== undefined){
            paths = CONFIGMENU.tab[key].tab_url+`&q=`+single
        }else{
            paths = CONFIGMENU.tab[key].tab_url.split("?")[0]+`?q=`+single
        }
    }else{
        if (filters !== "") {
            paths = CONFIGMENU.tab[key].tab_url+filters
        }else{
            paths = CONFIGMENU.tab[key].tab_url
            if (window.location.pathname.split("/")[1] == 'sertifikat' && key === 'aktif') {
                paths += '&sertifikat='+window.location.pathname.split("/")[3]
            }
        }
    }

    const handleTab = () => {
        setCurrentPage(1)
        setStart(0)
        setLimit(perPage)
    }
    const handleSearch = (input) => {
        setSingle(input)
        if(CONFIGMENU.tab[key].tab_url.split("?")[1] !== undefined){
            paths = CONFIGMENU.tab[key].tab_url+`&q=`+single
        }else{
            paths = CONFIGMENU.tab[key].tab_url.split("?")[0]+`?q=`+single
        }
    }

    const handleTable = (path) => {
        Engine.GET(paths, TOKEN)
        .then(data => {
            let datas = data.length === 'undefined' ? [data] : data.slice((parseInt(currentPage) - parseInt(1)) * parseInt(perPage),(parseInt(start)+parseInt(perPage)))
            setData(datas)
            setTotalResults(data.length)
            setLoading(false)
        })
        .catch(e => {
            console.log(e)
        })
    }
    

    const downloadExcel = (path) => {
        var arr = []
        var fileName = ''
        path.map((a,b) => {
            var str = a.id.toString();
            arr.push(str)
        })

        var downloadUrl = MENU.replace("_", "");
        console.log(downloadUrl)
        if(downloadUrl === 'pandu'){
            fileName = 'Custom Report - Personil Pandu';
        }else if(downloadUrl === 'pendukungpandu'){
            fileName = 'Custom Report - Personil Pendukung Pandu';
        }else if(downloadUrl === 'kapal'){
            fileName = 'Custom Report - Asset Kapal';
        }else if(downloadUrl === 'stasiunpandu_and_equipment'){
            fileName = 'Custom Report - Asset Stasiun Pandu';
        }else{
            fileName = 'Custom Report - Asset Rumah Dinas';
        }

        var ids = {"cabang_id" : arr}

        Engine.POSTFILE(downloadUrl, fileName, ids, TOKEN, "report/")
    }

    const filterDownload = async (param) => {   
        const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        const fileExtension = '.xlsx';
        var filename = CONFIGMENU.url;
        var tablename = CONFIGMENU.url;
        var sheets = moment(param.date).format("MMMM YYYY");
        if(filename === "pilotshipavailability"){
            filename = "pilotship";
            sheets = "Kesiapan Armada";
        }
        var x = Math.floor((Math.random() * 10000) + 100);
        
        const fileName = "Custom Report-"+filename+"_"+x;
        
        let arr = []
        if (filename == "pelaporanmanagement" && param.jenislaporan == 1){
            return await Engine.POSTFILE("pelaporanmanagement","pelaporanmanagement", param, TOKEN, "report/");
        }else if (filename == "pelaporanmanagement" && param.jenislaporan == 2){
            return await Engine.POSTFILE("pelaporanmanagementpandu","pelaporanmanagementpandu", param, TOKEN, "report/");
        }else if (filename == "pelaporanmanagement" && param.jenislaporan == 3){
            return await Engine.POSTFILE("pelaporanmanagementtunda","pelaporanmanagementtunda", param, TOKEN, "report/");
        }

        var fileNames = ''
        var downloadUrl = MENU.replace("_", "");
        if(downloadUrl === 'crewlist' ){
            fileNames = 'Laporan Daftar Awak Kapal';
            Engine.POSTFILE(downloadUrl,fileNames, param, TOKEN, "report/")
        }
        
        if(downloadUrl === 'pilotship_availability' ){
            console.log(param)
            if(param !== [] ){
                fileNames = 'Laporan Daftar Awak Kapal';
                Engine.POSTFILE("pilotship",fileNames, param, TOKEN, "report/")
            }else{
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 1300,
                    timerProgressBar: true,
                })
                
                Toast.fire({
                    background: '#F4F5F7',
                    type: 'error',
                    title: "Masukkan parameter terlebih dahulu"
                })
            }
        }
    }
    
    useEffect(() => {
        setLoading(true)
        handleTable(paths)
    }, [start, limit, single, paths])

    return (
        <div className="row">
            <div className={"col-md-" +(CONFIGMENU.per_month ? "12" : "5")}>
                <Filter
                    handlePerPage={handlePerPage}
                    handleSearch={handleSearch}
                    handlePerMonth={handlePerMonth}
                    handlePerBranch={handlePerBranch}
                    handleInput={single}
                    handlePath={paths}
                    handleTable={handleTable}
                    handleFiltered={handleFiltered}
                    handleDownloads={filterDownload}
                    handleUrl={CONFIGMENU.url}
                    config={{
                        page_length: CONFIGMENU.page_length,
                        search: CONFIGMENU.search,
                        filter: CONFIGMENU.filter,
                        per_month: CONFIGMENU.per_month,
                        per_branch: CONFIGMENU.per_branch,
                        report: CONFIGMENU.report,
                    }}
                />
            </div>
            
            <div className="col">
            {console.log("menuuuusss" , MENU)}
            {isAuthorized.cabang_id > 0 ? 
                CONFIGMENU.create_title && (
                    <>
                        <NavLink
                            className="btn btn-primary btn-sm float-right btn-radius"
                            to={CONFIGMENU.front_url + "/create"}
                        >
                            {CONFIGMENU.create_title}
                        </NavLink>
                    </>
                ) : MENU === 'user' || MENU === 'user_group' && isAuthorized.cabang_id === 0 ? 
                CONFIGMENU.create_title && (
                    <>
                        <NavLink
                            className="btn btn-primary btn-sm float-right btn-radius"
                            to={CONFIGMENU.front_url + "/create"}
                        >
                            {CONFIGMENU.create_title}
                        </NavLink>
                    </>
                ) : CONFIGMENU.create_title && (
                    <>
                        
                    </>
                )
            }
                
                {CONFIGMENU.generate_report && (
                    <button className="btn btn-primary btn-sm float-right mr-5 btn-radius" onClick={() => downloadExcel(data)} download>
                        <i className="fa fa-excel"></i>Unduh
                    </button>
                )}
            </div>
            <div className="col-md-12">
                {window.location.pathname.split("/")[1] === 'sertifikat' 
                    ? <div className="card p-3" style={{display:CONFIGMENU.tab[key].thead.length > 0 ? "block" : "none"}}>
                            <Tables />
                        </div>
                    : typeof TabPermintaan !== 'undefined' || typeof TabNonAktif !== 'undefined' 
                    ? 
                        <Tabs id="controlled-tab-example" activeKey={key} onSelect={(key) => setKey(key)} onClick={() => handleTab()}>
                            <Tab eventKey="aktif" title={CONFIGMENU.tab_data.aktif}>
                                <Tables />
                            </Tab>
                            {typeof TabPermintaan !== 'undefined' && (
                                <Tab eventKey="permintaan"  title={CONFIGMENU.tab_data.permintaan}>
                                    <Tables />
                                </Tab>
                            )}
                            {typeof TabNonAktif !== 'undefined' && (
                                <Tab eventKey="nonaktif"  title={CONFIGMENU.tab_data.nonaktif}>
                                    <Tables />
                                </Tab>
                            )}
                        </Tabs>
                    :
                        <div className="card p-3" style={{display:CONFIGMENU.tab[key].thead.length > 0 ? "block" : "none"}}>
                            <Tables />
                        </div>
                }
            </div>
        </div>
    )
}
