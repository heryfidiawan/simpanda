import React, { useState, useEffect } from "react"
import { useHistory } from "react-router-dom"
import { shallowEqual, useSelector } from "react-redux"

import { InputDate } from "../../../simpanda/components/InputDate"
import { Engine } from "../../../simpanda/config/Engine"
import moment from 'moment'

export function SchedulePandu(props) {
	const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)
	
	const TOKEN   = isAuthorized.accessToken
	const history = useHistory()
	const PARENT  = props.match.url.split("/")[1]
	const MENU 	  = props.match.url.split("/")[2]
	const ACTION  = props.match.params.action
	const KEY 	  = props.match.params.key

	const PARENTURL = `${PARENT}/${MENU}`

	const [data, setData] = useState({cabang_id: isAuthorized.cabang_id, date: ''})
	const [cabang, setCabang] = useState([])
	const [subdata, setSubData] = useState([])
	const [cabangId, setCabangId] = useState(data.cabang_id)

	const getCabang = async () => {
		await Engine.GET('cabang', TOKEN)
		.then(data => {
			setCabang(data)
        }).catch(e => console.log(e))
	}

	const getPersonil = async () => {
		await Engine.GET('personil/?tipe_personil_id=1&approval_status_id=1&enable=1&cabang_id='+data.cabang_id, TOKEN)
		.then(async (data) => {
			await data.map(result => {
				setSubData(prev => ([...prev, 
					{
						'personil_id': result.id,
						'nama': result.nama,
						'kehadiran': 1,
						'to': '',
						'from': '',
						'keterangan': 'Hadir',
					}
				]))
			})
        }).catch(e => console.log(e) )
	}

	const getData = async () => {
		await Engine.GET(`panduschedule/${KEY}`, TOKEN)
		.then((data) => {
			console.log("GET DATA", data)
			setData(data)
			setSubData(data['personil'])
			setCabangId(data.cabang_id)
        }).catch(e => console.log(e) )
	}

	useEffect(() => {
		if (ACTION === 'create') {
			getPersonil()
		}
		getCabang()
		if (ACTION !== 'create') {
			getData()
		}
		validation()
	}, [data.cabang_id, ACTION, validType])
	
	const dataHandle = (e) => {
		const {name, value} = e.target
		setData(prev => ({ ...prev, [name]: value }))
		if (name == 'cabang_id') {
			setSubData([])
		}
	}

	const subHandle = (e) => {
		const target = e.target
		let name = target.name
		let value = target.value

		let tempState = [...subdata]
		let tempElement = { ...tempState[target.dataset.index] }
		
		if (target.type == 'radio') {
			name = 'kehadiran'
			value = parseInt(target.value)

			if (value > 0) {
				tempElement['keterangan'] = tempElement['keterangan'] = 'Hadir'
			}
		}
		
		tempElement[name] = tempElement[name] = value
		tempState[target.dataset.index] = tempElement

		setSubData(tempState)
		if (name == 'cabang_id') {
			setCabangId(value)
		}
	}

	console.log('data', data)
	console.log('cabang', cabang)
	// console.log('personil', personil)
	console.log('subdata', subdata)

	const submitHandle = async (e) => {
		e.preventDefault()

	    console.log('SUBMIT DATA',data)
	    data['personil'] = subdata

		const method = ACTION === 'create' ? 'POST' : 'PUT'
		const URL    = ACTION === 'create' ? 'panduschedule' : 'panduschedule'+'/'+KEY
		await Engine.POST(URL, method, data, TOKEN)
		.then(response => {
			if (response) {
				history.push(`/${PARENTURL}`)
			}
		})
	}

	const dataSorted = subdata.sort((a,b) => (a.personil_id > b.personil_id) ? 1 : ((b.personil_id > a.personil_id) ? -1 : 0))
	const disabledParent = ACTION !== 'create'
	const disabledChild = ACTION === 'detail'

	const [validType, setValidType] = useState(['create','edit','detail'])
    const validation = () => {
        if (isAuthorized.cabang_id < 1) {
            setValidType(['detail'])
        }
        validType.includes(ACTION) === false && history.push('/error/error-v1')
    }

    return (
		<div className="row">
			<div className="col-12">
				<form onSubmit={submitHandle}>
					<div className="card mb-5">
						<div className="card-body">
							{/*<h5 className="font-weight-bold mb-3"></h5>*/}
							<div className="row">
								<div className="col-md-6">
									<div className="form-group">
										<label>Cabang <span className="text-red">*</span></label>
										<select 
											name="cabang_id" 
											onChange={dataHandle} 
											value={data.cabang_id}
											className="form-control bg-white"
											required={true}
											disabled={disabledParent}
											>
											<option value="">Pilih Cabang</option>
											{cabang.map((v,i) => {
												return(<option key={i} value={v.id}>{v.nama}</option>)
											})}
										</select>
									</div>
								</div>
								<div className="col-md-6">
									<div className="form-group">
										<label>Tanggal <span className="text-red">*</span></label>
										<InputDate 
			                            	name="date" 
			                              	dataHandle={dataHandle} 
			                              	value={data.date} 
			                              	required={true}
			                              	disabled={disabledParent}
			                            />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="card mb-5">
						<div className="card-body">
							<table className="table">
								<thead>
									<tr>
										<th>Personil</th>
										<th colSpan="2">Kehadiran</th>
										<th>Waktu Mulai <span className="text-red">*</span></th>
										<th>Waktu Selesai <span className="text-red">*</span></th>
										<th>Keterangan</th>
									</tr>
								</thead>
								<tbody>
									{subdata.length > 0 && dataSorted.map((pr,p) => {
										return(
											<tr key={p}>
												<td>
													<input 
														type="text" 
														className="form-control form-control-sm bg-white"
														name="personil_id" 
														value={pr.nama} 
														disabled={true}
													/>
												</td>
												<td>
													<div className="form-check mt-2">
	                                                    <input 
	                                                    	type="radio" 
	                                                    	id={`kehadiran_hadir_${p}`} 
	                                                        name={`kehadiran_hadir_${p}`} 
	                                                        className="form-check-input" 
	                                                        onChange={subHandle}
	                                                        value="1"
	                                                        data-index={p}
	                                                        checked={
	                                                        	subdata[p]['kehadiran'] == 1 ? true : false
	                                                        }
	                                                        disabled={disabledChild}
	                                                    />
	                                                    <label className="form-check-label" htmlFor={`kehadiran_hadir_${p}`}>
	                                                        Hadir
	                                                    </label>
	                                                </div>
												</td>
												<td>
													<div className="form-check mt-2">
	                                                    <input 
	                                                    	type="radio" id={`kehadiran_tidak_${p}`} 
	                                                        name={`kehadiran_tidak_${p}`} 
	                                                        className="form-check-input" 
	                                                        onChange={subHandle}
	                                                        value="0"
	                                                        data-index={p}
	                                                        checked={
	                                                        	parseInt(subdata[p]['kehadiran']) == 0 ? true : false
	                                                        }
	                                                        disabled={disabledChild}
	                                                    />
	                                                    <label className="form-check-label" htmlFor={`kehadiran_tidak_${p}`}>
	                                                        Tidak Hadir
	                                                    </label>
	                                                </div>
												</td>
												<td>
													<input 
														type="time" 
                                                        name="from" 
                                                        max={subdata[p]['to']}
                                                        className="form-control form-control-sm bg-white" 
                                                        value={subdata[p]['from']}
                                                        onChange={subHandle}
                                                        data-index={p}
                                                        required={subdata[p]['kehadiran'] == 1 ? true : false}
                                                        disabled={disabledChild}
                                                    />
												</td>
												<td>
													<input 
														type="time" 
                                                        name="to" 
                                                        min={subdata[p]['from']}
                                                        className="form-control form-control-sm bg-white" 
                                                        value={subdata[p]['to']}
                                                        onChange={subHandle}
                                                        data-index={p}
                                                        required={subdata[p]['kehadiran'] == 1 ? true : false}
                                                        disabled={disabledChild}
                                                    />
												</td>
												<td>
													<input 
														type="text" 
                                                        name="keterangan" className="form-control form-control-sm bg-white" 
                                                        value={subdata[p]['keterangan']}
                                                        onChange={subHandle}
                                                        data-index={p}
                                                        disabled={disabledChild}
                                                        required={subdata[p]['kehadiran'] == 1 ? false : true}
                                                        readOnly={subdata[p]['kehadiran'] == 1 ? true : false}
                                                    />
												</td>
											</tr>
										)
									})}
								</tbody>
							</table>
						</div>
					</div>
					{ACTION !== 'detail' &&
						<button type="submit" className="btn btn-primary float-right px-5">
							Simpan
						</button>
					}
				</form>
			</div>
		</div>
    )
}
