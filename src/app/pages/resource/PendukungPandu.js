import React, { useState, useEffect } from "react"
import { useHistory } from "react-router-dom"
import { shallowEqual, useSelector } from "react-redux"

import { Engine } from "../../../simpanda/config/Engine"
import { jabatan_pendukung_pandu } from "../../../simpanda/config/detail"

import { InputDate } from "../../../simpanda/components/InputDate"
import { TempatTugas } from "../../../simpanda/components/TempatTugas"
// import { Skppp } from "../../../simpanda/components/Skppp"
import { SertifikatKesehatan } from "../../../simpanda/components/SertifikatKesehatan"
import { Sertifikat } from "../../../simpanda/components/Sertifikat"
import { Remark } from "../../../simpanda/components/Remark"
import { ActivityLog } from "../../../simpanda/components/ActivityLog"

import moment from 'moment'

const defaultData = {
	// General
	nama: '',
	nipp: '',
	jabatan: '',
	enable: '',
	tempat_lahir: '',
	tanggal_lahir: '',
	tipe_personil_id: '',
	asset_kapal_id: '',
	manning: '',

	// Tempat Tugas
	cabang_id: '',
	tanggal_mulai: '',
	tanggal_selesai: '',
	nomor_sk: '',
	sk: '',

	// Skppp
	// skpp_tanggal_mulai: '',
	// skpp_tanggal_selesai: '',
	// skpp: '',

	// skppp / Sertifikat Kesehatan
	skes_tanggal_mulai: '',
	skes_tanggal_selesai: '',
	surat_kesehatan: '',
}

export function PendukungPandu(props) {
	const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)
	
	const TOKEN   = isAuthorized.accessToken
	const history = useHistory()
	const PARENT  = props.match.url.split("/")[1]
	const MENU 	  = props.match.url.split("/")[2]
	const ACTION  = props.match.params.action
	const KEY 	  = props.match.params.key

	const PARENTURL = `${PARENT}/${MENU}`

	const [data, setData] = useState(defaultData)
	const [sertifikat, setSertifikat] = useState([])

	const [cabang, setCabang] = useState([])
	const [tipePersonil, setTipePersonil] = useState([])
	const [kapal, setKapal] = useState([])

	const [radio, setRadio] = useState(0)
	const [display, setDisplay] = useState('')
	const [cabangId, setCabangId] = useState(isAuthorized.cabang_id)
	const [tipeAssetId, setTipeAssetId] = useState(0)
	const [errMessage, setErrMessage] = useState(false)
	const getCabang = async () => {
		await Engine.GET('cabang', TOKEN)
		.then(data => {
			setCabang(data)
        }).catch(e => console.log(e))
	}

	const getPersonil = async () => {
		await Engine.GET('tipepersonil/?flag=pendukung', TOKEN)
		.then(data => {
			setTipePersonil(data)
        }).catch(e => console.log(e))
	}

	console.log("cabang",cabangId)

	const getKapal = async () => {
		if (tipeAssetId > 0 && cabangId > 0) {
			await Engine.GET('assetkapal?tipe_asset_id='+tipeAssetId+'&cabang_id='+cabangId+'&enable=1&approval_status_id=1', TOKEN)
			.then(data => {
				setKapal(data)
	        }).catch(e => console.log(e))
		}
	}

	const getData = async () => {
		await Engine.GET(`personil/${KEY}`, TOKEN)
		.then((data) => {
			console.log("GET DATA", data)
			setData(data)
			setSertifikat(data['sertifikat'])
			setCabangId(data.cabang_id)

			let tipePersonil = data.tipe_personil[0]
			setTipeAssetId(tipePersonil.tipe_asset_id)
			if (tipePersonil.tipe_asset_id < 1) {
				setDisplay('none')
			}
			setRadio(tipePersonil.radio)
        }).catch(e => console.log(e) )
	}

	useEffect(() => {
		getKapal()
		getCabang()
		getPersonil()
		if (ACTION !== 'create') {
			getData()
		}
		if (ACTION === 'create') {
			setData(prev => ({ ...prev, 'cabang_id': isAuthorized.cabang_id }))
		}
		validation()
	}, [cabangId, ACTION, validType])

	useEffect(() => {
		getKapal()
		
	}, [tipeAssetId])
	const dataHandle = async (e) => {
		const target = e.target

		if (target.name === 'nipp') {
            if (parseInt(target.value.length) <= 9) {
                setErrMessage(prev => ({...prev, 'max': null}))
            }else {
                setErrMessage(prev => ({...prev, 'max': 'NIPP maksimal 9 digit'}))
				return false;
            }
        }

		if (target.name === 'tipe_personil_id') {
			let index = target.selectedIndex
            let element = target.childNodes[index]
            let tipe_asset_id = element.getAttribute('data-tipe_asset_id')
            let fradio = element.getAttribute('data-radio')
			console.log("aabbcc",radio,tipe_asset_id)

            if (fradio > 0) {
            	setRadio(fradio)
            }
            if (tipe_asset_id < 1) {
            	setDisplay('none')
            	setData(prev => ({ ...prev, 'asset_kapal_id': '' }))
            	setData(prev => ({ ...prev, 'manning': '' }))
            }else {
            	setDisplay('')
            }

			setTipeAssetId(tipe_asset_id)
			
		}
		if (target.type === 'file') {
            if (target.files[0] ) {
				if (target.files[0].size <= 15000000) {
					let base64 = await convertBase64(target.files[0])
					setData(prev => ({ ...prev, [target.name]: base64 }))
					setErrMessage(prev => ({ ...prev, [target.name]: '' })) 
				}else{
					console.log('aaaa')
					setData(prev => ({ ...prev, [target.name]: '' })) 
					setErrMessage(prev => ({ ...prev, [target.name]: 'Ukuran file tidak boleh melebihi 15 MB' })) 
					
				}
            	return false
            }
        }
		setData(prev => ({ ...prev, [target.name]: target.value }))
	}

	console.log('data', data)
	console.log('sertifikat', sertifikat)
	console.log('tipeAssetId', tipeAssetId)
	console.log('cabangId', cabangId)
	// console.log('kapal', kapal)

	const [remark, setRemark] = useState('')
    const [modal, setModal] = useState(false)

    const modalHandle = (e) => {
        setModal(e)
    }

    const remarkHandle = (e) => {
        setRemark(e.target.value)
    }

	const submitHandle = async (e) => {
		e.preventDefault()

		if (modal === false) {
            setModal(true)
            return false
        }

	    console.log('SUBMIT DATA',data)
	    data.approval_status_id = 0
	    Object.assign(data, {'activity_keterangan': remark})
	    data['sertifikat'] = sertifikat

		const method = ACTION === 'create' ? 'POST' : 'PUT'
		const URL    = ACTION === 'create' ? 'personil' : 'personil'+'/'+KEY
		await Engine.POST(URL, method, data, TOKEN)
		.then(response => {
			if (response) {
				history.push(`/${PARENTURL}`)
			}
		})
	}

	const disabled = ACTION === 'detail'

	const [validType, setValidType] = useState(['create','edit','detail'])
    const validation = () => {
        if (isAuthorized.cabang_id < 1) {
            setValidType(['detail'])
        }
        validType.includes(ACTION) === false && history.push('/error/error-v1')
    }

    return (
		<div className="row">
			<div className="col-12">
				<form onSubmit={submitHandle}>
					<div className="card mb-5">
						<div className="card-body">
							<h3 className="font-weight-bold mb-3">Info Umum</h3>
							<div className="row">
								<div className="col-md-6">
									{/*<div className="form-group">
										<label>Cabang</label>
										<select name="cabang_id" 
											onChange={dataHandle} 
											value={data.cabang_id}
											className="form-control bg-white"
											required={true}
											disabled={disabled}
										>
											<option value="">Pilih Cabang</option>
											{cabang.map((v,i) => {
												return(<option key={i} value={v.id}>{v.nama}</option>)
											})}
										</select>
									</div>*/}
									<div className="form-group">
										<label>Nama <span className="text-red">*</span></label>
										<input type="text" name="nama" 
											onChange={dataHandle} 
											value={data.nama}
											className="form-control bg-white" 
											required={true}
											disabled={disabled}
										/>
									</div>
									<div className="form-group">
										<label>NIPP <span className="text-red">*</span></label>
										<input type="number" name="nipp" 
											onChange={dataHandle} 
											value={data.nipp}
											className="form-control bg-white" 
											required={true}
											disabled={disabled}
										/>
										{<span className="text-danger d-block mt-2">{errMessage.max}</span>}
									</div>
									<div className="form-group" style={{'display':display}}>
										<label>Jabatan {display == 'none' ? "" : <span className="text-red">*</span>}</label>
										<select name="jabatan" 
											onChange={dataHandle} 
											value={data.jabatan || ''}
											className="form-control bg-white"
											required={display == 'none' ? false : true}
											disabled={disabled}
										>
											<option value="">Pilih Jabatan</option>
											{jabatan_pendukung_pandu.map((v,i) => {
												return(<option key={i} value={v.name}>{v.name}</option>)
											})}
										</select>
									</div>
									<div className="form-group">
										<label>Status <span className="text-red">*</span></label>
										<select name="enable" 
											onChange={dataHandle} 
											value={data.enable}
											className="form-control bg-white"
											required={true}
											disabled={disabled}
										>
											<option value="">Pilih Status</option>
											<option value="0">Tidak Aktif</option>
											<option value="1">Aktif</option>
										</select>
									</div>
									<div className="form-group">
										<label>Tempat Lahir <span className="text-red">*</span></label>
										<input type="text" name="tempat_lahir" 
											onChange={dataHandle} 
											value={data.tempat_lahir}
											className="form-control bg-white" 
											required={true}
											disabled={disabled}
										/>
									</div>
								</div>
								<div className="col-md-6">
									<div className="form-group">
										<label>Tanggal Lahir <span className="text-red">*</span></label>
										<InputDate 
											name="tanggal_lahir" 
											dataHandle={dataHandle} 
											value={data.tanggal_lahir} 
											required={true}
											disabled={disabled}
										/>
									</div>
									<div className="form-group">
										<label>Tipe Personil <span className="text-red">*</span></label>
										<select name="tipe_personil_id" 
											onChange={dataHandle} 
											value={data.tipe_personil_id}
											className="form-control bg-white"
											required={true}
											disabled={disabled}
										>
											<option value="">Pilih Tipe Personil</option>
											{tipePersonil.map((v,i) => {
												return(
													<option 
														key={i} 
														data-tipe_asset_id={v.tipe_asset_id} 
														data-radio={v.radio} 
														value={v.id}
													>{v.nama}</option>
												)
											})}
										</select>
									</div>
									<div className="form-group" style={{'display':display}}>
										<label>Kapal {display == 'none' ? "" : <span className="text-red">*</span>}</label>
										<select name="asset_kapal_id" 
											onChange={dataHandle} 
											value={data.asset_kapal_id || ''}
											className="form-control bg-white"
											required={display == 'none' ? false : true}
											disabled={disabled}
										>
											<option value="">Pilih Kapal</option>
											{kapal.map((v,i) => {
												return(<option key={i} value={v.id}>{v.nama_asset}</option>)
											})}
										</select>
									</div>
									<div className="form-group" style={{'display':display}}>
										<label>Manning Agency {display == 'none' ? "" : <span className="text-red">*</span>}</label>
										<select name="manning" 
											onChange={dataHandle} 
											value={data.manning || ''}
											className="form-control bg-white"
											required={display == 'none' ? false : true}
											disabled={disabled}
										>
											<option value="">Pilih</option>
											<option value="1">DUB</option>
											<option value="2">MSM</option>
											<option value="3">MCSI</option>
										</select>
									</div>
									{/* <div className="form-group" style={{'display':planerdisplay}}>
										<label>Pendidikan {planerdisplay == 'none' ? "" : <span className="text-red">*</span>}</label>
										<select name="pendidikan" 
											onChange={dataHandle} 
											value={data.pendidikan || ''}
											className="form-control bg-white"
											required={planerdisplay == 'none' ? false : true}
											disabled={disabled}
										>
											<option value="">Pilih Pendidikan</option>
											<option value="D3/D4">D3/D4</option>
											<option value="S1">S1</option>
											<option value="S2">S2</option>
											<option value="S3">S3</option>
										</select>
									</div> */}
								</div>
							</div>
						</div>
					</div>

					<Remark 
	                    modal={modal} 
	                    modalHandle={modalHandle} 
	                    remarkHandle={remarkHandle} 
	                    remark={remark}
	                    submitHandler={submitHandle}
	                />

					<TempatTugas
						dataHandle={dataHandle}
						data={data}
						disabled={disabled}
						cabang={cabang}
						action={ACTION}
						errmessage = {errMessage}
					/>

					{/*<Skppp
						dataHandle={dataHandle}
						data={data}
						disabled={disabled}
						action={ACTION}
					/>*/}

					<SertifikatKesehatan
						dataHandle={dataHandle}
						data={data}
						disabled={disabled}
						action={ACTION}
						errmessage = {errMessage}
					/>

					<Sertifikat
						setSertifikat={setSertifikat}
						sertifikat={sertifikat}
						radio={radio}
						disabled={disabled}
						action={ACTION}
						errmessage = {errMessage}
					/>
					
					{ACTION !== 'detail' &&
						<button type="submit" className="btn btn-primary float-right px-5">
							Simpan Pendukung Pandu
						</button>
					}

					{ACTION === 'detail' && data['activityLog'] !== undefined &&
						<ActivityLog activityLog={data.activityLog} />
					}
				</form>
			</div>
		</div>
    )
}

const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
        const fileReader = new FileReader()
        fileReader.readAsDataURL(file)
        // fileReader.readAsText(file)
        fileReader.onload = () => {
            resolve(fileReader.result)
        }
        fileReader.onerror = (error) => {
            reject(error)
        }
    })
}