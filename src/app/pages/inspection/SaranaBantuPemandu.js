import React, { useState, useEffect } from "react";
import { Tab, Tabs } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";

import { InputDate } from "../../../simpanda/components/InputDate"
import { Engine } from "../../../simpanda/config/Engine";
import { question } from "../../../simpanda/config/question";
import moment from 'moment';

export function SaranaBantuPemandu(props) {
	const {isAuthorized} = useSelector(({auth}) => ({isAuthorized: auth.user}),shallowEqual)
	
	const TOKEN   = isAuthorized.accessToken
	const history = useHistory()
	const PARENT  = props.match.url.split("/")[1];
	const MENU 	  = props.match.url.split("/")[2];
	const ACTION  = props.match.params.action;
	const KEY 	  = props.match.params.key;

	const PARENTURL = `${PARENT}/${MENU}`;

	const [cabang, setCabang] = useState([])
	const [tipeKapal, setTipeKapal] = useState([])
	const [kapal, setKapal] = useState([])
	const [step, setStep] = useState(0)

	const [data, setData] = useState({
		'cabang_id': isAuthorized.cabang_id,
		'pelaksana': '',
		'tanggal_pemeriksaan': '',
		'personil_id': '',
		'jabatan': 'Master',
		'personil_id_kkm': '',
		'kkm_jabatan': 'Chief Engineer',
		'tipe_asset_id': '',
		'status_ijazah_id': '',
		'asset_kapal_id': '',
	})

	const [questionValue, setQuestionValue] = useState([])
	const [tipeAssetId, setTipeAssetId] = useState(false)
	const [tipePersonilId, setTipePersonilId] = useState(false)
	const [assetKapalId, setAssetKapalId] = useState(false)
	const [cabangId, setCabangId] = useState(isAuthorized.cabang_id)
	
	const [nahkoda, setNahkoda] = useState('')
	const [kkm, setKkm] = useState('')
	
	const [tipeasset, setTipeasset] = useState(false)
	const [textKapal, setTextKapal] = useState(false)

	const getCabang = async () => {
		await Engine.GET('cabang?id='+cabangId, TOKEN)
		.then(data => {
			setCabang(data[0])
			// console.log("CABANG", data)
        }).catch(e => console.log(e))
	}

	const getTipeKapal = async () => {
		await Engine.GET('tipeasset/?flag=kapal', TOKEN)
		.then((data) => {
			setTipeKapal(data);
			// console.log("KAPAL", data);
        }).catch(e => console.log(e) )
	}

	const getKapal = async () => {
		if (tipeAssetId > 0 && cabangId > 0) {
			await Engine.GET('assetkapal/?tipe_asset_id='+tipeAssetId+'&cabang_id='+cabangId+'&enable=1&approval_status_id=1', TOKEN)
			.then((data) => {
				setKapal(data)
				// console.log("KAPAL", data)
	        }).catch(e => console.log(e) )
		}
	}

	const getNahkoda = async () => {
		if (cabangId > 0 && tipePersonilId > 0 && assetKapalId > 0) {
			let url = 'personil?cabang_id='+cabangId+'&tipe_personil_id='+tipePersonilId+'&asset_kapal_id='+assetKapalId+'&enable=1&approval_status_id=1&jabatan=Master'
			await Engine.GET(url, TOKEN)
			.then(data => {
				if (data.length > 0) {
					setNahkoda(data[0])
					setData(prev => ({...prev, ['personil_id']: data[0].id}))
				}else{
					setNahkoda('')
					setData(prev => ({...prev, ['personil_id']: ''}))
				}
				
				console.log("get nahkoda", data)
	        }).catch(e => console.log(e))
		}
	}

	const getKkm = async () => {
		if (cabangId > 0 && tipePersonilId > 0 && assetKapalId > 0) {
			let url = 'personil?cabang_id='+cabangId+'&tipe_personil_id='+tipePersonilId+'&asset_kapal_id='+assetKapalId+'&enable=1&approval_status_id=1&jabatan=Chief Engineer'
			await Engine.GET(url, TOKEN)
			.then(data => {
				if (data.length > 0) {
					setKkm(data[0])
					setData(prev => ({...prev, ['personil_id_kkm']: data[0].id}))
				}else{
					setKkm('')
					setData(prev => ({...prev, ['personil_id_kkm']: ''}))
				}
				
				console.log("get kkm", data)
	        }).catch(e => console.log(e))
		}
	}

	useEffect(() => {
		getKapal()
		getNahkoda()
		getKkm()
		getCabang()
		getTipeKapal()
		
		if (ACTION !== 'create') {
			getData()
		}
		validation()
	}, [tipeAssetId, cabangId, tipePersonilId, assetKapalId, ACTION, validType])
	
	const getData = async () => {
		await Engine.GET(`saranabantupemandu/${KEY}`, TOKEN)
		.then((data) => {
			console.log("GET DATA", data)
			setData(data)
			setTipeAssetId(data.tipe_asset_id)
			setCabangId(data.cabang_id)
			// setJabatan(data.jabatan || '')
			const questionSort = data['question'].sort((a,b) => (a.question_id > b.question_id) ? 1 : ((b.question_id > a.question_id) ? -1 : 0))
			setQuestionValue(questionSort)
			setTipeasset(data.sarana_config_question)
			setTipePersonilId(data.tipe_personil_id)
			setAssetKapalId(data.asset_kapal_id)
			getNahkoda()
        }).catch(e => console.log(e) )
	}

	const nextHandle = (e) => {
		e.preventDefault()
		setStep(current => {
	    	return current + 1
	    })
	}

	const prevHandle = () => {
		if (step > 0) {
			setStep(current => {
		    	return current - 1
		    })
		}
	}

	const dataHandle = (e) => {
		const {name, value} = e.target
		setData(prev => ({ ...prev, [name]: value }))
		if (name == 'tipe_asset_id') {
			setQuestionValue([])
			let index = e.target.selectedIndex
			let optionElement = e.target.childNodes[index]
			let ElConfig =  optionElement.getAttribute('data-config')
			let ElTipePersonilId = optionElement.getAttribute('data-tipe_personil_id')
			setTipeasset(ElConfig)
			setTipePersonilId(ElTipePersonilId)
			setQuestion(question, ElConfig)
			setTipeAssetId(e.target.value)
		}
		if (name == 'cabang_id') {
			setCabangId(e.target.value)
		}
		if (name == 'asset_kapal_id') {
			let index = e.target.selectedIndex
			let optionElement = e.target.childNodes[index]
			setTextKapal(optionElement.text)
			setAssetKapalId(e.target.value)
			if (e.target.value < 1) {
				setNahkoda('')
				setKkm('')
				setData(prev => ({...prev, ['personil_id_kkm']: ''}))
				setData(prev => ({...prev, ['personil_id']: ''}))
			}
		}
		// if (name == 'personil_id') {
		// 	let temp = personil.filter(x=> parseInt(x.id) === parseInt(value))
		// }
	}

	const setQuestion = (question, tipeasset) => {
		question[tipeasset].map(parent => {
			parent.content.map(content => {
				content.question.map(question => {
					setQuestionValue(prev => ([...prev, {'question_id': question.id, 'value': 0, 'keterangan_sarana': ''}]))
				})
			})
		})
	}

	const questionHandle = (e) => {
		const target = e.target
		let tempState = [...questionValue]
		let tempElement = { ...tempState[target.dataset.index] }

		if (target.name === 'keterangan_sarana') {
			tempElement['keterangan_sarana'] = target.value
		}else {
			tempElement['value'] = target.value
		}
		
		tempState[target.dataset.index] = tempElement
		setQuestionValue(tempState)
	}

	console.log('step', step)
	console.log('data', data)
	console.log('nahkoda', nahkoda)
	console.log('kkm', kkm)
	console.log('cabang', cabang)
	// console.log('questionValue', questionValue)
	// console.log('tipeAssetId', tipeAssetId)
	// console.log('tipeasset', tipeasset)
	// console.log('textKapal', textKapal)
	// console.log('question',question)
	// console.log('kapal', kapal)

	const submitHandle = async (e) => {
		e.preventDefault()
		data['question'] = questionValue
		if (data['question']){
			data['approval_status_id'] = 0;
		}
	    console.log('SUBMIT DATA',data)

		const method = ACTION === 'create' ? 'POST' : 'PUT'
		const URL    = ACTION === 'create' ? 'saranabantupemandu' : 'saranabantupemandu'+'/'+KEY
		await Engine.POST(URL, method, data, TOKEN)
		.then(response => {
			if (response) {
				history.push(`/${PARENTURL}`)
			}
		})
	}

	const [validType, setValidType] = useState(['create','edit','detail'])
    const validation = () => {
        if (isAuthorized.cabang_id < 1) {
            setValidType(['detail'])
        }
        validType.includes(ACTION) === false && history.push('/error/error-v1')
    }

	let index = -1

    return (
		<div className="row">
			<div className="col-12">
				{step < 1 && ACTION !== 'detail' &&
					<form onSubmit={nextHandle}>
						<div className="card mb-5">
							<div className="card-body">
								<h5 className="font-weight-bold mb-3">Inspeksi Sarana Bantu Pemanduan</h5>
								<div className="row">
									<div className="col-md-6">
										<div className="form-group">
											<label>Cabang <span className="text-red">*</span></label>
											{/*<select name="cabang_id" 
												onChange={dataHandle} 
												value={data.cabang_id}
												className="form-control"
												required={true}
											>
												<option value="">Pilih Cabang</option>
												{cabang.map((v,i) => {
													return(<option key={i} value={v.id}>{v.nama}</option>)
												})}
											</select>*/}
											<input type="text" 
												name="cabang_id" 
												value={cabang.nama}
												className="form-control" 
												readOnly={true}
											/>
										</div>
										<div className="form-group">
											<label>Pelaksana <span className="text-red">*</span></label>
											<input type="text" name="pelaksana" 
												onChange={dataHandle} 
												value={data.pelaksana}
												className="form-control" 
												required={true}
											/>
										</div>
									</div>
									<div className="col-md-6">
										<div className="form-group">
											<label>Tanggal Pemeriksaan <span className="text-red">*</span></label>
											{/*<input type="date" name="tanggal_pemeriksaan" 
												onChange={dataHandle} 
												value={moment(data.tanggal_pemeriksaan).format('YYYY-MM-DD')}
												className="form-control" 
												required={true}
											/>*/}
											<InputDate 
												name="tanggal_pemeriksaan" 
												dataHandle={dataHandle} 
												value={data.tanggal_pemeriksaan} 
												required={true}
											/>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className="card mb-5">
							<div className="card-body">
								<h5 className="font-weight-bold mb-3">Dokumen Nahkoda dan KKM Kapal</h5>
								<div className="row">
									<div className="col-md-6">
										<div className="form-group">
											<label>Tipe Kapal <span className="text-red">*</span></label>
											<select 
												name="tipe_asset_id" 
												onChange={dataHandle} 
												value={data.tipe_asset_id}
												className="form-control bg-white"
												required={true}
												disabled={ACTION !== 'create' ? true : false}
											>
												<option value="">Pilih Tipe Kapal</option>
												{tipeKapal.map((v,i) => {
													return(
														<option 
															key={i} 
															data-config={v.sarana_config_question}
															data-tipe_personil_id={v.tipe_personil_id}
															value={v.id}
														>{v.nama}</option>
													)
												})}
											</select>
										</div>
										<div className="form-group">
											<label>Kapal <span className="text-red">*</span></label>
											<select name="asset_kapal_id" 
												onChange={dataHandle} 
												value={data.asset_kapal_id}
												className="form-control bg-white"
												required={true}
												disabled={ACTION !== 'create' ? true : false}
											>
												<option value="">Pilih Kapal</option>
												{kapal.map((v,i) => {
													return(
														<option key={i} value={v.id}>{v.nama_asset}</option>
													)
												})}
											</select>
										</div>
									</div>
									<div className="col-md-6">
										<div className="form-group">
											<label>Nama Nakhoda <span className="text-red">*</span></label>
											<input 
												type="text" 
												name="personil_id" 
												value={nahkoda ? nahkoda.nama : ''}
												className="form-control readonly" 
												required={true}
												disabled={true}
												readOnly
											/>
											{/*<select 
												name="personil_id" 
												onChange={dataHandle} 
												value={data.personil_id}
												className="form-control"
												required={true}
											>
												<option value="">Pilih Personil</option>
												{personil.map((v,i) => {
													return(<option key={i} value={v.id}>{v.nama}</option>)
												})}
											</select>*/}
										</div>
										<div className="form-group">
											<label>Nama KKM <span className="text-red">*</span></label>
											<input 
												type="text" 
												name="personil_id_kkm" 
												value={kkm ? kkm.nama : ''}
												className="form-control readonly" 
												required={true}
												disabled={true}
												readOnly
											/>
											{/*<select name="personil_id_kkm" 
												onChange={dataHandle} 
												value={data.personil_id_kkm}
												className="form-control"
												required={true}
											>
												<option value="">Pilih Personil</option>
												{kkm.map((v,i) => {
													return(<option key={i} value={v.id}>{v.nama}</option>)
												})}
											</select>*/}
										</div>
										<div className="form-group">
											<label>Status Ijazah ? <span className="text-red">*</span></label>
											<div className="pt-3">
												<div className="form-check form-check-inline">
													<input type="radio" id="valid" 
														name="status_ijazah_id" className="form-check-input" 
														onChange={dataHandle}
														value="1"
														checked={
															data['status_ijazah_id'] == 1 ? true : false
														}
														required={true}
													/>
													<label className="form-check-label" htmlFor="valid">Valid</label>
												</div>
												<div className="form-check form-check-inline">
													<input type="radio" id="tidak-valid" 
														name="status_ijazah_id" className="form-check-input"
														onChange={dataHandle}
														value="2"
														checked={
															data['status_ijazah_id'] == 2 ? true : false
														}
														required={true}
													/>
													<label className="form-check-label" htmlFor="tidak-valid">Tidak Valid</label>
												</div>
												<div className="form-check form-check-inline">
													<input type="radio" id="tidak-ada" 
														name="status_ijazah_id" className="form-check-input"
														onChange={dataHandle}
														value="0"
														checked={
															parseInt(data['status_ijazah_id']) == 0 ? true : false
														}
														required={true}
													/>
													<label className="form-check-label" htmlFor="tidak-ada">Tidak Ada</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<button 
							type="submit" 
							className="btn btn-primary float-right px-5"
							disabled={data.personil_id > 0 && data.personil_id_kkm > 0 ? false : true} 
						>
							Selanjutnya
						</button>
						<button type="button" className="btn btn-secondary float-right px-5 mx-3" 
							onClick={prevHandle}>Sebelumnya</button>
					</form>
				}

				{step > 0 && ACTION !== 'detail' && 
					<form onSubmit={submitHandle} >
						{question[tipeasset].map((parent, p) => {
							return (
								<div className="card card-body mb-5" key={p}>
									<h3 className="font-weight-bold mb-3">{parent.title}</h3>
									{parent.content.map((content, c) => {
										return (
											<div key={c}>
												<p className="font-weight-bold my-5 ml-3">
													{content.subtitle && <span className="font-weight-bold mr-2">#</span>}
													{content.subtitle}
												</p>
												{content.question.map((question, q) => {
													{index++}
													{/*{console.log("index", index, ' id ', question.id)}*/}
													return (
														<div className="row py-5 ml-5" key={q}>
															<div className="col-md-7">
																<p className="font-14 mt-2">{question.text}</p>
															</div>
															<div className="col-md-5">
																<div className="form-check form-check-inline mr-5">
																	<input type="radio" id={`valid_${index}`} 
																		name={`value_${index}`} className="form-check-input" 
																		onChange={questionHandle}
																		data-index={index}
																		value="1"
																		checked={
																			question.value
																			? parseInt(question.value) === 1 ? true : false
																			: parseInt(questionValue[index]['value']) === 1 ? true : false
																		}
																		required={true}
																		disabled={question.disabled}
																	/>
																	<label className="form-check-label" htmlFor={`valid_${index}`}>
																		{content.radio ? content.radio[0] : 'Baik'}
																	</label>
																</div>
																<div className="form-check form-check-inline mx-5">
																	<input type="radio" id={`tidak_valid_${index}`} 
																		name={`value_${index}`} className="form-check-input"
																		onChange={questionHandle}
																		data-index={index}
																		value="2"
																		checked={
																			question.value
																			? parseInt(question.value) === 2 ? true : false
																			: parseInt(questionValue[index]['value']) === 2 ? true : false
																		}
																		required={true}
																		disabled={question.disabled}
																	/>
																	<label className="form-check-label" htmlFor={`tidak_valid_${index}`}>
																		{content.radio ? content.radio[1] : 'Tidak Baik'}
																	</label>
																</div>
																<div className="form-check form-check-inline ml-5">
																	<input type="radio" id={`tidak_ada_${index}`} 
																		name={`value_${index}`} className="form-check-input"
																		onChange={questionHandle}
																		data-index={index}
																		value="0"
																		checked={
																			question.value
																			? parseInt(question.value) === 0 ? true : false
																			: parseInt(questionValue[index]['value']) === 0 ? true : false
																		}
																		required={true}
																		disabled={question.disabled}
																	/>
																	<label className="form-check-label" htmlFor={`tidak_ada_${index}`}>
																		{content.radio ? content.radio[2] : 'Tidak Ada'}
																	</label>
																</div>
																<textarea
																	name="keterangan_sarana" 
																	className="form-control col-12 mt-2"
																	rows="3"
																	onChange={questionHandle}
																	value={
																		questionValue[index]['keterangan_sarana'] === 'null' || questionValue[index]['keterangan_sarana'] === null 
																		? ''
																		: questionValue[index]['keterangan_sarana']
																	}
																	data-index={index}
																	required={false}
																	placeholder="Tulis keterangan"
																></textarea>
																{/*<input type="text"
																	name="keterangan_sarana" className="form-control col-12 mt-2"
																	onChange={questionHandle}
																	value={questionValue[index]['keterangan_sarana']}
																	data-index={index}
																	required={false}
																	placeholder="Tulis keterangan"
																/>*/}
															</div>
														</div>
													)
												})}
											</div>
										)
									})}
								</div>
							)
						})}
						<button type="submit" className="btn btn-primary float-right px-5">
							Submit
						</button>
						<button type="button" className="btn btn-secondary float-right px-5 mx-3" 
							onClick={prevHandle}>Sebelumnya</button>
					</form>
				}
			</div>

			{ACTION === 'detail' && data.question && <TableView data={data} question={question} /> }

		</div>
    )
}

const TableView = ({data, question}) => {
	let index = -1
	return(
		<React.Fragment>
			<div className="col-12">
				<div className="card p-5">
					<h3 className="my-5">Detail Umum</h3>
					<table className="table">
						<thead>
							<tr>
								<th>Lokasi / Perairan</th>
								<th>Pelaksana</th>
								<th>Tanggal Pemeriksaan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{data.cabang}</td>
								<td>{data.pelaksana}</td>
								<td>{moment(data.tanggal_pemeriksaan).format("DD MMM YYYY")}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div className="col-12 my-5">
				<div className="card p-5">
					<h3 className="my-5">
						Dokumen Nahkoda dan KKM Kapal 
						{/*{data.asset_kapal}*/}
					</h3>
					<table className="table">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Jabatan</th>
								<th>Ijazah</th>
								<th>Tipe Kapal</th>
								<th>Asset Kapal</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{data.nama_1}</td>
								<td>{data.jabatan}</td>
								<td>{data.status_ijazah_id == 1 ? 'Valid' : data.status_ijazah_id == 2 ? 'Tidak Valid' : 'Tidak Ada'}</td>
								<td>{data.tipe_asset}</td>
								<td>{data.asset_kapal}</td>
							</tr>
							<tr>
								<td>{data.personil_kkm}</td>
								<td>{data.kkm_jabatan}</td>
								<td>{data.status_ijazah_id == 1 ? 'Valid' : data.status_ijazah_id == 2 ? 'Tidak Valid' : 'Tidak Ada'}</td>
								<td>{data.tipe_asset}</td>
								<td>{data.asset_kapal}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div className="col-12">
				<div className="card p-5">
					<h3 className="my-5">
						{`Dokumen Kapal ` + data.tipe_asset} 
						{/*{data.asset_kapal}*/}
					</h3>
					<div className="table-responsive">
						<table className="table table-hover table-bordered">
							<thead className="thead-light">
								<tr>
									<th>Item Pengecekan Kapal</th>
									<th className="text-center">Baik</th>
									<th className="text-center">Tidak Baik</th>
									<th className="text-center">Tidak Ada</th>
									<th>keterangan</th>
								</tr>
							</thead>
							<tbody>
								{question[data.sarana_config_question].map((parent, p) => {
									return (
										<React.Fragment key={p}>
											<tr>
												<td colSpan="5" className="py-5 align-middle text-success font-weight-bold w-45">
													{parent.title}
												</td>
											</tr>
											{parent.content.map((content, c) => {
												return(
													<React.Fragment key={c}>
														{content.subtitle && 
															<tr>
																<td colSpan="5" className="py-5 align-middle">
																	<span className="font-weight-bold mr-2 text-info font-italic">{content.subtitle}</span>
																</td>
															</tr>
														}
														{content.question.map((question, q) => {
															{index++}
															return(
																<tr key={q}>
																	<td className="py-5 align-middle">
																		{question.text}
																	</td>
																	<td className="py-5 align-middle text-center w-10">
																		{data.question && data.question[index]['value'] == 1 ? <i className="fa fa-check text-success"></i> : false}
																	</td>
																	<td className="py-5 align-middle text-center w-10">
																		{data.question && data.question[index]['value'] == 2 ? <i className="fa fa-check text-success"></i> : false}
																	</td>
																	<td className="py-5 align-middle text-center w-10">
																		{data.question && parseInt(data.question[index]['value']) == 0 ? <i className="fa fa-check text-success"></i> : false}
																	</td>
																	<td className="py-5 align-middle w-25">
																		{data.question[index]['keterangan_sarana'] !== 'null'
																			? data.question[index]['keterangan_sarana']
																			: ''
																		}
																	</td>
																</tr>
															)
														})}
													</React.Fragment>
												)
											})}
										</React.Fragment>
									)
								})}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}
