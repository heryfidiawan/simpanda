import React, { useState, useEffect } from "react"
import { useHistory } from "react-router-dom"
import { APIS, Engine } from "../../../simpanda/config/Engine"
import { shallowEqual, useSelector } from "react-redux"
import { Image } from "../../../simpanda/components/Image"
import { InputDate } from "../../../simpanda/components/InputDate"
import Swal from "sweetalert2"

import moment from 'moment'

const iconUplod = '/media/icons/upload.png'
const pdfThumb  = '/media/simpanda/pdf-thumb.png'
const backEnd   = APIS

const defaultCheck = {
	'id': '',
	'kondisi_id': 1,
	'tanggal_awal': '',
	'tanggal_akhir': '',
	'keterangan': '',
	'status': 0,
	'gambar': '',
	'filetext': iconUplod,
}

export function PemeriksaanKapal(props) {
	const { isAuthorized } = useSelector(({ auth }) => ({ isAuthorized: auth.user }), shallowEqual)

	const TOKEN = isAuthorized.accessToken
	const history = useHistory()
	const PARENT = props.match.url.split("/")[1];
	const MENU = props.match.url.split("/")[2];
	const ACTION = props.match.params.action;
	const KEY = props.match.params.key;

	const PARENTURL = `${PARENT}/${MENU}`

	const [base, setBase] = useState([])
	const [kapal, setKapal] = useState([])
	const [cabang, setCabang] = useState([])

	const [data, setData] = useState({'cabang_id': isAuthorized.cabang_id, 'asset_kapal_id':''})
	const [check, setCheck] = useState([])
	const [cabangId, setCabangId] = useState(data.cabang_id)

	const [step, setStep] = useState(-1)
	const [reqGambar, setReqGambar] = useState(false)
	const [showGambar, setShowGambar] = useState('')
	const [disabs, setDisabs] = useState(false)
	
	const [errMessage, setErrMessage] = useState(false)

	const getBase = async () => {
		await Engine.GET('pemeriksaankapalcheck', TOKEN)
			.then(data => {
				setBase(data)
				console.log("BASE", data)
			}).catch(e => console.log(e))
	}

	const getKapal = async () => {
		if (cabangId > 0) {
			await Engine.GET(`assetkapal/?cabang_id=${cabangId}&enable=1&approval_status_id=1`, TOKEN)
				.then(data => {
					setKapal(data)
					// console.log("KAPAL", data)
				}).catch(e => console.log(e))
		}
	}
	
	useEffect(() => {
		getKapal()
		getBase()
		getCabang()
		if (ACTION !== 'create') {
			getData()
		}

		validation()
	}, [cabangId, ACTION, validType])

	useEffect(() => {
		if (ACTION === 'create' && base.length > 0) {
			base.map(result => {
				setCheck(prev => ([...prev, defaultCheck]))
			})
		}
	}, [base])

	const getCabang = async () => {
		await Engine.GET('cabang', TOKEN)
			.then(data => {
				setCabang(data)
				// console.log("CABANG", data)
			}).catch(e => console.log(e))
	}

	const getData = async () => {
		await Engine.GET(`pemeriksaankapal/${KEY}`, TOKEN)
			.then(async (data) => {
				console.log("GET DATA Danang", data)
				setData(data)
				setCabangId(data.cabang_id)
				await data.check.map(result => {
					let filetext = result['gambar'] !== undefined && result.gambar != null ? `${backEnd}${result.gambar}` : iconUplod
					setCheck(current => ([...current, Object.assign(result, {'filetext': filetext})]))
				})
				check.sort((a, b) => {
				    return a.pemeriksaan_kapal_check_id.localeCompare(b.pemeriksaan_kapal_check_id)
				})
			}).catch(e => console.log(e))
	}

	// kondisi
	// 1 => baik
	// 2 => rusak

	// status
	// 1 => open
	// 0 => close

	const nextHandle = (e) => {
		e.preventDefault()
		if (parseInt(step + 1) === parseInt(base.length)) {
			return submitHandle()
		}

		if (step > -1){
			const tanggal_awal = moment(check[step].tanggal_awal).format('YYYYMMDD');
			const tanggal_akhir = moment(check[step].tanggal_akhir).format('YYYYMMDD');
			if (tanggal_awal > tanggal_akhir){
				return Swal.fire({
			        title: 'Perhatian',
			        text: 'Tanggal awal tidak boleh melebihi tanggal akhir',
			        type: 'error',
			    })
			}
		}
		if (step < base.length-1) {
			setStep(current => {
		    	return current + 1
		    })
		}
	}

	const prevHandle = () => {
		if (step > -1) {
			setStep(current => {
		    	return current - 1
		    })
		}
	}

	const dataHandle = (e) => {
		const { name, value } = e.target
		setData(prev => ({ ...prev, [name]: value }))
		if (name == 'cabang_id') {
			setCabangId(value)
		}
	}

	const checkHandle = async (e) => {
		const target = e.target
		check[step].id = step + 1

       	if (target.name == 'kondisi_id') {
	       	let tempState = [...check]
			let tempElement = { ...tempState[step] }

			if (target.value == 1) {
				tempElement['kondisi_id'] = 1
				tempElement['status'] = 0
				tempElement['tanggal_akhir'] = moment().format('YYYY-MM-DD')
			} 
			if (target.value == 2) {
				tempElement['kondisi_id'] = 2
				tempElement['status'] = 1
				tempElement['tanggal_akhir'] = ''
			}

			tempElement['gambar'] = ''
			tempElement['filetext'] = ''
			tempState[step] = tempElement
			setCheck(tempState)
			return false
		}

		if (target.name == 'status') {
	  		let tempState = [...check]
			let tempElement = { ...tempState[step] }

			if (target.value == 1) {
				tempElement['kondisi_id'] = 2
				tempElement['status'] = 1
				tempElement['tanggal_akhir'] = ''
				tempState[step] = tempElement
				setCheck(tempState)
			}else if (target.value == 0) {
				tempElement['kondisi_id'] = 1
				tempElement['status'] = 0
				tempElement['tanggal_akhir'] = moment().format('YYYY-MM-DD')
			}

			tempElement['gambar'] = ''
			tempElement['filetext'] = ''
			tempState[step] = tempElement
			setCheck(tempState)
			return false
		}

		if (target.name == 'gambar' && target.files[0]) {
			let gambar = await convertBase64(target.files[0])
			let objUrl = await URL.createObjectURL(target.files[0])
			let tempState = [...check]
			let tempElement = { ...tempState[step] }
			if (target.files[0].size <= 15000000) {
				tempElement['gambar'] = gambar
				tempElement['filetext'] = objUrl
				setErrMessage('') 
			}else{
				tempElement['gambar'] = ''
				tempElement['filetext'] = ''
				setErrMessage('Ukuran file tidak boleh melebihi 15 MB') 
			}

			tempState[step] = tempElement
			setCheck(tempState)
			return false;
		}

  		let tempState = [...check]
		let tempElement = { ...tempState[step] }
		tempElement[target.name] = target.value
		tempState[step] = tempElement
		setCheck(tempState)
	}
	
	const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader()
            fileReader.readAsDataURL(file)
            // fileReader.readAsText(file)
            fileReader.onload = () => {
                resolve(fileReader.result)
            }
            fileReader.onerror = (error) => {
                reject(error)
            }
        })
    }

    console.log('SHOW', showGambar)
	console.log('data', data)
	console.log('check', check)
	console.log('step', step)
	console.log('kapal', kapal)

	const submitHandle = async () => {
		data['check'] = check

		if (data['check']){
			data['approval_status_id'] = 0;
		}
		const method = ACTION === 'create' ? 'POST' : 'PUT';
		const URL = ACTION === 'create' ? 'pemeriksaankapal' : 'pemeriksaankapal' + '/' + KEY
		console.log('SUBMIT DATA', data)
		await Engine.POST(URL, method, data, TOKEN)
			.then(response => {
				console.log('FORM SUBMIT', data)
				if (response) {
					history.push(`/${PARENTURL}`)
				}
			})
	}

	const [validType, setValidType] = useState(['create','edit','detail'])
    const validation = () => {
        if (isAuthorized.cabang_id < 1) {
            setValidType(['detail'])
        }
        validType.includes(ACTION) === false && history.push('/error/error-v1')
    }

	return (
		<div className="row">
			<div className="col-12">
				{step < 0 && ACTION !== 'detail' &&
					<form onSubmit={nextHandle}>
						<div className="card mb-5">
							<div className="card-body">
								<h5 className="font-weight-bold mb-3">Informasi Umum</h5>
								<div className="row">
									<div className="col-md-6">
										<div className="form-group">
											<label>Cabang</label>
											<select name="cabang_id"
												onChange={dataHandle}
												value={data.cabang_id}
												className="form-control" required >
												<option value="">Pilih Cabang</option>
												{cabang.map((v, i) => {
													return (<option key={i} value={v.id}>{v.nama}</option>)
												})}
											</select>
										</div>
									</div>
									<div className="col-md-6">
										<div className="form-group">
											<label>Kapal</label>
											<select name="asset_kapal_id"
												onChange={dataHandle}
												value={data.asset_kapal_id}
												className="form-control" required >
												<option value="">Pilih Kapal</option>
												{kapal.map((v, i) => {
													return (<option key={i} value={v.id}>{v.nama_asset}</option>)
												})}
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" className="btn btn-primary float-right px-5">Selanjutnya</button>
						<button type="button" className="btn btn-secondary float-right px-5 mx-3" onClick={prevHandle}>Sebelumnya</button>
					</form>
				}

				{base.map((val, index) => {
					return (
						<div key={index}>
							{step >= 0 && index === step &&
								<form onSubmit={nextHandle}>
									<div className="card mb-5">
										<div className="card-body">
											<h5 className="font-weight-bold mb-3 text-primary"><i>Step {index + 1}/{base.length}</i></h5>
											<h3 className="font-weight-bold pb-5">Item Pengecekan Kapal</h3>
											<div className="pb-5 pt-5">
												<p className="font-weight-bold">{index + 1}. {val.question}</p>
											</div>
											<div className="pb-5 pt-5">
												<div className="form-check form-check-inline">
													<input type="radio" id="baik"
														name="kondisi_id" className="form-check-input" required
														onChange={checkHandle}
														value="1"
														checked={
															check[step] && check[step]['kondisi_id'] == 1 ? true : false
														}
													/>
													<label className="form-check-label" htmlFor="baik">Baik</label>
												</div>
												<div className="form-check form-check-inline">
													<input type="radio" id="rusak"
														name="kondisi_id" className="form-check-input"
														onChange={checkHandle}
														value="2"
														checked={
															check[step] && check[step]['kondisi_id'] == 2 ? true : false
														}
													/>
													<label className="form-check-label" htmlFor="rusak">Rusak</label>
												</div>
											</div>
											<div className="row mt-5">
												<div className="col-md-6">
													<div className="form-group">
														<label>Tanggal Inspeksi</label>
														{/*<input 
															type="date" 
															name="tanggal_awal" 
															className="form-control" 
															onChange={checkHandle}
															value={check[step] && moment(check[step]['tanggal_awal']).format('YYYY-MM-DD')}
															required={true}
														/>*/}
														<InputDate 
															name="tanggal_awal" 
															dataHandle={checkHandle} 
															value={check[step] && check[step]['tanggal_awal']} 
															required={true}
														/>
													</div>
												</div>
												<div className="col-md-6">
													<div className="form-group" style={{display: check[step] && check[step]['kondisi_id'] == 1 ? '' : 'none'}}>
														<label>Tanggal Close</label>
														{/*<input type="date" name="tanggal_akhir" className="form-control" 
															onChange={checkHandle}
															value={check[step] && moment(check[step]['tanggal_akhir']).format('YYYY-MM-DD')}
															required={check[step] && check[step]['kondisi_id'] == 1 ? true : false}
															disabled={check[step] && check[step]['kondisi_id'] == 2 ? true : false}
															readOnly={check[step] && check[step]['tanggal_awal'] == ''}
															min={check[step]['tanggal_awal'] ?? ''}
														/>*/}
														<InputDate 
															name="tanggal_akhir" 
															dataHandle={checkHandle} 
															value={check[step] && check[step]['tanggal_akhir']}
															required={check[step] && check[step]['kondisi_id'] == 1 ? true : false}
															disabled={check[step] && check[step]['kondisi_id'] == 2 ? true : false}
															readOnly={check[step] && check[step]['tanggal_awal'] == ''}
															min={check[step]['tanggal_awal'] ?? ''}
														/>
													</div>
												</div>
												<div className="col-12">
													<div className="form-group">
														<label>Keterangan</label>
														<textarea rows="5" name="keterangan" className="form-control" required
															onChange={checkHandle}
															value={check[step] ? check[step]['keterangan'] : ''}
															maxlength="500"
														></textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div className="card mb-5">
										<div className="card-body">
											<div className="row">
												<div className="col-md-6">
													<div className="form-group">
														<label>Status</label>
														<select name="status" 
															onChange={checkHandle}
															value={check[step] && check[step]['kondisi_id'] == 1 ? 0 : 1}
															className="form-control" required >
															<option key={1} value="1">Open</option>
															<option key={0} value="0">Close</option>
														</select>
													</div>
												</div>
												<div className="col-md-6">
													<div className="form-group" style={{display: check[step] && check[step]['kondisi_id'] == 2 ? '' : 'none'}}>
														<label className="d-block">Unggah Gambar</label>
                                                            <Image 
                                                            	data={check[step]} 
                                                            	dataHandle={checkHandle} 
                                                            	name="gambar" 
                                                            	// required={check[step] && check[step]['kondisi_id'] == 2 && check[step]['gambar'] == '' ? true : false}
                                                            />
														{<span className="text-danger d-block mt-2">{errMessage}</span>}
													</div>
												</div>
											</div>
										</div>
									</div>
									<button type="submit" className="btn btn-primary float-right px-5">
										{step === base.length - 1 ? 'Submit' : 'Selanjutnya'}
									</button>
									<button type="button" className="btn btn-secondary float-right px-5 mx-3" onClick={prevHandle}>Sebelumnya</button>
								</form>
							}
						</div>
					)
				})}

				{ACTION === 'detail' &&
					<React.Fragment>
						<div className="card p-5 mb-5">
							<h3 className="mb-5">Detail Umum</h3>
							<div className="row">
								<div className="col">
									<h5>Nama Kapal</h5>
									<p>{data.asset_kapal}</p>
								</div>
								<div className="col">
									<h5>Cabang</h5>
									<p>{data.cabang}</p>
								</div>
							</div>
						</div>

						<div className="card p-5">
							{/*<h3 className="mb-5">Dokumen Kapal</h3>*/}
							<div className="table-responsive">
								<table className="table table-hover table-bordered">
									<thead className="thead-light">
										<tr>
											<th>Item Pengecekan Kapal</th>
											<th>Kondisi Baik</th>
											<th>Kondisi Rusak</th>
											<th>Tanggal Awal</th>
											<th>Tanggal Akhir</th>
											<th>Keterangan</th>
											<th>Temuan</th>
										</tr>
									</thead>
									<tbody>
										{base.map((val, index) => {
											return (
												<tr key={index}>
													<td className="py-5 align-middle w-25">{val.question}</td>
													<td className="py-5 text-center align-middle">
														{check[index] && check[index]['kondisi_id'] == 1 ?
															<i className="fa fa-check text-success"></i> : ''
														}
													</td>
													<td className="py-5 text-center align-middle">
														{check[index] && check[index]['kondisi_id'] == 2 ?
															<i className="fa fa-check text-success"></i> : ''
														}
													</td>
													<td className="py-5 align-middle">
														{check[index] && moment(check[index]['tanggal_awal']).format("DD MMM YYYY")}
													</td>
													<td className="py-5 align-middle">
														{check[index] && check[index]['tanggal_akhir'] ? moment(check[index]['tanggal_akhir']).format("DD MMM YYYY") : "-"}
													</td>
													<td className="py-5 align-middle w-25">{check[index] && check[index]['keterangan']}</td>
													<td className="py-5 align-middle w-25">
														{
															check[index] && check[index]['gambar'] && check[index]['kondisi_id'] == 2 ?
																<a href={check[index] && APIS + check[index]['gambar']}>Download</a>
															: null
														}
													</td>
												</tr>
											)
										})}
									</tbody>
								</table>
							</div>
						</div>
					</React.Fragment>
				}
			</div>
		</div>
	)
}
