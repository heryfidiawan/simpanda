import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import moment from 'moment';
import { APIS, Engine } from "../../../simpanda/config/Engine";
import { Tab, Tabs } from "react-bootstrap";
import { getDate } from "date-fns";
import { Image } from "../../../simpanda/components/Image"
import { InputDate } from "../../../simpanda/components/InputDate"
import Swal from "sweetalert2";

const defaultData = [
    {
        title: "Laporan Bulanan (Setiap 1 Bulan)",
        checkname: "check_laporan_bulanan",
        detailname: "laporan_bulanan",
    },
    {
        title: "Bukti Pembayaran PNBP",
        checkname: 'check_bukti_pembayaran_pnpb',
        detailname: 'bukti_pembayaran_pnpb',
    },
    {
        title: "SISPRO/SOP",
        checkname: 'check_sispro',
        detailname: 'sispro',
    },
    {
        title: "Tarif Jasa Pandu dan Tunda",
        checkname: 'check_tarif_jasa_pandu_tunda',
        detailname: 'tarif_jasa_pandu_tunda',
    },
    {
        title: "Data Dukung Lainnya",
        checkname: 'check_data_dukung',
        detailname: 'data_dukung',
    },
]




export function EvaluasiPelimpahan(props) {
    const { isAuthorized } = useSelector(({ auth }) => ({ isAuthorized: auth.user }), shallowEqual)
    const history = useHistory()

    const PARENT = props.match.url.split("/")[1];
    const MENU = props.match.url.split("/")[2];
    const ACTION = props.match.params.action;
    const KEY = props.match.params.key;
    const PARENTURL = `${PARENT}/${MENU}`;

    const [cabang, setCabang] = useState([])
    const [personil, setPersonil] = useState([])
    const [sertifikat, setSertifikat] = useState([])
    const [kapals, setKapal] = useState([])
    const [data, setData] = useState({'file_pendukung': '', 'file_sk_pelimpahan': '', 'bup': 'PT. Pelabuhan Indonesia II (Persero)'})
    const [keys, setKey] = useState("1_Umum");
    
	const [errMessage, setErrMessage] = useState(false)

    const getCabang = async () => {
        await Engine.GET('cabang', isAuthorized.accessToken)
            .then((data) => {
                setCabang(data)

            })
            .catch((e) => {
                console.log(e);
            });
    };
    
    let tabels = keys.split("_")[0];
    let content = keys.split("_")[1];

    const getPersonil = (cabang_id) => {
        
        Engine.GET(`personil?cabang_id=${cabang_id}&tipe_personil_id=${content}&enable=1&approval_status_id=1`, isAuthorized.accessToken)
            .then((datas) => {
                setPersonil(datas)
                
            })
            .catch((e) => {
                console.log(e);
            });
    };
    const getKapal = (cabang_id) => {
        
        Engine.GET(`assetkapal?cabang_id=${cabang_id}&tipe_asset_id=${content}&enable=1&approval_status_id=1`, isAuthorized.accessToken)
            .then((datas) => {
                setKapal(datas)
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const warningSk = (value) => {
        const Toast = Swal.mixin({
            toast: true,
            position: 'center',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
        })
        
        return Toast.fire({
            background: '#F4F5F7',
            type: 'error',
            title: "Tanggal harus melebihi hari ini"
        })
    };

    

    const dataHandle = async (e) => {
        const { name, value } = e.target
        let valueData = e.target.value

        // if (name == "tanggal_sk" && moment(value).format("YYYYMMDD") <= moment(new Date()).format("YYYYMMDD")){
        // 	return warningSk(value);
        // }
        
        if (e.target.type === "file") {
            if (e.target.files[0]) {
                if (e.target.files[0].size <= 15000000) {
					valueData = await convertBase64(e.target.files[0])
					setData(prev => ({ ...prev, [name]: valueData }))
					setErrMessage(prev => ({ ...prev, [name]: '' })) 
				}else{
                    valueData = ""
					setData(prev => ({ ...prev, [name]: valueData }))
					setErrMessage(prev => ({ ...prev, [name]: 'Ukuran file tidak boleh melebihi 15 MB' }))
					
				}
                
            }
            // setData(prev => ({ ...prev, [name]: valueData }))
        } else {
            setData(prev => ({ ...prev, [name]: value }))
        }

    }

    console.log('data',data)

    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader()
            fileReader.readAsDataURL(file)
            // fileReader.readAsText(file)
            fileReader.onload = () => {
                resolve(fileReader.result)
            }
            fileReader.onerror = (error) => {
                reject(error)
            }
        })
    }

    const radioHandle = (e) => {
        const target = e.target
        if (target.value == 1) {
            setData(prev => ({ ...prev, [target.name]: 1 }))
        } else {
            setData(prev => ({ ...prev, [target.name]: 0 }))
        }
    }

    const handleChange = (e) => {
        console.log('aaaa')
    }
    
    const submitHandle = async (e) => {

        e.preventDefault()
    	// if (moment(data.tanggal_sk).format("YYYYMMDD") <= moment(new Date()).format("YYYYMMDD")){
    	// 	return warningSk(data.tanggal_sk);
     //    }
        data['approval_status_id'] = '0'
        data['cabang_id'] = data['cabang_id'] || isAuthorized.cabang_id;
        data['tanggal_sk'] = data['tanggal_sk'] || moment(new Date()).format("YYYY-MM-DD");
        const method = ACTION === 'create' ? 'POST' : 'PUT';
        const URL = ACTION === 'create' ? 'evaluasipelimpahan' : 'evaluasipelimpahan' + '/' + KEY
        console.log('SUBMIT DATA', data)
        await Engine.POST(URL, method, data, isAuthorized.accessToken)
            .then(response => {
                console.log('FORM SUBMIT', response)
                if (response) {
                    history.push(`/${PARENTURL}`)
                }
            })

    }

    useEffect(() => {

        getCabang()
        if (ACTION !== 'create') {
            Engine.GET(`evaluasipelimpahan/${KEY}`, isAuthorized.accessToken)
                .then((datas) => {
                    setData(datas)
                    getPersonil(datas.cabang_id)
                    getKapal(datas.cabang_id)
                   
                })
                .catch((e) => {
                    console.log(e);
                });
        }
    }, [keys])

    const validType = ['create','edit','detail']
    if (validType.includes(ACTION) === false) {
        history.push(`/${PARENTURL}`)
    }

    const View1 = () => {
        if (keys !== "1_Umum") {
          return null
        }
        return (<>
        <div key="EP6" className="card mb-5">
            <div style={{ padding: '30px' }}>
                <h5>Evaluasi Pelimpahan Pelaksanaan Pandu Tunda</h5>
                <br />
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label htmlFor="cabang_id" className="d-block">Cabang </label>
                            <input plaintext="true" readOnly name="cabang_id" className='form-control-plaintext' value={data.cabang} />
                        </div>
                    </div>
                    <div className="col-md-6">

                        <div className="form-group">
                            <label htmlFor="text" className="d-block">BUP/Terminal Khusus</label>
                            <input plaintext="true" readOnly name="bup" className='form-control-plaintext' value={data.bup} />
                        </div>

                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label htmlFor="text" className="d-block">Izin BUP/ Pengoperasian Tersus</label>
                            <input plaintext="true" readOnly name="izin_bup" className='form-control-plaintext' value={data.izin_bup} />
                        </div>

                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label htmlFor="text" className="d-block">Penetapan Perairan Pandu</label>
                            <input plaintext="true" readOnly name="penetapan_perairan_pandu" className='form-control-plaintext' value={data.penetapan_perairan_pandu} />
                        </div>

                    </div>
                    <div className="col-md-6">

                        <div className="form-group">
                            <label htmlFor="text" className="d-block">Izin Pelimpahan dan Pemanduan</label>
                            <input plaintext="true" readOnly name="izin_pelimpahan" className='form-control-plaintext' value={data.izin_pelimpahan} />
                        </div>

                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label htmlFor="text" className="d-block">Pengawas Pemanduan Setempat</label>
                            <input plaintext="true" readOnly name="pengawas_pemanduan" className='form-control-plaintext' value={data.pengawas_pemanduan} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div key="EP4" className="card mb-5">
            <div style={{ padding: '30px' }}>
                <h5>Administrasi dan Data Dukung</h5>
                <br />
            
                <div className="table-responsive">
                    <table className="table table-hover table-bordered">
                        <thead className="thead-light">
                            <tr>
                                <th className="text-center">No</th>
                                <th className="text-center">Nama</th>
                                <th className="text-center">Ada</th>
                                <th className="text-center">Tidak Ada</th>
                                <th className="text-center">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>

                            {defaultData.map((o, p) => {
                                return (
                                    <tr key={"tr" + p}>
                                        <td className="py-2 align-middle text-center">{"0" + (p + 1)}</td>
                                        <td className="py-5 align-middle">{o.title}</td>

                                        <td className="py-5 align-middle text-center w-10">
                                            {data[o.checkname] === 1 &&
                                                <i className="fa fa-check text-success"></i>
                                            }
                                        </td>
                                        <td className="py-5 align-middle text-center w-10">
                                            {data[o.checkname] === 0 &&
                                                <i className="fa fa-check text-success"></i>
                                            }
                                        </td>
                                        <td className="py-5 align-middle text-center">
                                            {data[o.detailname]}
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>

                <div className="form-group">
                    <label htmlFor="text" className="d-block">Unggah File Pendukung</label>
                    <label htmlFor={"image"}>
                        <Image data={data} dataHandle={dataHandle} name="file_pendukung" required={data['file_pendukung'] ? false : true}/>
                    </label>
                </div>
            </div>
        </div>
        <div key="EP5" className="card mb-5">
                <div style={{ padding: '30px' }}>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="text" className="d-block">Tanggal SK</label>
                                <input type="date" plaintext="true" readOnly value={moment(data.tanggal_sk).format('YYYY-MM-DD')} className="form-control-plaintext" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="text" className="d-block">Unggah SK Pelimpahan</label>
                                <label htmlFor={"image"}>
                                    <Image data={data} dataHandle={dataHandle} name="file_sk_pelimpahan" required={data['file_sk_pelimpahan'] ? false : true}/>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>)
      }

      const View2 = () => {
        if (keys !== "pandu_1") {
          return null
        }
        return (<>
          <div className="card mb-5">
            <div style={{ padding: '30px' }}>
              <h3>  SDM PANDU </h3>
              <div className="row">
                <div className="col-md-12 pt-8">
                  <div className="table-responsive" >
                    <table className="table table-hover w-100">
                      <thead>
                        <tr className="text-center" style={{ background: '#F4F5F7' }}>
                          <td>No</td>
                          <td>Nama</td>
                          <td>Tingkat (Class)</td>
                          <td>Nomor Sertifikat (Certificate Number)</td>
                          <td>Nomor Endorsement (Endorsement Number)</td>
                          <td>Masa Berlaku (Expiry)</td>
                          <td>Keterangan</td>
                        </tr>
                      </thead>
                      <tbody>
                        {personil.length > 0 && personil.map((c, d) => {
                          return (
                            <tr key={d} className="text-center">
                              <td>{(d + 1)}</td>
                              <td>{c.nama}</td>
                              <td>{c.kelas !== null ? "CLASS "+parseInt(c.kelas) : ""}</td>
                              <td>{c.no_sertifikat}</td>
                              <td>{c.no_sertifikat}</td>
                              <td>{c.tanggal_selesai ? moment(c.tanggal_expire).format('DD MMMM YYYY') : ""}</td>
                              <td>{c.tipe_personil}</td>
                            </tr>
                          )
                        })}
                        {personil.length === 0 &&
                          <tr className="text-center">
                            <td colSpan="6">No Data Found</td>
                          </tr>
                        }
    
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
        )
      }
    
      const View3 = () => {
        if (keys !== "pandu_5") {
          return null
        }
        return (<>
          <div className="card mb-5">
            <div style={{ padding: '30px' }}>
              <h3>  OPERATOR RADIO PEMANDUAN </h3>
              <div className="row">
                <div className="col-md-12 pt-8">
                  <div className="table-responsive" >
                    <table className="table table-hover w-100">
                      <thead>
                        <tr className="text-center" style={{ background: '#F4F5F7' }}>
                          <td>No</td>
                          <td>Nama</td>
                          <td>Jenis Sertifikat</td>
                          <td>Nomor Sertifikat (Certificate Number)</td>
                          <td>Tempat Tahun di Penerbitan (Date Issued)</td>
                          <td>Masa Berlaku (Expiry)</td>
                          <td>Keterangan</td>
                        </tr>
                      </thead>
                      <tbody>
                        {personil.length > 0 && personil.map((c, d) => {
                            console.log(c)
                          return (
                            <tr key={d} className="text-center">
                              <td>{(d + 1)}</td>
                              <td>{c.nama}</td>
                              <td>{c.nama}</td>
                              <td>{c.kelas !== null ? "CLASS "+parseInt(c.kelas) : ""}</td>
                              <td>{c.no_sertifikat}</td>
                              <td>{moment(c.tanggal_selesai).format('DD MMMM YYYY')}</td>
                              <td>{c.tipe_personil}</td>
                            </tr>
                          )
                        })}
                        {personil.length === 0 &&
                          <tr className="text-center">
                            <td colSpan="6">No Data Found</td>
                          </tr>
                        }
    
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
        )
      }

      const View4 = () => {
        if (keys !== "kapal_1") {
          return null
        }
        return (<>
          <div className="card mb-5">
            <div style={{ padding: '30px' }}>
              <h3>  KAPAL TUNDA (ASSIST TUG) </h3>
              <div className="row">
                <div className="col-md-12 pt-8">
                  <div className="table-responsive" >
                    <table className="table table-hover w-100">
                      <thead>
                        <tr className="text-center" style={{ background: '#F4F5F7' }}>
                          <td>No</td>
                          <td>Nama Kapal</td>
                          <td>Daya Kuda (Horse Power)</td>
                          <td>Tahun Pembuatan (Year Build)</td>
                          <td>Kebangsaan (Nationality)</td>
                          <td>Jenis/Type</td>
                          <td>Tenaga Penggerak (Propullation)</td>
                          <td>Bollar Pull</td>
                          <td>Keterangan</td>
                        </tr>
                      </thead>
                      <tbody>
                        {kapals.length > 0 && kapals.map((c, d) => {
    
                          return (
                            <tr key={d} className="text-center">
                              <td>{(d + 1)}</td>
                              <td>{c.nama_asset}</td>
                              <td>{c.horse_power}</td>
                              <td>{c.tahun_pembuatan}</td>
                              <td>{c.negara_pembuat}</td>
                              <td>{c.tipe}</td>
                              <td>{c.daya_motor}</td>
                              <td>{c.bolard_pull}</td>
                              <td>{c.kepemilikan_kapal !== null ? c.kepemilikan_kapal === 3 ? "Milik": "Sewa" : ""}</td>
                            </tr>
                          )
                        })}
                        {kapals.length === 0 &&
                          <tr className="text-center">
                            <td colSpan="9">No Data Found</td>
                          </tr>
                        }
    
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
        )
      }

      const View5 = () => {
        if (keys !== "kapal_2") {
          return null
        }
        return (<>
          <div className="card mb-5">
            <div style={{ padding: '30px' }}>
              <h3>  KAPAL PANDU (PILOT BOAT) </h3>
              <div className="row">
                <div className="col-md-12 pt-8">
                  <div className="table-responsive" >
                    <table className="table table-hover w-100">
                      <thead>
                        <tr className="text-center" style={{ background: '#F4F5F7' }}>
                          <td>No</td>
                          <td>Nama Kapal</td>
                          <td>Daya Kuda (Horse Power)</td>
                          <td>Tahun Pembuatan (Year Build)</td>
                          <td>Kebangsaan (Nationality)</td>
                          <td>Jenis/Type</td>
                          <td>Kecepatan (Speed)</td>
                          <td>Bollar Pull</td>
                          <td>Keterangan</td>
                        </tr>
                      </thead>
                      <tbody>
                        {kapals.length > 0 && kapals.map((c, d) => {
    
                          return (
                            <tr key={d} className="text-center">
                              <td>{(d + 1)}</td>
                              <td>{c.nama_asset}</td>
                              <td>{c.horse_power}</td>
                              <td>{c.tahun_pembuatan}</td>
                              <td>{c.negara_pembuat}</td>
                              <td>{c.tipe}</td>
                              <td>{c.daya_motor}</td>
                              <td>{c.bolard_pull}</td>
                              <td>{c.kepemilikan_kapal !== null ? c.kepemilikan_kapal === 3 ? "Milik": "Sewa" : ""}</td>
                            </tr>
                          )
                        })}
                        {kapals.length === 0 &&
                          <tr className="text-center">
                            <td colSpan="9">No Data Found</td>
                          </tr>
                        }
    
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
        )
      }

      const View6 = () => {
        if (keys !== "kapal_3") {
          return null
        }
        return (<>
          <div className="card mb-5">
            <div style={{ padding: '30px' }}>
              <h3>  KAPAL KEPIL (MOORING BOAT) </h3>
              <div className="row">
                <div className="col-md-12 pt-8">
                  <div className="table-responsive" >
                    <table className="table table-hover w-100">
                      <thead>
                        <tr className="text-center" style={{ background: '#F4F5F7' }}>
                          <td>No</td>
                          <td>Nama Kapal</td>
                          <td>Daya Kuda (Horse Power)</td>
                          <td>Tahun Pembuatan (Year Build)</td>
                          <td>Kebangsaan (Nationality)</td>
                          <td>Jenis/Type</td>
                          <td>Kecepatan (Speed)</td>
                          <td>Bollar Pull</td>
                          <td>Keterangan</td>
                        </tr>
                      </thead>
                      <tbody>
                        {kapals.length > 0 && kapals.map((c, d) => {
    
                          return (
                            <tr key={d} className="text-center">
                              <td>{(d + 1)}</td>
                              <td>{c.nama_asset}</td>
                              <td>{c.horse_power}</td>
                              <td>{c.tahun_pembuatan}</td>
                              <td>{c.negara_pembuat}</td>
                              <td>{c.tipe}</td>
                              <td>{c.daya_motor}</td>
                              <td>{c.bolard_pull}</td>
                              <td>{c.kepemilikan_kapal !== null ? c.kepemilikan_kapal === 3 ? "Milik": "Sewa" : ""}</td>
                            </tr>
                          )
                        })}
                        {kapals.length === 0 &&
                          <tr className="text-center">
                            <td colSpan="9">No Data Found</td>
                          </tr>
                        }
    
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
        )
      }
    

    return (<>
        {ACTION !== "detail" &&
        <form onSubmit={submitHandle}>
            <div key="EP1" className="card mb-5">
                <div style={{ padding: '30px' }}>
                    <h5>Evaluasi Pelimpahan Pelaksanaan Pandu Tunda</h5>
                    <br />
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="cabang_id" className="d-block">Cabang <span className="text-red">*</span></label>
                                {ACTION !== "detail" &&
                                    <select name="cabang_id" id="cabang_id" className='form-control' value={data.cabang_id || isAuthorized.cabang_id} onChange={dataHandle} required>
                                        <option value="">Pilih Cabang</option>

                                        {cabang.map((a, b) => {
                                            return (
                                                <option key={b} value={a.id}> {a.nama} </option>
                                            )
                                        })}
                                    </select>}

                                {/* {ACTION === "detail" &&
                                    <input plaintext="true" readOnly name="cabang_id" className='form-control-plaintext' value={data.cabang} />
                                } */}
                            </div>
                        </div>
                        <div className="col-md-6">

                            <div className="form-group">
                                <label htmlFor="text" className="d-block">BUP/Terminal Khusus <span className="text-red">*</span></label>
                                {ACTION !== "detail" &&
                                    <input type="text" value={data.bup || ""} onChange={dataHandle} name="bup" className="form-control" placeholder="Tulis BUP/Terminal Khusus" readOnly={true} />
                                }
                                {/* {ACTION === "detail" &&
                                    <input plaintext="true" readOnly name="bup" className='form-control-plaintext' value={data.bup} />
                                } */}
                            </div>

                        </div>
                        <div className="col-md-6">

                            <div className="form-group">
                                <label htmlFor="text" className="d-block">Izin BUP/ Pengoperasian Tersus <span className="text-red">*</span></label>
                                {ACTION !== "detail" &&
                                    <input type="text" value={data.izin_bup || ""} onChange={dataHandle} name="izin_bup" className="form-control" placeholder="Tulis Izin BUP/ Pengoperasian Tersus" required />
                                }
                                {/* {ACTION === "detail" &&
                                    <input plaintext="true" readOnly name="izin_bup" className='form-control-plaintext' value={data.izin_bup} />
                                } */}
                            </div>

                        </div>
                        <div className="col-md-6">

                            <div className="form-group">
                                <label htmlFor="text" className="d-block">Penetapan Perairan Pandu <span className="text-red">*</span></label>
                                {ACTION !== "detail" &&
                                    <input type="text" value={data.penetapan_perairan_pandu || ""} onChange={dataHandle} name="penetapan_perairan_pandu" className="form-control" placeholder="Tulis Penetapan Perairan Pandu" required />
                                }
                                {/* {ACTION === "detail" &&
                                    <input plaintext="true" readOnly name="penetapan_perairan_pandu" className='form-control-plaintext' value={data.penetapan_perairan_pandu} />
                                } */}
                            </div>

                        </div>
                        <div className="col-md-6">

                            <div className="form-group">
                                <label htmlFor="text" className="d-block">Izin Pelimpahan dan Pemanduan <span className="text-red">*</span></label>
                                {ACTION !== "detail" &&
                                    <input type="text" value={data.izin_pelimpahan || ""} onChange={dataHandle} name="izin_pelimpahan" className="form-control" placeholder="Tulis Izin Pelimpahan dan Pemanduan" required />
                                }
                                {/* {ACTION === "detail" &&
                                    <input plaintext="true" readOnly name="izin_pelimpahan" className='form-control-plaintext' value={data.izin_pelimpahan} />
                                } */}
                            </div>

                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="text" className="d-block">Pengawas Pemanduan Setempat <span className="text-red">*</span></label>
                                {ACTION !== "detail" &&
                                    <input type="text" value={data.pengawas_pemanduan || ""} onChange={dataHandle} name="pengawas_pemanduan" className="form-control" placeholder="Tulis Pengawas Pemanduan Setempat" required />
                                }
                                {/* {ACTION === "detail" &&
                                    <input plaintext="true" readOnly name="pengawas_pemanduan" className='form-control-plaintext' value={data.pengawas_pemanduan} />
                                } */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div key="EP2" className="card mb-5">
                <div style={{ padding: '30px' }}>
                    {ACTION !== "detail" && <>
                        <h5 className="mb-10">Administrasi dan Data Dukung</h5>
                        <br />
                        <div className="row">
                            
                            <div className="col-md-4">

                                <h6>Laporan Bulanan (Setiap 1 Bulan)</h6>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" value="1" required name="check_laporan_bulanan" id="laporan_bulanan1" onChange={radioHandle} checked={data.check_laporan_bulanan == 1 ? true : false} />
                                    <label htmlFor="laporan_bulanan1" className="ml-2">
                                        Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" value="0" required name="check_laporan_bulanan" id="laporan_bulanan2" onChange={radioHandle} checked={data.check_laporan_bulanan == 0 ? true : false} />
                                    <label htmlFor="laporan_bulanan2" className="ml-2">
                                        Tidak Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <input type="text" name="laporan_bulanan" onChange={dataHandle} value={data.laporan_bulanan || ""} className="form-control" placeholder="Tulis Keterangan" required={data.check_laporan_bulanan == 1 ? true : false} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4">
                                <h6>Bukti Pembayaran PNBP</h6>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" required value="1" name="check_bukti_pembayaran_pnpb" onChange={radioHandle} id="bukti_pembayaran_pnpb1" checked={data.check_bukti_pembayaran_pnpb === 1 ? true : false} />
                                    <label htmlFor="bukti_pembayaran_pnpb1" className="ml-2">
                                        Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" required value="0" name="check_bukti_pembayaran_pnpb" onChange={radioHandle} id="bukti_pembayaran_pnpb2" checked={data.check_bukti_pembayaran_pnpb === 0 ? true : false} />
                                    <label htmlFor="bukti_pembayaran_pnpb2" className="ml-2">
                                        Tidak Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <input type="text" value={data.bukti_pembayaran_pnpb || ""} onChange={dataHandle} name="bukti_pembayaran_pnpb" className="form-control" placeholder="Tulis Keterangan" required={data.check_bukti_pembayaran_pnpb == 1 ? true : false} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4">
                                <h6>SISPRO/SOP</h6>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" required value="1" name="check_sispro" onChange={radioHandle} id="sispro1" checked={data.check_sispro === 1 ? true : false} />
                                    <label htmlFor="sispro1" className="ml-2">
                                        Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" required value="0" name="check_sispro" onChange={radioHandle} id="sispro2" checked={data.check_sispro === 0 ? true : false} />
                                    <label htmlFor="sispro2" className="ml-2">
                                        Tidak Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <input type="text" value={data.sispro || ""} onChange={dataHandle} name="sispro" className="form-control" placeholder="Tulis Keterangan" required={data.check_sispro == 1 ? true : false} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4">
                                <h6>Tarif Jasa Pandu dan Tunda</h6>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" required value="1" name="check_tarif_jasa_pandu_tunda" onChange={radioHandle} id="tarif_jasa_pandu_tunda1" checked={data.check_tarif_jasa_pandu_tunda === 1 ? true : false} />
                                    <label htmlFor="tarif_jasa_pandu_tunda1" className="ml-2">
                                        Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" required value="0" name="check_tarif_jasa_pandu_tunda" onChange={radioHandle} id="tarif_jasa_pandu_tunda2" checked={data.check_tarif_jasa_pandu_tunda === 0 ? true : false} />
                                    <label htmlFor="tarif_jasa_pandu_tunda2" className="ml-2">
                                        Tidak Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <input type="text" onChange={dataHandle} value={data.tarif_jasa_pandu_tunda || ""} name="tarif_jasa_pandu_tunda" className="form-control" placeholder="Tulis Keterangan" required={data.check_tarif_jasa_pandu_tunda == 1 ? true : false} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4">
                                <h6>Data Dukung Lainnya</h6>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" required value="1" name="check_data_dukung" onChange={radioHandle} id="data_dukung1" checked={data.check_data_dukung === 1 ? true : false} />
                                    <label htmlFor="data_dukung1" className="ml-2">
                                        Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div>
                                    <input type="radio" required value="0" name="check_data_dukung" onChange={radioHandle} id="data_dukung2" checked={data.check_data_dukung === 0 ? true : false} />
                                    <label htmlFor="data_dukung2" className="ml-2">
                                        Tidak Ada
                                </label>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    <input type="text" onChange={dataHandle} defaultValue={data.data_dukung || ""} name="data_dukung" className="form-control" placeholder="Tulis Keterangan" required={data.check_data_dukung == 1 ? true : false} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <h6>Unggah File Pendukung</h6>
                            </div>
                            <div className="upload-wrapper d-block" style={{position: "relative",overflow: "hidden",display: "inline-block",width: "245px",height: "150px"}}>
                                <label htmlFor={"file_pendukung"}>
                                    <Image data={data} dataHandle={dataHandle} name="file_pendukung" required={data['file_pendukung'] ? false : true}/>
                                </label>
                                
                                {<span className="text-danger d-block mt-2">{errMessage.file_pendukung}</span>}
                            </div>
                        </div>
                    </>}
                </div>
            </div>
            <div key="EP3" className="card mb-5">
                <div style={{ padding: '30px' }}>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="text" className="d-block">Tanggal SK <span className="text-red">*</span></label>
                                {ACTION !== "detail" &&
                                    <InputDate 
                                        name="tanggal_sk" 
                                        dataHandle={dataHandle} 
                                        value={data.tanggal_sk} 
                                        required={true}
                                    />
                                }
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="d-block">Unggah SK Pelimpahan <span className="text-red">*</span></label>
                                <Image data={data} dataHandle={dataHandle} name="file_sk_pelimpahan" required={data['file_sk_pelimpahan'] ? false : true}/>
                                {<span className="text-danger d-block mt-2">{errMessage.file_sk_pelimpahan}</span>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" className="btn btn-primary float-right mt-8"> Submit </button>
        </form>
        }
        {ACTION === "detail" &&
                <div className="row">
                <div className="col-md-12">
                    <Tabs
                        id={"controlled-tab-example"}
                        activeKey={keys}
                        onSelect={(k) => setKey(k)}
                        onClick={handleChange()}
                        >
                        <Tab eventKey="1_Umum" title="EVALUASI PELIMPAHAN">
                            <View1
                            />
                        </Tab>
                        <Tab eventKey="pandu_1" title="SDM PANDU">
                            <View2
                            // handleChange={handleChange}
                            />
                        </Tab>
                        <Tab eventKey="pandu_5" title="OPERATOR RADIO">
                            <View3
                            // handleChange={handleChange}
                            />
                        </Tab>
                        <Tab eventKey="kapal_1" title="KAPAL TUNDA" >
                            <View4
                            onClick={handleChange}
                            />
                        </Tab>
                        <Tab eventKey="kapal_2" title="MOTOR PANDU">
                            <View5
                            // handleChange={handleChange}
                            />
                        </Tab>
                        <Tab eventKey="kapal_3" title="KAPAL KEPIL">
                            <View6
                            // handleChange={handleChange}
                            />
                        </Tab>
                    </Tabs>
                </div>
                </div>
            }</>
    )
}