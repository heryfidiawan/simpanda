import React, { useState, useEffect } from "react";
import { Tab, Tabs } from "react-bootstrap";
import { useSubheader } from "../../../_metronic/layout";
import { APIS, Engine } from "../../../simpanda/config/Engine";
import { shallowEqual, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { checkbox } from "../../../simpanda/config/checkbox";
import moment from 'moment';
import { Image } from "../../../simpanda/components/Image"
import { InputDate } from "../../../simpanda/components/InputDate"

const defaultTableTemp = {
  'nama': '',
  'jabatan': '',
  'tgl': moment().format('YYYY-MM-DD'),
  'status': '',
  'investigasi_insiden_id': ''
}

let checks = [
  {"bukti_pendukung": ""}, {"wujud_cedera": ""}, {"bagian_tubuh_cedera": ""}, {"mekanisme_cedera": ""}, {"peralatan_kelengkapan": ""}, {"alat_pelindung_diri": ""}, {"perilaku": ""}, {"kebersihan_kerapihan": ""}, {"peralatan_perlengkapan": ""}, {"kemampuan_kondisi_fisik": ""}, {"pemeliharaan_perbaikan": ""}, {"design": ""}, {"tingkat_kemampuan": ""}, {"penjagaan": ""}, {"tindakan_terkait": ""},
];
let peralatan_kelengkapan = []
let alat_pelindung_diri = []
let perilaku = []
let kebersihan_kerapihan = []
let peralatan_perlengkapan = []
let kemampuan_kondisi_fisik = []
let pemeliharaan_perbaikan = []
let tingkat_kemampuan = []
let design = []
let penjagaan = []
let tindakan_terkait = []
let wujud_cedera = []
let tubuh_cedera = []
let mekanisme_cedera = []
let bukti_pendukung = [];

export function InvestigasiInsiden(props) {

  const { isAuthorized } = useSelector(({ auth }) => ({ isAuthorized: auth.user }), shallowEqual)

  const PARENT = props.match.url.split("/")[1];
  const MENU = props.match.url.split("/")[2];
  const ACTION = props.match.params.action;
  const KEY = props.match.params.key;
  const BACKEND = MENU.replace("-", "");
  const PARENTURL = `${PARENT}/${MENU}`;
  defaultTableTemp['investigasi_insiden_id'] = KEY

  const history = useHistory();

  const [data, setData] = useState([]);
  const [dataTable, setDataTable] = useState([]);
  const [currentSteps, setCurrentStep] = useState(1);
  const [keys, setKey] = useState("Umum");
  const [tableTemp, setTableTemp] = useState(defaultTableTemp);
  const [filter, setFilter] = useState([]);
  const [cabang, setCabang] = useState([]);
  const [lainnya, setLainnya] = useState(true);
  const [displays, setDisplays] = useState("");
  const [count, setCount] = useState({
    kronologi_kejadian : 0,
    temuan_investigasi: 0,
    kerusakan_alat: 0,
    uraian_kejadian: 0,
    analisis_penyebab: 0
  });

	const [errMessage, setErrMessage] = useState(false)

  // console.log('data :>> ', data);
  
  const getCabang = async () => {
    await Engine.GET('cabang', isAuthorized.accessToken)
      .then((data) => {
        setCabang(data)

      })
      .catch((e) => {
        console.log(e);
      });
  };

  const dataHandler = async (e) => {
    const target = e.target
    const { name, value } = e.target
    let valueData = e.target.value
    let valueTemp = e.target.value
    setCount({
      ...count,
      [e.target.name] : e.target.value.length
    })
    if (e.target.type === "checkbox") {

      if (e.target.checked) {
        var obj = {}
        obj[e.target.name] = e.target.value + ","

        var f = 0;
        for (var a in checks) {
          var b = checks[a]

          for (var c in b) {
            if (e.target.name === c) {
              b[c] = b[c] + e.target.value + ",";
              f = 1;
            }
          }
        }

        if (f === 0)
          checks.push(obj)
        
      } else {
        for (var a in checks) {
          var b = checks[a];
          for (var c in b) {
            if (e.target.name === c) {
              var g = b[c];
              b[c] = g.replace(e.target.value + ",", "");
            }
          }
        }
      }
      setData(prev => ({ ...prev, checks }))

    } else if (e.target.type === "radio") {
      if (e.target.id != "Lainnya") {
        setLainnya(true)
      } else {
        // setData({rincian_kegiatan : ""})
        setLainnya(false);
      }
      setData(prev => ({ ...prev, [name]: value }))
    } else if (target.type === "file") {
      
      if (target.files[0]) {
        valueData = await convertBase64(target.files[0])
        valueTemp = URL.createObjectURL(target.files[0])
        if (target.files[0].size <= 15000000) {
          if (target.files[0].type.includes('pdf')) {
          
            setDisplays('/media/simpanda/pdf-thumb.png')
          }else{
            setDisplays('"../../../_metronic/_assets/images/upload.png"')
          }
					setData(prev => ({ ...prev, [name]: valueData }))
					setErrMessage('') 
				}else{
          valueData = ""
					setData(prev => ({ ...prev, [name]: valueData }))
					setErrMessage('Ukuran file tidak boleh melebihi 15 MB')
					
				}
        
        
        
        // setData(prev => ({ ...prev, [name]: valueData }))
      }
    } else {
      setData(prev => ({ ...prev, [name]: value }))
    }


  };

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader()
      fileReader.readAsDataURL(file)
      // fileReader.readAsText(file)
      fileReader.onload = () => {
        resolve(fileReader.result)
      }
      fileReader.onerror = (error) => {
        reject(error)
      }
    })
  }

  const sesuaikan_data = (data) => {
    var arr = ["Inspeksi di tempat kejadian", "Wawancara", "Pengambilan Foto/Video", "Sketsa Kejadian"];
    var lain = 1;
    for (var a in arr) {
      if (data.rincian_kegiatan === arr[a]) {
        lain = 0;
      }
    }

    if (lain === 1) {
      setLainnya(false);
    }
    // console.log("RESLUTdataaa", data);
  }

  useEffect(() => {
    getCabang()
    if (KEY !== undefined) {

      Engine.GET(`${BACKEND}/${KEY}`, isAuthorized.accessToken)
        .then((data) => {
          setData(data);
          sesuaikan_data(data);
        })
        .catch((e) => {
          console.log(e);
        });

      Engine.GET(`investigasiinsidentim?investigasi_insiden_id=${KEY}`, isAuthorized.accessToken)
        .then((datas) => {
          setDataTable(datas);
          console.log("irfan",datas)

        })
        .catch((e) => {
          console.log(e);
        });
    }
    // setCurrentStep(8);

  }, []);

  const _next = () => {
    // console.log(data)
    var arr = []
    var i = 0
    var form = document.getElementById("formInvestigasi");
    var a = form.getElementsByTagName("input")
    var b = form.getElementsByTagName("textarea")
    var c = form.getElementsByTagName("radio")
    for (i = 0; i < a.length; i++) {
      // deal with inputs[index] element.
      if (parseInt(a[i].dataset.step) === parseInt(currentSteps))
      {
        if (a[i].type === "text") {
          if (a[i].value === "") {
            console.log(a[i])
            arr.push(i)
          }
        }
      }
    }

    for (i = 0; i < b.length; i++) {
      // deal with inputs[index] element.
      if (parseInt(b[i].dataset.step) === parseInt(currentSteps))
      {
        if (b[i].value === "") {
          console.log(b[i])
          arr.push(i)
        }
      }
    }
    // console.log(arr.length)
    if (arr.length > 0) {
      Swal.fire({
        title: 'Gagal melanjutkan',
        text: 'Silahkan mengisi field yang dibutuhkan',
        type: 'warning',
    })
    }else{
      let currentStep = currentSteps
      currentStep = currentStep >= 8 ? currentStep : currentStep + 1
      setCurrentStep(currentStep)
    // console.log("datanya", data)
    }
    
  }

  const _prev = () => {
    let currentStep = currentSteps
    currentStep = currentStep <= 1 ? 1 : currentStep - 1
    setCurrentStep(currentStep)
    console.log("balik lagi", data)
  }

  const PreviousButton = () => {
    let currentStep = currentSteps;
    if (currentStep !== 1) {
      return (
        <button
          className="btn btn-secondary mr-5"
          type="button" onClick={_prev}>
          Sebelumnya
        </button>
      )
    }
    return null;
  }


  const NextButton = () => {
    let currentStep = currentSteps;
    if (currentStep < 8) {
      return (
        <button
          className="btn btn-primary"
          type="button" onClick={_next}>
          Next
        </button>
      )
    } else {
      return (
        <button
          className="btn btn-primary"
          type="button" onClick={submitHandle}>
          Save
        </button>
      );
    }
  }

  const tableHandle = (e) => {
    const { name, value } = e.target
    setTableTemp(prev => ({ ...prev, [name]: value }))
  }

  const removeTable = (index) => {
    const newTable = [...dataTable]
    newTable.splice(index, 1)
    setDataTable(newTable)

  }

  const addTable = () => {
  	console.log(tableTemp)
    setDataTable([...dataTable, tableTemp])
    dataTable.push(tableTemp)

    Array.from(document.getElementsByClassName('table-view')).forEach(
      (input) => (input.value = '')
    )

  }

  const submitHandle = async () => {
	
	const prepard_by = data['prepard_by'] ? data['prepard_by'].toLowerCase() : ""; 
	const reviewed_by = data['reviewed_by'] ? data['reviewed_by'].toLowerCase() : "";
	const approved_by = data['approved_by'] ? data['approved_by'].toLowerCase() : "";
    if(prepard_by===reviewed_by || prepard_by===approved_by || reviewed_by===approved_by){  
      Swal.fire({
        title: 'Gagal melanjutkan',
        text: 'Disusun oleh, Diperiksa oleh dan Disetujui oleh tidak boleh sama',
        type: 'warning',
      });
      return false;
    }else{
      
    }


    data['investigasi_insiden_tim'] = dataTable
    data['approval_status_id'] = 0
    data['tanggal_pemeriksaan'] = data['tanggal_pemeriksaan'] || moment().format('YYYY-MM-DD');
    data['approved_tanggal'] = data['approved_tanggal'] || moment().format('YYYY-MM-DD');
    data['prepard_tanggal'] = data['prepard_tanggal'] || moment().format('YYYY-MM-DD');
    data['reviewed_tanggal'] = data['reviewed_tanggal'] || moment().format('YYYY-MM-DD');
    data['cabang_id'] = data['cabang_id'] || isAuthorized.cabang_id;
    var checks = data.checks;
    for (var a in checks) {
      var arr = checks[a];
      for (var b in arr) {
        var input = arr[b];
        data[b] = input;
      }
    }
    delete data.checks;
    console.log("datanya", data)
    const method = ACTION === 'create' ? 'POST' : 'PUT'
    const URL = ACTION === 'create' ? 'investigasiinsiden' : 'investigasiinsiden' + '/' + KEY
    await Engine.POST(URL, method, data, isAuthorized.accessToken)
      .then(response => {
        // console.log('FORM SUBMIT',data)
        if (response) {
          history.push(`/${PARENTURL}`)
        }
      })

  }

  const View1 = () => {
    if (keys !== "Umum") {
      return null
    }
    return (
      <div className="card">
        <div style={{ padding: '30px' }}>
          <h3> Detail Umum </h3>
          <div className="row pt-10">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">No Report</label>
                <input plaintext="true" readOnly name="no_report" className='form-control-plaintext' value={data.no_report ? data.no_report : "-"} />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Cabang</label>
                <input plaintext="true" readOnly name="cabang_id" className='form-control-plaintext' value={data.cabang} />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Unit Terkait</label>
                <input plaintext="true" readOnly name="unit_terkait" className='form-control-plaintext' value={data.unit_terkait ? data.unit_terkait : "-"} />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Judul Report</label>
                <input type="text" name="judul_report" plaintext="true" readOnly required className="form-control-plaintext" value={data.judul_report ? data.judul_report : "-"} />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Kronologi Kejadian</label>
                <input type="text" name="kronologi_kejadian" plaintext="true" readOnly required className="form-control-plaintext" value={data.kronologi_kejadian ? data.kronologi_kejadian : "-"} />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Temuan Investigasi</label>
                <input type="text" name="temuan_investigasi" plaintext="true" readOnly required className="form-control-plaintext" value={data.temuan_investigasi ? data.temuan_investigasi : "-"} />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label className="d-block">Unggah Bukti Temuan</label>
                <label htmlFor={"bukti_temuan"}>
                  
                  <div className={`upload-wrapper d-block`}>
                      {ACTION !== 'detail' &&
                          <input
                              type="file"
                              name="bukti_temuan" 
                              onChange={dataHandler}
                              required={data['bukti_temuan'] ? false : true}
                          /> 
                      }
                      <Image data={data} name="bukti_temuan"/>
                  </div>

                </label>
              </div>
            </div>

          </div>
          <div className="pb-5 mb-5">Dilaporkan oleh </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Saksi 1</label>
                <input type="text" name="saksi_1" plaintext="true" readOnly required className="form-control-plaintext" value={data.saksi_1 ? data.saksi_1 : "-"} />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Saksi 2</label>
                <input type="text" name="saksi_2" plaintext="true" readOnly required className="form-control-plaintext" value={data.saksi_2 ? data.saksi_2 : "-"} />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Investigator</label>
                <input type="text" name="investigator" plaintext="true" readOnly required className="form-control-plaintext" value={data.investigator ? data.investigator : "-"} />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 mb-5">
              <label htmlFor="text" className="d-block">Rincian kegiatan investigasi yang dilakukan</label>
              <div className="form-group">
                <input type="text" name="rincian_kegiatan" plaintext="true" readOnly required className="form-control-plaintext" value={data.rincian_kegiatan ? data.rincian_kegiatan : "-"} />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Bukti pendukung yang dilampirkan</label>
                {data.bukti_pendukung ?
                  (
                    data.bukti_pendukung.split(",").map((c, d) => {

                      checkbox.bukti_pendukung.menus.filter(t => t.vals === c).map((a, b) => {
                        if (bukti_pendukung.indexOf(a.detail) === -1) {
                          bukti_pendukung.push(a.detail)
                        }
                      })
                    })
                  ) : (bukti_pendukung = [])
                }
                {bukti_pendukung.length > 0 ? (bukti_pendukung.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const View2 = () => {
    if (keys !== "Klasifikasi") {
      return null
    }
    return (
      <div className="card">
        <div style={{ padding: '30px' }}>
          <h3> Klasifikasi Insiden </h3>
          <div className="row pt-10">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Luka Sakit</label>
                <input plaintext="true" readOnly name="luka_sakit" className='form-control-plaintext' value={data.luka_sakit ? data.luka_sakit : "-"} />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Wujud Cidera</label>
                {/* <input plaintext="true" readOnly name="wujud_cedera" className='form-control-plaintext' value={data.wujud_cedera ? data.wujud_cedera.split(",").join(", ") : "-"} /> */}
                {data.wujud_cedera !== null ?
                  (
                    data.wujud_cedera.split(",").map((c, d) => {

                      checkbox.wujud_cedera.menus.filter(t => t.vals === c).map((a, b) => {
                        if (wujud_cedera.indexOf(a.detail) === -1) {
                          wujud_cedera.push(a.detail)
                        }
                      })
                    })
                  ) : (wujud_cedera = [])
                }
                {wujud_cedera.length > 0 ? (wujud_cedera.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Bagian Tubuh yang Cidera</label>
                {/* <input plaintext="true" readOnly name="bagian_tubuh_cedera" className='form-control-plaintext' value={data.bagian_tubuh_cedera ? data.bagian_tubuh_cedera.split(",").join(", ") : "-"} /> */}
                {data.bagian_tubuh_cedera !== null ?
                  (
                    data.bagian_tubuh_cedera.split(",").map((c, d) => {

                      checkbox.tubuh_cedera.menus.filter(t => t.vals === c).map((a, b) => {
                        if (tubuh_cedera.indexOf(a.detail) === -1) {
                          tubuh_cedera.push(a.detail)
                        }
                      })
                    })
                  ) : (tubuh_cedera = [])
                }
                {tubuh_cedera.length > 0 ? (tubuh_cedera.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Mekanisme Cidera</label>
                {/* <input plaintext="true" readOnly name="mekanisme_cedera" className='form-control-plaintext' value={data.mekanisme_cedera ? data.mekanisme_cedera.split(",").join(", ") : "-"} /> */}
                {data.mekanisme_cedera !== null ?
                  (
                    data.mekanisme_cedera.split(",").map((c, d) => {

                      checkbox.mekanisme_cedera.menus.filter(t => t.vals === c).map((a, b) => {
                        if (mekanisme_cedera.indexOf(a.detail) === -1) {
                          mekanisme_cedera.push(a.detail)
                        }
                      })
                    })
                  ) : (mekanisme_cedera = [])
                }
                {mekanisme_cedera.length > 0 ? (mekanisme_cedera.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Kerusakan Alat</label>
                <input plaintext="true" readOnly name="kerusakan_alat" className='form-control-plaintext' value={data.kerusakan_alat ? data.kerusakan_alat.split(",").join(", ") : "-"} />
              </div>
            </div>
          </div>
          <h3> Nearmiss </h3>
          <div className="row pt-10">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Uraian Kejadian</label>
                <input plaintext="true" readOnly name="uraian_kejadian" className='form-control-plaintext' value={data.uraian_kejadian ? data.uraian_kejadian : "-"} />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Analisa Penyebab Kejadian</label>
                <input plaintext="true" readOnly name="analisa_penyebab" className='form-control-plaintext' value={data.analisa_penyebab ? data.analisa_penyebab : "-"} />
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }

  const View3 = () => {
    if (keys !== "Analisis") {
      return null
    }
    return (
      <div className="card">
        <div style={{ padding: '30px' }}>
          <h3> Analisis Data Penyebab </h3>
          <br />
          <h5>Penyebab Langsung</h5>
          <div className="row pt-10">
            <div className="col-md-12">
              <div className="form-group">
                <label htmlFor="text" className="d-block">A. Peralatan dan Perlengkapan</label>
                {data.peralatan_kelengkapan !== null ?
                  (
                    data.peralatan_kelengkapan.split(",").map((c, d) => {

                      checkbox.peralatan_kelengkapan.menus.filter(t => t.vals === c).map((a, b) => {
                        if (peralatan_kelengkapan.indexOf(a.detail) === -1) {
                          peralatan_kelengkapan.push(a.detail)
                        }
                      })
                    })
                  ) : (peralatan_kelengkapan = [])
                }
                {peralatan_kelengkapan.length > 0 ? (peralatan_kelengkapan.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="peralatan_kelengkapan" className='form-control-plaintext' value={menu ? menu : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">B. Alat Pelindung Diri</label>
                {data.alat_pelindung_diri !== null ?
                  (
                    data.alat_pelindung_diri.split(",").map((c, d) => {
                      checkbox.alat_pelindung_diri.menus.filter(t => t.vals === c).map((a, b) => {
                        if (alat_pelindung_diri.indexOf(a.detail) === -1) {
                          alat_pelindung_diri.push(a.detail)
                        }
                      })
                    })
                  ) : (
                    alat_pelindung_diri = []
                  )

                }
                {alat_pelindung_diri.length > 0 ? (alat_pelindung_diri.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="wujud_cedera" className='form-control-plaintext' value={data.wujud_cedera ? data.wujud_cedera : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">C. Perilaku</label>
                {data.perilaku !== null ?
                  (
                    data.perilaku.split(",").map((c, d) => {
                      checkbox.perilaku.menus.filter(t => t.vals === c).map((a, b) => {
                        if (perilaku.indexOf(a.detail) === -1) {
                          perilaku.push(a.detail)
                        }
                      })
                    })
                  ) : (perilaku = [])

                }
                {perilaku.length > 0 ? (perilaku.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="bagian_tubuh_cedera" className='form-control-plaintext' value={data.bagian_tubuh_cedera ? data.bagian_tubuh_cedera : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">D. Kebersihan dan Kerapihan</label>
                {data.kebersihan_kerapihan !== null ?
                  (
                    data.kebersihan_kerapihan.split(",").map((c, d) => {
                      checkbox.kebersihan_kerapihan.menus.filter(t => t.vals === c).map((a, b) => {
                        if (kebersihan_kerapihan.indexOf(a.detail) === -1) {
                          kebersihan_kerapihan.push(a.detail)
                        }
                      })
                    })
                  ) : (kebersihan_kerapihan = [])

                }
                {kebersihan_kerapihan.length > 0 ? (kebersihan_kerapihan.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="mekanisme_cedera" className='form-control-plaintext' value={data.mekanisme_cedera ? data.mekanisme_cedera : "-"}/> */}
              </div>
            </div>
          </div>
          <div>
            <h5>Prosedur dan Kebijakan</h5>
          </div>
          <div className="row pt-10">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">A. Peralatan dan Perlengkapan</label>
                {data.peralatan_perlengkapan !== null ? (data.peralatan_perlengkapan.split(",").map((c, d) => {
                  checkbox.peralatan_perlengkapan.menus.filter(t => t.vals === c).map((a, b) => {
                    if (peralatan_perlengkapan.indexOf(a.detail) === -1) {
                      peralatan_perlengkapan.push(a.detail)
                    }
                  })
                })) : (peralatan_perlengkapan = [])

                }
                {peralatan_perlengkapan.length > 0 ? (peralatan_perlengkapan.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="kerusakan_alat" className='form-control-plaintext' value={data.kerusakan_alat ? data.kerusakan_alat : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">B. Kemampuan dan Kondisi Fisik</label>
                {data.kemampuan_kondisi_fisik !== null ? (data.kemampuan_kondisi_fisik.split(",").map((c, d) => {
                  checkbox.kemampuan_kondisi_fisik.menus.filter(t => t.vals === c).map((a, b) => {
                    if (kemampuan_kondisi_fisik.indexOf(a.detail) === -1) {
                      kemampuan_kondisi_fisik.push(a.detail)
                    }
                  })
                })) : (kemampuan_kondisi_fisik = [])

                }
                {kemampuan_kondisi_fisik.length > 0 ? (kemampuan_kondisi_fisik.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="uraian_kejadian" className='form-control-plaintext' value={data.uraian_kejadian ? data.uraian_kejadian : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">C. Pemeliharaan dan Perbaikan</label>
                {data.pemeliharaan_perbaikan !== null ? (data.pemeliharaan_perbaikan.split(",").map((c, d) => {
                  checkbox.pemeliharaan_perbaikan.menus.filter(t => t.vals === c).map((a, b) => {
                    if (pemeliharaan_perbaikan.indexOf(a.detail) === -1) {
                      pemeliharaan_perbaikan.push(a.detail)
                    }
                  })
                })) : (pemeliharaan_perbaikan = [])

                }
                {pemeliharaan_perbaikan.length > 0 ? (pemeliharaan_perbaikan.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="analisa_penyebab" className='form-control-plaintext' value={data.analisa_penyebab ? data.analisa_penyebab : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">D. Design</label>
                {data.design !== null ? (data.design.split(",").map((c, d) => {
                  checkbox.design.menus.filter(t => t.vals === c).map((a, b) => {
                    if (design.indexOf(a.detail) === -1) {
                      design.push(a.detail)
                    }
                  })
                })) : (design = [])

                }
                {design.length > 0 ? (design.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="uraian_kejadian" className='form-control-plaintext' value={data.uraian_kejadian ? data.uraian_kejadian : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">E. Tingkat Kemampuan</label>
                {data.tingkat_kemampuan !== null ? (data.tingkat_kemampuan.split(",").map((c, d) => {
                  checkbox.tingkat_kemampuan.menus.filter(t => t.vals === c).map((a, b) => {
                    if (tingkat_kemampuan.indexOf(a.detail) === -1) {
                      tingkat_kemampuan.push(a.detail)
                    }
                  })
                })) : (tingkat_kemampuan = [])

                }

                {tingkat_kemampuan.length > 0 ? (tingkat_kemampuan.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="uraian_kejadian" className='form-control-plaintext' value={data.uraian_kejadian ? data.uraian_kejadian : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">F. Penjagaan</label>
                {data.penjagaan !== null ? (data.penjagaan.split(",").map((c, d) => {
                  checkbox.penjagaan.menus.filter(t => t.vals === c).map((a, b) => {
                    if (penjagaan.indexOf(a.detail) === -1) {
                      penjagaan.push(a.detail)
                    }
                  })
                })) : (penjagaan = [])

                }
                {penjagaan.length > 0 ? (penjagaan.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="uraian_kejadian" className='form-control-plaintext' value={data.uraian_kejadian ? data.uraian_kejadian : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">G. Tindakan Terkait Lainnya</label>
                {data.tindakan_terkait !== null && data.tindakan_terkait !== undefined ?
                  (
                    data.tindakan_terkait.split(",").map((c, d) => {
                      checkbox.tindakan_terkait.menus.filter(t => t.vals === c).map((a, b) => {
                        if (tindakan_terkait.indexOf(a.detail) === -1) {
                          tindakan_terkait.push(a.detail)
                        }
                      })
                    })
                  ) : (tindakan_terkait = [])

                }
                {tindakan_terkait.length > 0 ? (tindakan_terkait.map((e, f) => {
                  return (
                    <li key={f}>{e}</li>
                  )
                })) : ("-")

                }
                {/* <input plaintext="true" readOnly name="uraian_kejadian" className='form-control-plaintext' value={data.uraian_kejadian ? data.uraian_kejadian : "-"}/> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="mb-5">Faktor utama yang mengakibatkan Insiden (dengan kode huruf dan angka)</div>
              <div className="form-group">
                <input type="text" plaintext="true" readOnly name="faktor_utama_insiden" className="form-control-plaintext" value={data.faktor_utama_insiden ? data.faktor_utama_insiden : "-"}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const View4 = () => {
    if (keys !== "Rekomendasi") {
      return null
    }
    return (
      <div className="card">
        <div style={{ padding: '30px' }}>
          <h3> Rekomendasi Tindakan Perbaikan </h3>
          <div className="row pt-10">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Rekomendasi Tindakan Selanjutnya</label>
                <input plaintext="true" readOnly name="rekomendasi_tindakan" className='form-control-plaintext' value={data.rekomendasi_tindakan ? data.rekomendasi_tindakan : "-"} />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Pihak yang Bertanggung Jawab</label>
                <input plaintext="true" readOnly name="pihak_yang_bertanggungjawab" className='form-control-plaintext' value={data.pihak_yang_bertanggungjawab ? data.pihak_yang_bertanggungjawab : "-"} />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Pelaksana</label>
                <input plaintext="true" readOnly name="pelaksana" className='form-control-plaintext' value={data.pelaksana ? data.pelaksana : "-"} />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="date" className="d-block">Tanggal Pemeriksaan</label>
                <input type="date" plaintext="true" readOnly value={moment(data.tanggal_pemeriksaan).format('YYYY-MM-DD')} className="form-control-plaintext" />
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }

  const View5 = () => {
    if (keys !== "Tim") {
      return null
    }
    return (<>
      <div className="card">
        <div style={{ padding: '30px' }}>
          <h3> Tim Investigasi Insiden </h3>
          <div className="row">
            <div className="col-md-12 pt-8">
              <div className="table-responsive" >
                <table className="table table-hover w-100">
                  <thead>
                    <tr className="text-center" style={{ background: '#F4F5F7' }}>
                      <td>No</td>
                      <td>Nama</td>
                      <td>Jabatan</td>
                      <td>Tanggal</td>
                      <td>Status</td>
                    </tr>
                  </thead>
                  <tbody>
                    {data.investigasi_insiden_tim.rows.length > 0 && data.investigasi_insiden_tim.rows.map((c, d) => {

                      return (
                        <tr key={d} className="text-center">
                          <td>{(d + 1)}</td>
                          <td>{c.nama}</td>
                          <td>{c.jabatan}</td>
                          <td>{moment(c.tgl).format('DD MMMM YYYY')}</td>
                          <td>{c.status}</td>
                        </tr>
                      )
                    })}
                    {data.investigasi_insiden_tim.rows.length === 0 &&
                      <tr className="text-center">
                        <td colSpan="5">No Data Found</td>
                      </tr>
                    }

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="card">
        <div style={{ padding: '30px' }}>
          <h6>Approval</h6>
          <br />
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Disusun oleh</label>
                <input plaintext="true" readOnly name="prepard_by" className='form-control-plaintext' value={data.prepard_by ? data.prepard_by : "-"} /></div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Tanggal</label>
                <input type="date" name="prepard_tanggal" plaintext="true" readOnly value={moment(data.prepard_tanggal).format('YYYY-MM-DD')} className="form-control-plaintext" />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Diperiksa oleh</label>
                <input plaintext="true" readOnly name="reviewed_by" className='form-control-plaintext' value={data.reviewed_by ? data.reviewed_by : "-"} />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Tanggal</label>
                <input type="date" name="reviewed_tanggal" plaintext="true" readOnly value={moment(data.reviewed_tanggal).format('YYYY-MM-DD')} className="form-control-plaintext" />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Disetujui oleh</label>
                <input plaintext="true" readOnly name="approved_by" className='form-control-plaintext' value={data.approved_by ? data.approved_by : "-"} /></div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="text" className="d-block">Tanggal</label>
                <input type="date" name="approved_tanggal" plaintext="true" readOnly value={moment(data.approved_tanggal).format('YYYY-MM-DD')} className="form-control-plaintext" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
    )
  }

  return (
    <>
      {ACTION === "detail" &&
        <div className="row">
          <div className="col-md-12">
            <Tabs
              id={"controlled-tab-example"}
              activeKey={keys}
              onSelect={(k) => setKey(k)}
            >
              <Tab eventKey="Umum" title="Umum">
                <View1
                // currentStep={currentSteps} 
                // handleChange={handleChange}
                />
              </Tab>
              <Tab eventKey="Klasifikasi" title="Klasifikasi Insiden">
                <View2
                // handleChange={handleChange}
                />
              </Tab>
              <Tab eventKey="Analisis" title="Analisis Data Penyebab">
                <View3
                // handleChange={handleChange}
                />
              </Tab>
              <Tab eventKey="Rekomendasi" title="Rekomendasi Tindakan Perbaikan">
                <View4
                // handleChange={handleChange}
                />
              </Tab>
              <Tab eventKey="Tim" title="Tim Investigasi Insiden">
                <View5
                // handleChange={handleChange}
                />
              </Tab>
            </Tabs>
          </div>
        </div>
      }
      {ACTION !== "detail" &&
        <form onSubmit={submitHandle} id="formInvestigasi">
          <div className="row">
            <div className="col-md-12">
              {currentSteps === 1 &&
                <React.Fragment>
                  <div className="card">
                    <div style={{ padding: '30px' }}>
                      <h3> Langkah 1/8 </h3>
                      <div>Inspeksi Investigasi Insiden</div>
                      <br />
                      <div>Umum</div>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">No Report (.../YYYY/MM/D) <span className="text-red">*</span></label>
                            <input type="text" data-step="1" name="no_report" required className="form-control" value={data.no_report} onChange={dataHandler} placeholder="Format: Kode cabang/Tahun/Bulan/Nomor urut" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="cabang_id" className="d-block">Cabang <span className="text-red">*</span></label>
                            {ACTION !== "detail" &&
                              <select name="cabang_id" id="cabang_id" data-step="1" className='form-control' value={data.cabang_id || isAuthorized.cabang_id} onChange={dataHandler}>
                                <option value="">Pilih Cabang</option>
                                {cabang.map((a, b) => {
                                  return (
                                    <>
                                      <option key={b} value={a.id}> {a.nama} </option>
                                    </>
                                  )
                                })}
                              </select>}
                              {/* {ACTION === "detail" &&
                                <input plaintext="true" readOnly name="cabang_id" className='form-control-plaintext' value={data.cabang} />
                              } */}
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Unit Terkait <span className="text-red">*</span></label>
                            <input type="text" name="unit_terkait" data-step="1" required className="form-control" value={data.unit_terkait} onChange={dataHandler} placeholder="Tulis Unit Terkait" />
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Judul Report <span className="text-red">*</span></label>
                            <input type="text" name="judul_report" data-step="1" required className="form-control" value={data.judul_report} onChange={dataHandler} placeholder="Tulis Judul Report" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Kronologi Kejadian <span className="text-red">*</span></label>
                            <textarea rows="7" name="kronologi_kejadian" data-step="1" required className="form-control" value={data.kronologi_kejadian} onChange={dataHandler} maxlength="4000" placeholder="Tulis Kronologi Kejadian"></textarea>
                            <span>{data.kronologi_kejadian? data.kronologi_kejadian.length : count.kronologi_kejadian} / 4000 Karakter</span>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Temuan Investigasi <span className="text-red">*</span></label>
                            <textarea rows="7" type="text" data-step="1" name="temuan_investigasi" required className="form-control" value={data.temuan_investigasi} onChange={dataHandler} maxlength="4000" placeholder="Tulis Temuan Investigasi"></textarea>
                            <span>{data.temuan_investigasi? data.temuan_investigasi.length : count.temuan_investigasi} / 4000 Karakter</span>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor={"bukti_temuan_file"} className="d-block">Unggah Bukti Temuan <span className="text-red">*</span></label>
                            
                            <div className={`upload-wrapper d-block`}>
                              {ACTION !== 'detail' &&
                                  <input
                                      type="file"
                                      name="bukti_temuan" 
                                      onChange={dataHandler}
                                      required={data['bukti_temuan'] ? false : true}
                                  /> 
                              }
                              <Image data={data}  dataHandle={dataHandler} name="bukti_temuan"/>
                            </div>
                              
                          {<span className="text-danger d-block mt-2">{errMessage}</span>}
                          </div>
                        </div>

                      </div>
                      <div className="pb-5 mb-5">Dilaporkan oleh </div>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Saksi 1 <span className="text-red">*</span></label>
                            <input type="text" name="saksi_1" data-step="1" required="true" className="form-control" value={data.saksi_1} onChange={dataHandler} placeholder="Tulis Saksi 1" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Saksi 2 <span className="text-red">*</span></label>
                            <input type="text" name="saksi_2" data-step="1" required className="form-control" value={data.saksi_2} onChange={dataHandler} placeholder="Tulis Saksi 2" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Investigator <span className="text-red">*</span></label>
                            <input type="text" name="investigator" data-step="1" required="true" className="form-control" value={data.investigator} onChange={dataHandler} placeholder="Tulis Investigator" />
                          </div>
                        </div>
                      </div>
                      <div className="pb-5 mb-5">Rincian kegiatan investigasi yang dilakukan</div>
                      <div className="row">
                        <div className="col-md-6 mb-5">
                          <div className="form-check form-check-inline">
                            <input type="radio" data-step="1" required name="rincian_kegiatan" onChange={dataHandler} value="Inspeksi di tempat kejadian" checked={data.rincian_kegiatan === "Inspeksi di tempat kejadian" ? true : false} id="kejadian" className={`form-check-input`} />
                            <label
                              className="form-check-label"
                              htmlFor="kejadian"
                            >
                              Inspeksi di tempat kejadian
                          </label>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-check form-check-inline">
                            <input type="radio" data-step="1" required onChange={dataHandler} name="rincian_kegiatan" value="Sketsa Kejadian" id="Sketsa" checked={data.rincian_kegiatan === "Sketsa Kejadian" ? true : false} className={`form-check-input`} />
                            <label
                              className="form-check-label"
                              htmlFor="Sketsa"
                            >
                              Sketsa Kejadian
                          </label>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-check form-check-inline">
                            <input type="radio" data-step="1" required value="Wawancara" onChange={dataHandler} name="rincian_kegiatan" id="Wawancara" checked={data.rincian_kegiatan === "Wawancara" ? true : false} className={`form-check-input`} />
                            <label
                              className="form-check-label"
                              htmlFor="Wawancara"
                            >
                              Wawancara
                          </label>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-check form-check-inline">
                            <input type="radio" data-step="1" required onChange={dataHandler} value="Lainnya" name="rincian_kegiatan" id="Lainnya" checked={(data.rincian_kegiatan === "Lainnya" ) ? true : false} className={`form-check-input`} />
                            <label
                              className="form-check-label"
                              htmlFor="Lainnya"
                            >
                              Lainnya
                          </label>
                          </div>
                        </div>
                        <div className="col-md-6 mb-8">
                          <div className="form-check form-check-inline">
                            <input type="radio" data-step="1" required onChange={dataHandler} value="Pengambilan Foto/Video" name="rincian_kegiatan" id="Pengambilan" defaultChecked={ACTION !== 'create' && data.rincian_kegiatan === "Pengambilan Foto/Video" ? true : false} className={`form-check-input`} />
                            <label
                              className="form-check-label"
                              htmlFor="Pengambilan"
                            >
                              Pengambilan Foto/Video
                          </label>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="mb-5">Tulislah Kegiatan bila memilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="rincian_kegiatan_lainnya" required disabled={lainnya} className="form-control" value={data.rincian_kegiatan_lainnya} onChange={dataHandler} placeholder="Tulis Kejadian" />
                          </div>
                        </div>
                      </div>
                      <div className="pb-5 mb-5">Bukti pendukung yang dilampirkan</div>
                      <div key={"bukti_pendukung"} className="row">
                        <div className="col-md-12" style={{display: "inline-flex",paddingLeft:"0px"}}>
                          <div className="col-md-8">
                            {checkbox.bukti_pendukung.menus.map((a, b) => {
                              // console.log("yayaaa", a, b)
                              if(data.checks && data.checks[0]) {
                                return (
                                  <div className="form-check form-check-inline" style={{paddingRight: "20px",paddingTop: "10px"}}>
                                    <input 
                                      type="checkbox" 
                                      name="bukti_pendukung" 
                                      data-step="1" 
                                      onChange={dataHandler} 
                                      value={a.vals}
                                      defaultChecked={ACTION !== 'create' ? false : data.checks[0].bukti_pendukung.split(",").includes(a.vals) ? true : false}
                                      id={"bukti_pendukung" + b}
                                      className={`form-check-input`} 
                                    />
                                    {console.log('action', ACTION)}
                                    <label
                                      className="form-check-label"
                                      htmlFor={"bukti_pendukung" + b}
                                    >
                                      {a.detail}
                                    </label>
                                  </div>
                                );
                              };
                              return (
                                  <div className="form-check form-check-inline" style={{paddingRight: "20px",paddingTop: "10px"}}>
                                    <input 
                                      type="checkbox" 
                                      name="bukti_pendukung" 
                                      data-step="1" 
                                      onChange={dataHandler} 
                                      value={a.vals}
                                      id={"bukti_pendukung" + b}
                                      className={`form-check-input`} 
                                    />
                                    {console.log('action', ACTION)}
                                    <label
                                      className="form-check-label"
                                      htmlFor={"bukti_pendukung" + b}
                                    >
                                      {a.detail}
                                    </label>
                                  </div>
                              );
                            })}
                          </div>
                          <div className="col-md-4" >
                            <div className="form-group">
                              <input type="text" name="bukti_pendukung_lainnya" required disabled={lainnya} className="form-control" value={data.bukti_pendukung_lainnya} onChange={dataHandler} placeholder="Tulis Kejadian" />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              }
              {currentSteps === 2 &&
                <React.Fragment>
                  <div className="card">
                    <div style={{ padding: '30px' }}>
                      <h3> Langkah 2/8 </h3>
                      <div>Klasifikasi Insiden</div>
                      <br />
                      <h6 className="pb-5">Luka Atau Sakit</h6>
                      <div className="row pb-10">
                        <div className="col-md-6">
                          <div className="form-check form-check-inline">
                            <input 
                              type="radio" 
                              onChange={dataHandler} 
                              value="Minor Injury" 
                              data-step="2" 
                              name="luka_sakit" 
                              id="Injury" 
                              defaultChecked={ACTION !== 'create' ? false : data.luka_sakit && data.luka_sakit === "Minor Injury" ? true : false}
                              className={`form-check-input`} 
                            />
                            <label
                              className="form-check-label"
                              htmlFor="Injury"
                            >
                              Minor Injury
                            </label>
                          </div>
                        </div>

                        <div className="col-md-6">
                          <div className="form-check form-check-inline">
                            <input
                              type="radio"
                              onChange={dataHandler}
                              value="Major Injury"
                              data-step="2"
                              name="luka_sakit"
                              id="Injury2"
                              defaultChecked={ACTION !== 'create' ? false : data.luka_sakit && data.luka_sakit === "Major Injury" ? true : false}
                              className={`form-check-input`}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="Injury2"
                            >
                              Mayor Injury
                        </label>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-check form-check-inline">
                            <input
                              type="radio"
                              onChange={dataHandler}
                              value="Fatality"
                              data-step="2"
                              name="luka_sakit"
                              id="Injury3"
                              defaultChecked={ACTION !== 'create' ? false : data.luka_sakit && data.luka_sakit === "Fatality" ? true : false}
                              className={`form-check-input`}
                            />
                            <label
                              className="form-check-label"
                              htmlFor="Injury3"
                            >
                              Fatality
                        </label>
                          </div>
                        </div>
                      </div>
                      <div>
                        <h6 className="py-10">Wujud Cidera</h6>
                        <div className="row">
                          {checkbox.wujud_cedera.menus.map((a, b) => {
                            if(data.checks && data.checks[1]) {
                              return (
                                <div key={b} className="col-md-6 pb-5">
                                  <div className="form-check form-check-inline">
                                    <input
                                      type="checkbox"
                                      name="wujud_cedera"
                                      data-step="2"
                                      onChange={dataHandler}
                                      value={a.vals}
                                      defaultChecked={ACTION !== 'create' ? false : data.checks[1].wujud_cedera.split(",").includes(a.vals) ? true : false}
                                      id={"wujud" + b}
                                      className={`form-check-input`}
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor={"wujud" + b}
                                    >
                                      {a.detail}
                                    </label>
                                  </div>
                                </div>
                              );
                            };
                            return (
                              <div key={b} className="col-md-6 pb-5">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="wujud_cedera"
                                    data-step="2"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"wujud" + b}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"wujud" + b}
                                  >
                                    {a.detail}
                                  </label>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                        <h6 className="py-10">Bagian Tubuh yang Cidera</h6>
                        <div className="row">
                          {checkbox.tubuh_cedera.menus.map((a, b) => {
                            if(data.checks && data.checks[2]) {
                              return (
                                <div key={b} className="col-md-4 pb-5">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="bagian_tubuh_cedera"
                                    data-step="2"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"tubuh" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[2].bagian_tubuh_cedera.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"tubuh" + b}
                                  >
                                    {a.detail}
                                  </label>
                                </div>
                              </div>
                              );
                            };
                            return (
                              <div key={b} className="col-md-4 pb-5">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="bagian_tubuh_cedera"
                                    data-step="2"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"tubuh" + b}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"tubuh" + b}
                                  >
                                    {a.detail}
                                  </label>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                        <h6 className="py-10">Mekanisme Cidera</h6>
                        <div className="row">
                          {checkbox.mekanisme_cedera.menus.map((a, b) => {
                            if(data.checks && data.checks[3]) {
                              return (
                                <div key={b} className="col-md-6 pb-5">
                                  <div className="form-check form-check-inline">
                                    <input
                                      type="checkbox"
                                      name="mekanisme_cedera"
                                      data-step="2"
                                      onChange={dataHandler}
                                      value={a.vals}
                                      id={"mekanisme" + b}
                                      defaultChecked={ACTION !== 'create' ? false : data.checks[3].mekanisme_cedera.split(",").includes(a.vals) ? true : false}
                                      className={`form-check-input`}
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor={"mekanisme" + b}
                                    >
                                      {a.detail}
                                    </label>
                                  </div>
                                </div>
                              );
                            };
                            return (
                              <div key={b} className="col-md-6 pb-5">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="mekanisme_cedera"
                                    data-step="2"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"mekanisme" + b}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"mekanisme" + b}
                                  >
                                    {a.detail}
                                  </label>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              }
              {currentSteps === 3 &&
                <React.Fragment>
                  <div className="card">
                    <div style={{ padding: '30px' }}>
                      <h3> Langkah 3/8 </h3>
                      <div>Klasifikasi Insiden</div>
                      <br />
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Kerusakan Alat</label>
                            <textarea rows="7" name="kerusakan_alat" data-step="3" required className="form-control" value={data.kerusakan_alat} onChange={dataHandler} maxlength="2000" placeholder="Tulis kerusakan alat"></textarea>
                            <span>{data.kerusakan_alat? data.kerusakan_alat.length : count.kerusakan_alat} / 4000 Karakter</span>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <h3>Nearmiss</h3>
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Uraian Kejadian</label>
                            <textarea rows="7" name="uraian_kejadian" data-step="3" required className="form-control" value={data.uraian_kejadian} onChange={dataHandler} maxlength="4000" placeholder="Tulis uraian kejadian"></textarea>
                            <span>{data.uraian_kejadian? data.uraian_kejadian.length : count.uraian_kejadian} / 4000 Karakter</span> 
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Analisa Penyebab Kejadian</label>
                            <textarea rows="7" name="analisa_penyebab" data-step="3" required className="form-control" value={data.analisa_penyebab} onChange={dataHandler} maxlength="4000" placeholder="Tulis analisa penyebab kejadian"></textarea>
                            <span>{data.analisa_penyebab? data.analisa_penyebab.length : count.analisa_penyebab} / 4000 Karakter</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              }
              {currentSteps === 4 &&
                <React.Fragment>
                  <div className="card">
                    <div style={{ padding: '30px' }}>
                      <h3> Langkah 4/8 </h3>
                      <div>Analisis Data Penyebab</div>
                      <br />
                      <h5>Penyebab Langsung</h5>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>A. Peralatan dan Perlengkapan</h6>
                        </div>
                      </div>

                      {checkbox.peralatan_kelengkapan.menus.map((a, b) => {
                        if(data.checks && data.checks[4]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="peralatan_kelengkapan" 
                                    data-step="4" 
                                    onChange={dataHandler} 
                                    value={a.vals} 
                                    id={"peralatan_kelengkapan" + b} 
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[4].peralatan_kelengkapan.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"peralatan_kelengkapan" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="peralatan_kelengkapan" 
                                  data-step="4" 
                                  onChange={dataHandler} 
                                  value={a.vals} 
                                  id={"peralatan_kelengkapan" + b} 
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"peralatan_kelengkapan" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="peralatan_kelengkapan_lainnya" required className="form-control" value={data.peralatan_kelengkapan_lainnya} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>B. Alat Pelindung Diri</h6>
                        </div>
                      </div>
                      {checkbox.alat_pelindung_diri.menus.map((a, b) => {
                        if(data.checks && data.checks[5]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="alat_pelindung_diri"
                                    data-step="4"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"alat_pelindung_diri" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[5].alat_pelindung_diri.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"alat_pelindung_diri" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="alat_pelindung_diri"
                                  data-step="4"
                                  onChange={dataHandler}
                                  value={a.vals}
                                  id={"alat_pelindung_diri" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"alat_pelindung_diri" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="alat_pelindung_diri_lainnya" required className="form-control" value={data.alat_pelindung_diri_lainnya} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>C. Perilaku</h6>
                        </div>
                      </div>
                      {checkbox.perilaku.menus.map((a, b) => {
                        if(data.checks && data.checks[6]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="perilaku"
                                    data-step="4"
                                    onChange={dataHandler}
                                    value={a.vals} id={"perilaku" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[6].perilaku.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"perilaku" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="perilaku"
                                  data-step="4"
                                  onChange={dataHandler}
                                  value={a.vals} id={"perilaku" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"perilaku" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="perilaku_lainnya" required className="form-control" value={data.perilaku_lainnya} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>D. Kebersihan dan Kerapihan</h6>
                        </div>
                      </div>
                      {checkbox.kebersihan_kerapihan.menus.map((a, b) => {
                        if(data.checks && data.checks[7]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="kebersihan_kerapihan"
                                    data-step="4"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"kebersihan_kerapihan" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[7].kebersihan_kerapihan.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"kebersihan_kerapihan" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="kebersihan_kerapihan"
                                  data-step="4"
                                  onChange={dataHandler}
                                  value={a.vals}
                                  id={"kebersihan_kerapihan" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"kebersihan_kerapihan" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="kebersihan_kerapihan_lainnya" required className="form-control" value={data.kebersihan_kerapihan_lainnya} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              }
              {currentSteps === 5 &&
                <React.Fragment>
                  <div className="card">
                    <div style={{ padding: '30px' }}>
                      <h3> Langkah 5/8 </h3>
                      <div>Analisis Data Penyebab</div>
                      <br />
                      <h5>Prosedur dan Kebijakan</h5>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>A. Peralatan dan Perlengkapan</h6>
                        </div>
                      </div>
                      {checkbox.peralatan_perlengkapan.menus.map((a, b) => {
                        if(data.checks && data.checks[8]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="peralatan_perlengkapan"
                                    data-step="5"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"peralatan_perlengkapan" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[8].peralatan_perlengkapan.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"peralatan_perlengkapan" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="peralatan_perlengkapan"
                                  data-step="5"
                                  onChange={dataHandler}
                                  value={a.vals}
                                  id={"peralatan_perlengkapan" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"peralatan_perlengkapan" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="peralatan_perlengkapan_lainnya" defaultValue={data.peralatan_perlengkapan_lainnya || ""} className="form-control" onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>B. Kemampuan dan Kondisi Fisik</h6>
                        </div>
                      </div>
                      {checkbox.kemampuan_kondisi_fisik.menus.map((a, b) => {
                        if(data.checks && data.checks[9]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="kemampuan_kondisi_fisik"
                                    data-step="5"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"kemampuan_kondisi_fisik" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[9].kemampuan_kondisi_fisik.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"kemampuan_kondisi_fisik" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="kemampuan_kondisi_fisik"
                                  data-step="5"
                                  onChange={dataHandler}
                                  value={a.vals}
                                  id={"kemampuan_kondisi_fisik" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"kemampuan_kondisi_fisik" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="kemampuan_kondisi_fisik_lain" required className="form-control" value={data.kemampuan_kondisi_fisik_lain || ""} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              }
              {currentSteps === 6 &&
                <React.Fragment>
                  <div className="card">
                    <div style={{ padding: '30px' }}>
                      <h3> Langkah 6/8 </h3>
                      <div>Analisis Data Penyebab</div>
                      <br />
                      <h5>Prosedur dan Kebijakan</h5>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>C. Pemeliharaan dan Perbaikan</h6>
                        </div>
                      </div>
                      {checkbox.pemeliharaan_perbaikan.menus.map((a, b) => {
                        if(data.checks && data.checks[10]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="pemeliharaan_perbaikan"
                                    data-step="6"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"pemeliharaan_perbaikan" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[10].pemeliharaan_perbaikan.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"pemeliharaan_perbaikan" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="pemeliharaan_perbaikan"
                                  data-step="6"
                                  onChange={dataHandler}
                                  value={a.vals}
                                  id={"pemeliharaan_perbaikan" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"pemeliharaan_perbaikan" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="pemeliharaan_perbaikan_lainnya" required className="form-control" value={data.pemeliharaan_perbaikan_lainnya || ""} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>D. Kebersihan dan Kerapihan </h6>
                        </div>
                      </div>
                      {checkbox.design.menus.map((a, b) => {
                        if(data.checks && data.checks[11]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="design"
                                    onChange={dataHandler}
                                    data-step="6"
                                    value={a.vals}
                                    id={"design" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[11].design.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"design" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="design"
                                  onChange={dataHandler}
                                  data-step="6"
                                  value={a.vals}
                                  id={"design" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"design" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="kebersihan_kerapihan_lain" required className="form-control" value={data.kebersihan_kerapihan_lain || ""} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>E. Tingkat Kemampuan</h6>
                        </div>
                      </div>
                      {checkbox.tingkat_kemampuan.menus.map((a, b) => {
                        if(data.checks && data.checks[12]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="tingkat_kemampuan"
                                    data-step="6"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"tingkat_kemampuan" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[12].tingkat_kemampuan.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"tingkat_kemampuan" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="tingkat_kemampuan"
                                  data-step="6"
                                  onChange={dataHandler}
                                  value={a.vals}
                                  id={"tingkat_kemampuan" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"tingkat_kemampuan" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="tingkat_kemampuan_lainnya" required className="form-control" value={data.tingkat_kemampuan_lainnya || ""} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>F. Penjagaan</h6>
                        </div>
                      </div>
                      {checkbox.penjagaan.menus.map((a, b) => {
                        if(data.checks && data.checks[13]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="penjagaan"
                                    data-step="6"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"penjagaan" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[13].penjagaan.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"penjagaan" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="penjagaan"
                                  data-step="6"
                                  onChange={dataHandler}
                                  value={a.vals}
                                  id={"penjagaan" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"penjagaan" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="penjagaan_lainnya" required className="form-control" value={data.penjagaan_lainnya || ""} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <h6>G. Tindakan Terkait Lainnya</h6>
                        </div>
                      </div>
                      {checkbox.tindakan_terkait.menus.map((a, b) => {
                        if(data.checks && data.checks[14]) {
                          return (
                            <div key={b} className="row">
                              <div className="col-md-6">
                                <div className="form-check form-check-inline">
                                  <input
                                    type="checkbox"
                                    name="tindakan_terkait"
                                    data-step="6"
                                    onChange={dataHandler}
                                    value={a.vals}
                                    id={"tindakan_terkait" + b}
                                    defaultChecked={ACTION !== 'create' ? false : data.checks[14].tindakan_terkait.split(",").includes(a.vals) ? true : false}
                                    className={`form-check-input`}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={"tindakan_terkait" + b}
                                  >
                                    {a.vals}.{a.detail}
                                  </label>
                                </div>
                              </div>
                            </div>
                          );
                        };
                        return (
                          <div key={b} className="row">
                            <div className="col-md-6">
                              <div className="form-check form-check-inline">
                                <input
                                  type="checkbox"
                                  name="tindakan_terkait"
                                  data-step="6"
                                  onChange={dataHandler}
                                  value={a.vals}
                                  id={"tindakan_terkait" + b}
                                  className={`form-check-input`}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={"tindakan_terkait" + b}
                                >
                                  {a.vals}.{a.detail}
                                </label>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Tulis bila pilih lainnya</div>
                          <div className="form-group">
                            <input type="text" name="tindakan_terkait_lainnya" required className="form-control" value={data.tindakan_terkait_lainnya || ""} onChange={dataHandler} placeholder="Tulis bila pilih lainnya" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="mb-5">Berdasarkan dari Daftar diatas, mohon sebutkan faktor utama yang mengakibatkan Insiden (dengan kode huruf dan angka)</div>
                          <div className="form-group">
                            <input type="text" name="faktor_utama_insiden" data-step="6" required className="form-control" value={data.faktor_utama_insiden} onChange={dataHandler} placeholder="Tulis imediative cause" />
                          </div>
                          <div className="form-group">
                            <input type="text" name="route_couse" data-step="6" required className="form-control" value={data.route_couse} onChange={dataHandler} placeholder="Tulis route cause" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              }
              {currentSteps === 7 &&
                <React.Fragment>
                  <div className="card">
                    <div style={{ padding: '30px' }}>
                      <h3> Langkah 7/8 </h3>
                      <div>Rekomendasi Tindakan Perbaikan</div>
                      <br />
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Rekomendasi Tindakan Selanjutnya</label>
                            <textarea rows="7" name="rekomendasi_tindakan" data-step="7" required className="form-control" value={data.rekomendasi_tindakan} onChange={dataHandler} placeholder="Rekomendasi Tindakan"></textarea>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Pihak yang Bertanggung Jawab</label>
                            <textarea rows="7" name="pihak_yang_bertanggungjawab" data-step="7" required className="form-control" value={data.pihak_yang_bertanggungjawab} onChange={dataHandler} placeholder="Tulis pihak yang bertanggung jawab"></textarea>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Pelaksana</label>
                            <textarea rows="7" name="pelaksana" data-step="7" required className="form-control" value={data.pelaksana} onChange={dataHandler} placeholder="Tulis pelaksana"></textarea>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Target Penyelesaian</label>
                            <InputDate 
                              name="tanggal_pemeriksaan" 
                              dataHandle={dataHandler} 
                              value={data.tanggal_pemeriksaan} 
                              required={true}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              }
              {currentSteps === 8 &&
                <React.Fragment>

                  <div className="card">
                    <div style={{ padding: '30px' }}>
                      <h3> Langkah 8/8 </h3>
                      <div>Tim Investigasi Insiden</div>
                      <br />
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="tabnames" className="d-block">Nama</label>
                            <input type="text" id="tabnames" name="nama" defaultValue={tableTemp.nama} className="form-control table-view" onChange={tableHandle} placeholder="Tulis Nama" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <label htmlFor="text" className="d-block">Status ?</label>
                          <div className="form-check form-check-inline">
                            <input type="radio" id="timleader" checked={tableTemp.status === 'Tim Leader'} name="status" value="Tim Leader" onChange={tableHandle} className={`form-check-input table-view`} />
                            <label
                              className="form-check-label"
                              htmlFor="timleader"
                            >
                              Tim Leader
                      </label>
                          </div>
                          <div className="form-check form-check-inline">
                            <input type="radio" id="members" checked={tableTemp.status === 'Member'} value="Member" name="status" onChange={tableHandle} className={`form-check-input table-view`} />
                            <label
                              className="form-check-label"
                              htmlFor="members"
                            >
                              Member
                      </label>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="jabatan" className="d-block">Jabatan</label>
                            <input type="text" id="jabatan" name="jabatan" onChange={tableHandle} className="form-control table-view" defaultValue={tableTemp.jabatan} placeholder="Tulis Jabatan" />
                          </div>
                        </div>
                      </div>
                      <div className="row" style={{ display: "none" }}>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="investigasi_insiden_id" className="d-block">investigasi_insiden_id</label>
                            <input type="text" id="investigasi_insiden_id" name="investigasi_insiden_id" onLoad={tableHandle} className="form-control table-view" value={KEY} placeholder="Lengkapi isian ini" />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-6">
                          <button type="button" className="btn btn-lg btn-primary" onClick={addTable} disabled={Object.keys(tableTemp).length < 4}>Add</button>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12 pt-8">
                          <div className="table-responsive" >
                            <table className="table table-hover w-100">
                              <thead>
                                <tr className="text-center" style={{ background: '#F4F5F7' }}>
                                  <td>No</td>
                                  <td>Nama</td>
                                  <td>Jabatan</td>
                                  <td>Tanggal</td>
                                  <td>Status</td>
                                  <td>Aksi</td>
                                </tr>
                              </thead>
                              <tbody>
                                {dataTable.length > 0 && dataTable.map((c, d) => {
                                  return (
                                    <tr key={d} className="text-center">
                                      <td>{(d + 1)}</td>
                                      <td>{c.nama}</td>
                                      <td>{c.jabatan}</td>
                                      <td>{moment(c.tgl).format('YYYY-MM-DD')}</td>
                                      <td>{c.status}</td>
                                      <td className="badge badge-default"><i className="far fa-window-close text-danger" onClick={() => removeTable(d)}></i></td>
                                    </tr>
                                  )
                                })}

                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="card">
                    <div style={{ padding: '30px' }}>
                      <h6>Approval</h6>
                      <br />
                      <div className="row">
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="Prepared" className="d-block">Disusun oleh</label>
                            <input type="text" name="prepard_by" required className="form-control" value={data.prepard_by || ""} onChange={dataHandler} placeholder="Tulis disusun oleh" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Tanggal</label>
                            <InputDate 
                              name="prepard_tanggal" 
                              dataHandle={dataHandler} 
                              value={data.prepard_tanggal} 
                              required={true}
                            />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Diperiksa oleh</label>
                            <input type="text" name="reviewed_by" required className="form-control" value={data.reviewed_by || ""} onChange={dataHandler} placeholder="Tulis diperiksa oleh" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Tanggal</label>
                            <InputDate 
                              name="reviewed_tanggal" 
                              dataHandle={dataHandler} 
                              value={data.reviewed_tanggal} 
                              required={true}
                            />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Disetujui oleh</label>
                            <input type="text" name="approved_by" required className="form-control" value={data.approved_by || ""} onChange={dataHandler} placeholder="Tulis disetujui oleh" />
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="text" className="d-block">Tanggal</label>
                            <InputDate 
                              name="approved_tanggal" 
                              dataHandle={dataHandler} 
                              value={data.approved_tanggal} 
                              required={true}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              }
              <div className="float-right py-10">
                <PreviousButton />
                <NextButton />
              </div>
            </div>
          </div>
        </form>
      }
    </>
  )
}
