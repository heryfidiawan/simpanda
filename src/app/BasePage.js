import React, { Suspense } from "react";

import { Redirect, Switch, Route } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";

// Main Page
import { Dashboard } from "./pages/Dashboard";
import { IndexPage } from "./pages/IndexPage";

import { FormDetail } from "./pages/FormDetail";

import { SchedulePandu } from "./pages/resource/SchedulePandu";
import { PendukungPandu } from "./pages/resource/PendukungPandu";
import { ArmadaSchedule } from "./pages/asset/ArmadaSchedule";

import { InvestigasiInsiden } from "./pages/inspection/InvestigasiInsiden";
import { PemeriksaanKapal } from "./pages/inspection/PemeriksaanKapal";
import { SaranaBantuPemandu } from "./pages/inspection/SaranaBantuPemandu";
import { EvaluasiPelimpahan } from "./pages/inspection/EvaluasiPelimpahan";
import { UserGroup } from "./pages/UserGroup";

export default function BasePage({auth}) {
  // console.log('BasePage auth',auth)
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {<Redirect exact from="/" to="/dashboard" />}

        <ContentRoute path="/dashboard" component={() => <Dashboard auth={auth}/>} />

        <Route exact path="/resource/realisasi-pandu"
          component={() => (<IndexPage auth={auth} parent={"resource"} menu={"realisasi_pandu"} />)}
        />

        <Route exact path="/resource/realisasi-pandu/:action" component={SchedulePandu} />
        <Route exact path="/resource/realisasi-pandu/:action/:key" component={SchedulePandu} />



        <Route exact path="/resource/pendukung-pandu"
          component={() => (<IndexPage auth={auth} parent={"resource"} menu={"pendukung_pandu"} />)}
        />

        <Route exact path="/resource/pendukung-pandu/:action" component={PendukungPandu} />
        <Route exact path="/resource/pendukung-pandu/:action/:key" component={PendukungPandu} />



        <Route exact path="/resource/pandu"
          component={() => <IndexPage auth={auth} parent={"resource"} menu={"pandu"} />}
        />

        <Route exact path="/resource/:menu/create" component={FormDetail} />
        <Route exact path="/resource/:menu/detail/:key" component={FormDetail} />
        <Route path="/resource/:menu/edit/:key" component={FormDetail} />



        <Route exact path="/asset/realisasi-armada"
          component={() => (<IndexPage auth={auth} parent={"asset"} menu={"realisasi_armada"} />)}
        />

        <Route exact path="/asset/realisasi-armada/:action" component={ArmadaSchedule} />
        <Route exact path="/asset/realisasi-armada/:action/:key" component={ArmadaSchedule} />
        


        <Route exact path="/asset/kapal"
          component={() => <IndexPage auth={auth} parent={"asset"} menu={"kapal"} />}
        />
        <Route exact path="/asset/stasiun-pandu-and-equipment"
          component={() => (<IndexPage auth={auth} parent={"asset"} menu={"stasiun_pandu_and_equipment"} />)}
        />
        <Route exact path="/asset/rumah-dinas"
          component={() => <IndexPage auth={auth} parent={"asset"} menu={"rumah_dinas"} />}
        />

        <Route exact path="/asset/:menu/create" component={FormDetail} />
        <Route exact path="/asset/:menu/detail/:key" component={FormDetail}/>
        <Route path="/asset/:menu/edit/:key" component={FormDetail} />



        <Route exact path="/inspection/sarana-bantu-pemandu"
          component={() => (<IndexPage auth={auth} parent={"inspection"} menu={"sarana_bantu_pemandu"} />)}
        />
        <Route exact path="/inspection/sarana-bantu-pemandu/:action" component={SaranaBantuPemandu} />
        <Route exact path="/inspection/sarana-bantu-pemandu/:action/:key" component={SaranaBantuPemandu} />


        <Route exact path="/inspection/pemeriksaan-kapal"
          component={() => (<IndexPage auth={auth} parent={"inspection"} menu={"pemeriksaan_kapal"} />)}
        />
        <Route exact path="/inspection/pemeriksaan-kapal/:action" component={PemeriksaanKapal} />
        <Route exact path="/inspection/pemeriksaan-kapal/:action/:key" component={PemeriksaanKapal} />


        <Route exact path="/inspection/investigasi-insiden"
          component={() => (<IndexPage auth={auth} parent={"inspection"} menu={"investigasi_insiden"} />)}
        />
        <Route exact path="/inspection/investigasi-insiden/:action" component={InvestigasiInsiden} />
        <Route exact path="/inspection/investigasi-insiden/:action/:key" component={InvestigasiInsiden} />
        

        <Route exact path="/inspection/evaluasi-pelimpahan"
          component={() => (<IndexPage auth={auth} parent={"inspection"} menu={"evaluasi_pelimpahan"} />)}
        />
        <Route exact path="/inspection/evaluasi-pelimpahan/:action" component={EvaluasiPelimpahan} />
        <Route exact path="/inspection/evaluasi-pelimpahan/:action/:key" component={EvaluasiPelimpahan} />


        <Route exact path="/report/crew-list"
          component={() => <IndexPage auth={auth} parent={"report"} menu={"crew_list"} />}
        />

        <Route axact path="/report/pelaporan-management"
          component={() => (<IndexPage auth={auth} parent={"report"} menu={"pelaporan_management"} />)}
        />

        <Route path="/report/pilot-ship-availability"
          component={() => (<IndexPage auth={auth} parent={"report"} menu={"pilot_ship_availability"} />)}
        />

        <Route exact path="/administrator/group-user"
          component={() => (<IndexPage auth={auth} parent={"administrator"} menu={"user_group"} />)}
        />
        <Route exact path="/administrator/group-user/:action" component={UserGroup} />
        <Route exact path="/administrator/group-user/:action/:key" component={UserGroup} />


        <Route exact path="/administrator/user" 
          component={() => <IndexPage auth={auth} parent={"administrator"} menu={"user"} />} 
        />
        <Route exact path="/administrator/:menu/create" component={FormDetail} />
        <Route exact path="/administrator/:menu/detail/:key" component={FormDetail} />
        <Route path="/administrator/:menu/edit/:key" component={FormDetail} />

        <Route exact path="/sertifikat/pandu/:label"
          component={() => (<IndexPage auth={auth} parent={"sertifikat"} menu={"pandu"} />)}
        />
        <Route exact path="/sertifikat/pendukung-pandu/:label"
          component={() => (<IndexPage auth={auth} parent={"sertifikat"} menu={"pendukung_pandu"} />)}
        />
        <Route exact path="/sertifikat/kapal/:label"
          component={() => (<IndexPage auth={auth} parent={"sertifikat"} menu={"assetkapal"} />)}
        />
        <Route exact path="/sertifikat/evaluasi-pelimpahan/:label"
          component={() => (<IndexPage auth={auth} parent={"sertifikat"} menu={"evaluasi_pelimpahan"} />)}
        />

        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
}
